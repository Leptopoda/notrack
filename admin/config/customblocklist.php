<?php
require('../include/global-vars.php');
require('../include/global-functions.php');
require('../include/config.php');
require('../include/menu.php');

ensure_active_session();

/************************************************
*Constants                                      *
************************************************/
define('BLOCKLIST_FILE', '../settings/blacklist.txt');
define('ALLOWLIST_FILE', '../settings/whitelist.txt');

/************************************************
*Global Variables                               *
************************************************/


/**
 * Generate List
 *  Creates a sample allow/block list, used when the file for custom list is missing.
 *
 *  @param str: listtype allow or block
 *  @return array of sample domains
 */
function generate_customlist($listtype) {
  $domainlist = array();

  if ($listtype == 'allow') {
    $domainlist = array(
      array('foresee.com', 'Foresee Surveys', false),
      array('nic.icu', 'Allow from Top Level Domain block', false),
    );
  }
  elseif ($listtype == 'block') {
    $domainlist = array(
      array('advertising.amazon.com', 'Amazon Adverts', false),
      array('polling.bbc.co.uk', 'BBC Breaking News Popup', false),
    );
  }

  return $domainlist;
}


/**
 * Load Custom List
 *  Load /settings/listtype.txt into an array $domainlist
 *
 *  @note regex
 *    Group 1. # Optional Comment (showing current item is disabled)
 *    Group 2. domain.com
 *    Group 3. # Optional Comment
 *    Group 4. Comment
 *
 *  @param str listtype allow or block (incase a new list needs generating)
 *  @param str filename list file to load
 *  @return array of domains
 *
 */
function load_customlist($listtype, $filename) {
  $line = '';
  $domainlist = array();
  $domains = array();                                     //Used for sorting list by domain name
  $matches = array();

  if (! file_exists($filename)) {
    return generate_customlist($listtype);
  }

  $fh = fopen($filename, 'r') or die('Error unable to open '.$filename);
  while (! feof($fh)) {
    $line = strip_tags(fgets($fh));
    if (preg_match('/^(#)?\s?([\w\-\.]{3,253})\s*(?:#)?(.{0,255})\n$/', $line, $matches)) {
      if ($matches[1] == '#') {
        $domainlist[] = array($matches[2], $matches[3], false);
      }
      else {
        $domainlist[] = array($matches[2], $matches[3], true);
      }
    }
  }

  fclose($fh);                                           //Close file

  $domains = array_column($domainlist, 0);               //Extract column 0 - domains
  array_multisort($domains, SORT_ASC, $domainlist);      //Sort ascending by domain name

  return $domainlist;
}


/**
 * Save Custom List
 *  Save $domainlist to /settings/listtype.txt, ready to be picked up by notrackd
 *
 *  @param str filename to write to
 *  @param array domainlist to be save to file
 */
function save_customlist($filename, $domainlist) {
  $filelines = array();
  $listcontents = array();

  foreach ($domainlist as $domainitem) {
    if ($domainitem[2] === true) {                        //Is domain enabled?
      $filelines[] = "{$domainitem[0]} #{$domainitem[1]}".PHP_EOL;
    }
    else {                                                //disabled - comment it out
      $filelines[] = "# {$domainitem[0]} #{$domainitem[1]}".PHP_EOL;
    }
  }

  if (file_put_contents($filename, $filelines) === false) {
    die('Unable to save list to '.$filename);
  }
}


/**
 * Process Bulk Domain Submission
 *  Expecting an array of domains in newdomains, action, and bulkview
 *  New items will be appended with bulkview
 *  1. Carry out input validation on POST items
 *  2. Validate new newdomains
 *  3. Run save_list()
 *
 */
function process_bulk_list() {
  $comment = '';
  $domain = '';
  $filename = '';

  $domainlist = array();
  $newdomains = array();

  $inputargs = array(
    'action'     => array ('filter' => FILTER_VALIDATE_REGEXP,
                           'options' => array (
                           'regexp' => '/^(allow|block)$/',
                           'default' => 'block',
                          ),
                    ),
    'newdomains' => array ('filter' => FILTER_VALIDATE_REGEXP,
                           'options' => array(
                           'regexp' => '/^\[(\[".*",".*",(true|false)\],?)+\]$/',
                           'default' => '',
                          ),
                    ),
    'bulkview' => FILTER_VALIDATE_BOOLEAN,
  );

  $inputs = filter_input_array(INPUT_POST, $inputargs);

  $filename = ($inputs['action'] == 'allow') ? ALLOWLIST_FILE : BLOCKLIST_FILE;

  //contenteditable view will already have the full list of domains, so only append with bulkview
  if ($inputs['bulkview'] === true) {
    $domainlist = load_customlist($inputs['action'], $filename);
  }

  $newdomains = json_decode(strip_tags($inputs['newdomains']));
  foreach($newdomains as $domainitem) {
    if (filter_domain($domainitem[0], true, false) === false) {
      //Ignore unsanitized domains
      continue;
    }

    $domain = strtolower($domainitem[0]);                 //domain names should be lowercase

    $comment = filter_var($domainitem[1], FILTER_SANITIZE_STRING);
    if (strlen($comment) > 255) {                         //Limit comment length to TINYTEXT
      $comment = substr($comment, 0, 254);
    }

    //Prevent duplication by checking if supplied domain is in the domain column of $domainlist
    if (in_array($domain, array_column($domainlist, 0))) {
      continue;
    }

    $domainlist[] = array($domain, $comment, $domainitem[2]);
  }

  save_customlist($filename, $domainlist);
  header("Location: ?v={$inputs['action']}");
}



/**
 * Process Single Domain Submission
 *  Expecting an action, comment, and singledomain
 *  Append new domain to existing list
 *  1. Carry out input validation on POST items
 *  2. Validate new singledomain
 *  3. Run save_list()
 *
 */
function process_single_domain() {
  $comment = '';
  $domain = '';
  $filename = '';

  $domainlist = array();
  $newdomains = array();

  $inputargs = array(
    'action'     => array ('filter' => FILTER_VALIDATE_REGEXP,
                          'options' => array (
                            'regexp' => '/^(allow|block)$/',
                            'default' => 'block',
                          ),
                    ),
    'comment' => FILTER_SANITIZE_STRING,
    'singledomain' => FILTER_SANITIZE_STRING | FILTER_FLAG_STRIP_LOW,
  );

  $inputs = filter_input_array(INPUT_POST, $inputargs);

  $filename = ($inputs['action'] == 'allow') ? ALLOWLIST_FILE : BLOCKLIST_FILE;
  $domainlist = load_customlist($inputs['action'], $filename);

  if (filter_domain($inputs['singledomain'], true, false) === false) {
    //Ignore unsanitized domain
    return;
  }

  $domain = strtolower($inputs['singledomain']);          //domain name should be lowercase
  $comment = $inputs['comment'];
  if (strlen($comment) > 255) {                           //Limit comment length to TINYTEXT
    $comment = substr($comment, 0, 254);
  }

  //Prevent duplication by checking if supplied domain is in the domain column of $domainlist
  if (in_array($domain, array_column($domainlist, 0))) {
    return;
  }

  $domainlist[] = array($domain, $comment, true);

  save_customlist($filename, $domainlist);
  header("Location: ?v={$inputs['action']}");
}


/**
 * Draw Toolbar
 *  Contents of table toolbar
 *
 *  @param str view - allow or block
 */
function draw_table_toolbar($view) {
  $active0 = ($view == 'allow') ? ' checked="checked"' : '';
  $active1 = ($view == 'block') ? ' checked="checked"' : '';

  echo '<div class="table-toolbar">', PHP_EOL;
  echo '<button class="float-left material-icon-button-text icon-upload" onclick="window.location.href=\'?v=bulk', $view, '\'">Bulk Upload</button>', PHP_EOL;
  echo '<div class="table-toolbar-options">', PHP_EOL;    //Start Table Toolbar Export
  echo '<button class="button-grey material-icon-centre icon-export" title="Export" onclick="exportTable(\'', $view, '\')">&nbsp;</button>', PHP_EOL;
  echo '</div>'.PHP_EOL;                                  //End Table Toolbar Export

  echo '<form method="get">', PHP_EOL;                    //Groupby box
  echo '<div id="groupby-container">', PHP_EOL;
  echo '<input id="gbtab0" type="radio" name="v" value="allow" onchange="submit()"', $active0, '><label for="gbtab0">Allow List</label>', PHP_EOL;
  echo '<input id="gbtab1" type="radio" name="v" value="block" onchange="submit()"', $active1, '><label for="gbtab1">Block List</label>', PHP_EOL;
  echo '</div>', PHP_EOL;                                 //End Groupby box
  echo '</form>', PHP_EOL;
  echo '</div>', PHP_EOL;                                 //End Table Toolbar
}


/**
 * Setup Custom Block List Table
 *  Call load_customlist
 *  Initialise Table
 *  Export required constants to JavaScript
 */
function setup_table($listtype, $filename) {
  $domainlist = array();

  $domainlist = load_customlist($listtype, $filename);

  echo '<section>', PHP_EOL;

  draw_table_toolbar($listtype);

  echo '<table id="custombl-table">', PHP_EOL;            //Start custom list table
  echo '<tr><th>&nbsp;</th><th>Domain</th><th>Comment</th><th>&nbsp;</th></tr>', PHP_EOL;

  echo '</table>', PHP_EOL;                               //End custom list table
  echo '</section>', PHP_EOL;

  echo '<script>', PHP_EOL;
  echo 'const DATA = ', json_encode($domainlist), ';', PHP_EOL;
  echo 'populateCustomBlTable();', PHP_EOL;
  echo '</script>', PHP_EOL;
}


/**
 * Show Bulkupload Layout
 *
 */
function show_bulkupload() {
  echo '<pre id="bulkBox" contenteditable="true">', PHP_EOL;
  echo '#paste a list of domains here', PHP_EOL;
  echo '</pre>', PHP_EOL;
  echo '<section>', PHP_EOL;
  echo '<div class="centered">', PHP_EOL;
  echo '<button class="material-icon-button-text icon-tick" id="bulkSubmit" onclick="submitBulkBox()">Submit</button>', PHP_EOL;
  echo '</div>', PHP_EOL;
  echo '</section>', PHP_EOL;
}


/********************************************************************
 Main
*/

//Carry out any post actions and then reload
if (isset($_POST['action'])) {
  if (isset($_POST['newdomains'])) {
    process_bulk_list();
  }
  elseif (isset($_POST['singledomain'])) {
    process_single_domain();
  }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="../css/master.css" rel="stylesheet" type="text/css">
  <link href="../css/icons.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png">
  <script src="../include/menu.js"></script>
  <script src="../include/customblocklist.js"></script>
  <title>NoTrack - Custom Blocklists</title>
</head>

<body>
<?php

$bulkview = false;
$filename = '';
$listtype = '';
$title = '';

$view = $_GET['v'] ?? 'block';

switch($view) {
  case 'allow':
    $title = 'Allow List';
    $listtype = 'allow';
    $filename = ALLOWLIST_FILE;
    break;
  case 'bulkallow':
    $title = 'Allow List';
    $listtype = 'allow';
    $filename = ALLOWLIST_FILE;
    $bulkview = true;
    break;
  case 'bulkblock':
    $title = 'Block List';
    $listtype = 'block';
    $filename = BLOCKLIST_FILE;
    $bulkview = true;
    break;
  default:
    $title = 'Block List';
    $listtype = 'block';
    $filename = BLOCKLIST_FILE;
    $view = 'block';
    break;
}

draw_page_header($title);
draw_page_nav();
echo '<main>', PHP_EOL;

if ($bulkview) {
  show_bulkupload($listtype);
}
else {
  setup_table($listtype, $filename);
}

echo '</main>', PHP_EOL;
echo '<form name="customblocklist" id="blocklistform" method="POST">', PHP_EOL;
echo '<input type="hidden" name="action" value=', json_encode($listtype), '>', PHP_EOL;
echo '<input type="hidden" name="newdomains" value="" id="newdomains">', PHP_EOL;
echo '<input type="hidden" name="bulkview" value = ', json_encode($bulkview), '>', PHP_EOL;
echo '</form>';
?>
</body>
</html>
