<?php
require('../include/global-vars.php');
require('../include/global-functions.php');
require('../include/config.php');
require('../include/menu.php');

ensure_active_session();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../css/master.css" rel="stylesheet" type="text/css">
<link href="../css/icons.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" href="../favicon.png">
<script src="../include/menu.js"></script>
<title>NoTrack - Config</title>
</head>
<?php

draw_page_header('Config');
draw_page_nav();
?>
<main>

<section id="system">
<h5>System</h5>
<div class="conf-nav">
<a href="./general.php"><img src="../svg/menu_config.svg" alt=""><span>General</span></a>
<a href="../dhcp.php"><img src="../svg/menu_dhcp.svg" alt=""><span>Home Network</span></a>
<a href="./dns.php"><img src="../svg/menu_dns.svg" alt=""><span>DNS Server</span></a>
<a href="./security.php"><img src="../svg/menu_security.svg" alt=""><span>Security</span></a>
<a href="../upgrade.php"><img src="../svg/menu_upgrade.svg" alt=""><span>Upgrade</span></a>
</div>
</section>

<section id="blocklists">
<h5>Block Lists</h5>
<div class="conf-nav">
<a href="./blocklists.php"><img src="../svg/menu_blocklists.svg" alt=""><span>Select Block Lists</span></a>
<a href="./tld.php"><img src="../svg/menu_domain.svg" alt=""><span>Top Level Domains</span></a>
<a href="./customblocklist.php?v=allow"><img src="../svg/menu_white.svg" alt=""><span>Custom Allow List</span></a>
<a href="./customblocklist.php?v=block"><img src="../svg/menu_black.svg" alt=""><span>Custom Block List</span></a>
<a href="./domains.php"><img src="../svg/menu_sites.svg" alt=""><span>View Domains Blocked</span></a>
</div>
</section>

</main>
</body>
</html>
