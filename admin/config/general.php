<?php
/**
 * NoTrack General Config
 * Provide form inputs split into sections: General, Security
 * POST inputs are validated, and if successful the config value is updated.
 */
require('../include/global-vars.php');
require('../include/global-functions.php');
require('../include/config.php');
require('../include/menu.php');

ensure_active_session();

/************************************************
*Constants                                      *
************************************************/
//define('WEBLIST', ['lighttpd', 'apache', 'nginx']);
define('SETTINGS_SINK', $_SERVER['DOCUMENT_ROOT'].'/sink/sinksettings.php');

/************************************************
*Global Variables                               *
************************************************/


/************************************************
*Arrays                                         *
************************************************/


//Deal with POST actions first, that way we can reload the page and remove POST requests from browser history.
if (isset($_POST['action'])) {
  if ($_POST['action'] == 'general') update_general_config();
  elseif ($_POST['action'] == 'security') update_security_config();
  $config->save();
  sleep(3);                             //Short pause to prevent race condition
  header('Location: general.php');
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="../css/master.css" rel="stylesheet" type="text/css">
  <link href="../css/icons.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png">
  <script src="../include/menu.js"></script>
  <title>NoTrack - General Config</title>
</head>
<?php


/**
 * General Config Form
 *
 */
function draw_general_form() {
  global $config;

  $help = '';
  $key = '';
  $value = '';
  $selected = '';

  //$freemem = preg_split('/\s+/', exec('free -m | grep Mem'));
  //$uptime = exec('uptime -p');

  echo '<section id="general">', PHP_EOL;
  echo '<form name="generalform" method="post">', PHP_EOL;
  echo '<input type="hidden" name="action" value="general">', PHP_EOL;
  echo "<h5>General Config</h5>", PHP_EOL;
  echo '<table class="conf-table">', PHP_EOL;

  //echo "<tr><td>Memory Used:</td><td>{$freemem[2]} MB</td></tr>", PHP_EOL;
  //echo "<tr><td>Free Memory:</td><td>{$freemem[3]} MB</td></tr>", PHP_EOL;
  //echo "<tr><td>Uptime:</td><td>{$uptime}</td></tr>", PHP_EOL;
  echo '<tr><td>NoTrack Version:</td><td>', VERSION, '</td></tr>', PHP_EOL;

  //Search Engine select box
  echo '<tr><td>Search Engine: </td>',
    '<td><select name="search" onchange="submitForm(\'server\')">', PHP_EOL;
  foreach ($config::SEARCHENGINELIST as $key => $value) {
    $selected = ($key == $config->search_engine) ? ' selected' : '';
    echo "<option value='{$key}'{$selected}>{$key}</option>", PHP_EOL;
  }
  echo '</select></td></tr>', PHP_EOL;

  //Whois select box
  echo '<tr><td>Who Is Lookup: </td>',
    '<td><select name="whois" onchange="submitForm(\'server\')">', PHP_EOL;
  foreach ($config::WHOISLIST as $key => $value) {
    $selected = ($key == $config->whois_provider) ? ' selected' : '';
    echo "<option value='{$key}'{$selected}>{$key}</option>".PHP_EOL;
  }
  echo '</select></td></tr>', PHP_EOL;

  $help = '<i>If you visit a blocked page and you wish to unblock it, you can provide a link to NoTrack admin here.</i>';
  echo '<tr><td>Sink Page Unblock URL:</td><td><input type="text" name="sinkurl" value="', $config->sink_url, '" placeholder="http://notrack.local">', $help, '</td></tr>';

  $help = '<i><a href="https://jsonwhois.com/">JsonWhois</a> provide a free Who Is lookup service which can be used by NoTrack Investigate.</i>';
  echo '<tr><td>JsonWhois API:</td><td><input type="text" name="whoisapi" value=', json_encode($config->whois_api), '">', $help, '</td></tr>';

  echo '<tr><td colspan="2"><button class="material-icon-button-text icon-tick" type="submit">Save Changes</button></td></tr>', PHP_EOL;
  echo '</table>', PHP_EOL;
  echo '</form>', PHP_EOL;
  echo '</section>', PHP_EOL;
}


/**
 * Draw Security Form
 */
function draw_security_form() {
  global $config;

  echo '<section id="security">', PHP_EOL;
  echo '<form method="POST">'.PHP_EOL;
  echo '<input type="hidden" name="action" value="security">', PHP_EOL;
  echo '<h5>Security</h5>', PHP_EOL;
  echo '<table class="conf-table">', PHP_EOL;
  echo '<tr><td>API Key:</td><td><input type="text" name="apikey" id="apikey" value="', $config->api_key,'">&nbsp;',
    '<button class="button-grey material-icon-button-text icon-generate" type="button" onclick="generateKey(\'apikey\')">',
    'Generate</button></td></tr>', PHP_EOL;
  echo '<tr><td>Read Only API:</td><td><input type="text" name="apireadonly" id="apireadonly" value="'.$config->api_readonly.'">&nbsp;',
    '<button class="button-grey material-icon-button-text icon-generate" type="button" onclick="generateKey(\'apireadonly\')">',
    'Generate</button></td></tr>', PHP_EOL;

  echo '<tr><td colspan="2"><button class="material-icon-button-text icon-tick" type="submit">Save Changes</button></td></tr>', PHP_EOL;
  echo '</table>', PHP_EOL;
  echo '</form>', PHP_EOL;
  echo '</section>', PHP_EOL;
}

/********************************************************************
 *  Show Web Server Section DEPRECATED
 *    1. Find running web server from WEBLIST using ps
 *    2. Split result of ps into an $pidarray using delimiter of one or more spaces
 *    3. $pidarray elements:
 *       0 - Process
 *       1 - PID
 *       2 - Date Opened
 *       3 - Memory Usage
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function web_section() {
  global $config, $dbwrapper;

  $pidstr = '';
  $pidarray = array();

  foreach (WEBLIST AS $app) {
    $pidstr = exec("ps -eo fname,pid,stime,pmem | grep $app");

    //Has valid process been found?
    if ($pidstr != '') {
      $pidarray = preg_split('/\s+/', $pidstr);            //Explode into array
      $pidarray[0] = ucfirst($pidarray[0]).' is Active';   //Prettify process name
      break;
    }
  }

  //Fallback if no process hasn't been found
  if ($pidstr == '') {
    $pidarray = array('<span class="red">Inactive</span>', '-', '-', '-');
  }

  echo '<section id="webserver">'.PHP_EOL;
  echo '<h5>Web Server</h5>', PHP_EOL;
  echo '<table class="conf-table">', PHP_EOL;
  echo "<tr><td>Status:</td><td>{$pidarray[0]}</td></tr>", PHP_EOL;
  echo "<tr><td>Pid:</td><td>{$pidarray[1]}</td></tr>", PHP_EOL;
  echo "<tr><td>Started On:</td><td>{$pidarray[2]}</td></tr>", PHP_EOL;
  echo "<tr><td>Memory Used:</td><td>{$pidarray[3]} MB</td></tr>", PHP_EOL;

  echo '</table>', PHP_EOL;
  echo '</section>', PHP_EOL;
}


/**
 * Save Sink Settings
 *
 * @param str sink_url
 *
 */
function save_sink_settings($sink_url) {
  $filelines = array();

  $filelines[] = '<?php'.PHP_EOL;                         //Start PHP tag for settings
  $filelines[] = "\$notrackadmin = '{$sink_url}';".PHP_EOL;
  $filelines[] = '?>'.PHP_EOL;                            //Final line closing PHP tag

  if (file_put_contents(SETTINGS_SINK, $filelines) === false) {
    die('Unable to save settings to '.SETTINGS_SINK);
  }
}


/**
 * Update General Config
 *  Validate users inputs provided in POST request
 *
 */
function update_general_config() {
  global $config;

  $sink_url = $_POST['sinkurl'] ?? '';
  $newsearch = $_POST['search'] ?? '';
  $newwhois = $_POST['whois'] ?? '';
  $newwhois_api = $_POST['whoisapi'] ?? '';

  $sink_url = trim($sink_url);
  if ($sink_url == '') {
    $config->sink_url = '';
  }
  else {
    if (preg_match('/^https?:\/\/[\w:\-\.]{1,254}(:\d{1,5})?$/', $sink_url)) {
      $config->sink_url = $sink_url;
    }
  }

  //Validate search_engine against values in config SEARCHENGINELIST
  if (filter_string($newsearch, 16)) {
    if (array_key_exists($newsearch, $config::SEARCHENGINELIST)) {
      $config->search_engine = $newsearch;
      $config->search_url = $config::SEARCHENGINELIST[$newsearch];
    }
  }

  //Validate search_engine against values in config WHOISLIST
  if (filter_string($newwhois, 16)) {
    if (array_key_exists($newwhois, $config::WHOISLIST)) {
      $config->whois_provider = $newwhois;
      $config->whois_url = $config::WHOISLIST[$newwhois];
    }
  }

  //Only except hexadecimal values for whoisapi
  if (filter_string($newwhois_api, 48)) {
    if (ctype_xdigit($newwhois_api)) {
      $config->whois_api = $newwhois_api;
    }
    elseif ($newwhois_api == '') {
      $config->whois_api = '';
    }
  }

  save_sink_settings($sink_url);                          //Update sink_url in sinksettings
}


/**
 * Update Security Config
 *  Validate users inputs provided in POST request
 *
 */
function update_security_config() {
  global $config;

  $apikey = $_POST['apikey'] ?? '';
  $apireadonly = $_POST['apireadonly'] ?? '';

  //Carry out input validation of apikey
  if (preg_match(REGEX_VALIDAPI, $apikey)) {
    $config->api_key = $apikey;
  }

  //Carry out input validation of apireadonly
  if (preg_match(REGEX_VALIDAPI, $apireadonly)) {
    $config->api_readonly = $apireadonly;
  }
}


/**
 Main
*/
draw_page_header('Config');
draw_page_nav();

echo '<main>', PHP_EOL;
draw_general_form();
draw_security_form();
//web_section();
?>

</main>
<script>
function submitForm(toAction) {
  document.getElementById('action').value = toAction;
  document.generalform.submit()
}

/**
 * Buffer 2 Hex
 *  Convert a Uint8Array array into hexadecimal characters,
 *   including padding any single chars, e.g. 15 = f is padded to 0f
 *
 *  @param array Uint8Array to convert to hex
 *  @return Hex string
 *
 */
function buf2hex(buffer) {
  return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}


/**
 * Generate Key
 *  Use getRandomValues crypto function to generate a 20 byte array of random ints
 *   (20 byte is used as it will look like a 40 character SHA1 Hash)
 *  Appropriate textbox is filled in with hex version of the number array
 *
 * @param str Textbox name
 *
 */
function generateKey(textbox) {
  let numarray = new Uint8Array(20);                       //20 byte (40) character = SHA1

  window.crypto.getRandomValues(numarray);                 //Random 8 bit ints
  document.getElementById(textbox).value = buf2hex(numarray);
}
</script>
</body>
</html>
