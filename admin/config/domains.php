<?php
/********************************************************************

********************************************************************/
require('../include/global-vars.php');
require('../include/global-functions.php');
require('../include/config.php');
require('../include/menu.php');
require('../include/mysqlidb.php');

ensure_active_session();

/************************************************
*Constants                                      *
************************************************/

/************************************************
*Global Variables                               *
************************************************/
$dbwrapper = new MySqliDb;

/************************************************
*Arrays                                         *
************************************************/


/**
 * Draw Filter Toolbar
 *  Populates filter bar with Search Box and Block List selector.
 *
 */
function draw_filter_toolbar() {
  global $config, $dbwrapper;

  $activelist = array();                                  //Array of active block lists
  $activelist = $dbwrapper->blocklist_active();           //Get active block lists

  echo '<div class="filter-toolbar domains-filter-toolbar">', PHP_EOL;
  //1st Row - Column headers
  echo '<div><h3>Search Domain</h3></div>';
  echo '<div><h3>Block List</h3></div>';

  //2nd Row - User inputs
  echo '<div><input type="text" id="searchbox" placeholder="site.com" value="" oninput="sortBlockedTable()"></div>';

  echo '<div><select id="searchbl" onchange="sortBlockedTable()">';
  echo '<option value="all">All</option>', PHP_EOL;

  //Populate select items more than zero results have been returned from SQL query
  if ($activelist !== false) {
    foreach ($activelist as $item) {
      echo '<option value=', json_encode($item[0]), '>',
        $config->get_blocklistname($item[0]), '</option>', PHP_EOL;
    }
  }

  echo '</select></div>', PHP_EOL;                        //End block list select
  echo '</div>'.PHP_EOL;                                  //End domains-filter-toolbar
}

/**
 * Draw Table Toolbar
 *  Populates Table Toolbar with empty pagination div and export button
 *
 */
function draw_table_toolbar() {
  echo '<div class="table-toolbar">', PHP_EOL;            //Start Table Toolbar
  echo '<div class="pag-nav" id="pagination"></div>', PHP_EOL;
  //Table Toolbar
  echo '<div class="table-toolbar-options">', PHP_EOL;    //Start div for Export Button
  echo '<button type="button" class="button-grey material-icon-centre icon-export"  title="Export Table" ',
    'onclick="exportTable(\'domainsBlocked\')">&nbsp;</button>', PHP_EOL;
  echo '</div>', PHP_EOL;                                 //End div for Export Button
  echo '</div>', PHP_EOL;                                 //End Table Toolbar
}


/**
 * Setup Block List Table
 *  Carry out SQL Search for Group By Name
 *  Initialise Group By Name Table
 *  Export required constants to JavaScript
 */
function setup_blocklist_table() {
  global $config, $dbwrapper;

  $queryresult = array();

  echo '<section>', PHP_EOL;
  draw_filter_toolbar();

  $queryresult = $dbwrapper->blocklist_domains();

  if ($queryresult === false) {
    echo '<h4>&#x1F633; Oh no, your block list is empty</h4>', PHP_EOL;
    return;
  }

  draw_table_toolbar();

  //Create Block Table
  echo '<table id="block-table">', PHP_EOL;
  echo '<tr><th class="sortable active asc" onclick="sortBlockedTable(0)">#</th>',
    '<th class="sortable" onclick="sortBlockedTable(1)">Block List</th>',
    '<th class="sortable" onclick="sortBlockedTable(2)">Domain</th>',
    '<th>Comment</th></tr>', PHP_EOL;
  echo '</table>', PHP_EOL;

  //Javascript Export
  echo '<script>', PHP_EOL;                               //Begin JavaScript Exports
  echo 'const BLOCKLISTS = ', json_encode($config->blocklists), ';', PHP_EOL;
  echo 'const ORIGINDATA = ', json_encode($queryresult), ';', PHP_EOL;
  echo 'let DATA = ORIGINDATA;', PHP_EOL;
  echo 'let sortColumn = 0;', PHP_EOL;
  echo 'let sortDirection = "ASC";', PHP_EOL;
  echo 'const SEARCH_ENGINE = "', $config->search_engine, '";', PHP_EOL;
  echo 'const SEARCH_URL = "', $config->search_url, '";', PHP_EOL;

  //Setup Investigate / Whois for popupmenu (NOTE future feature)
  if ($config->whois_api == '') {
    echo 'const INVESTIGATE = "', $config->whois_provider, '";', PHP_EOL;
    echo 'const INVESTIGATE_URL = "', $config->whois_url, '";', PHP_EOL;
  }
  else {
    echo 'const INVESTIGATE = "Investigate";', PHP_EOL;
    echo 'const INVESTIGATE_URL = "./investigate.php?subdomain=";', PHP_EOL;
  }

  echo 'populateBlockedTable(1);', PHP_EOL;
  echo '</script>', PHP_EOL;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=0.9">
  <link href="../css/master.css" rel="stylesheet" type="text/css">
  <link href="../css/icons.css" rel="stylesheet" type="text/css">
  <link href="../css/tabbed.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png">
  <script src="../include/menu.js"></script>
  <script src="../include/queries.js"></script>
  <title>NoTrack - Domains Blocked</title>
</head>

<body>
<?php
/**
 Main
 */
draw_page_header('Domains Blocked');
draw_page_nav();

echo '<main>', PHP_EOL;
setup_blocklist_table();

?>
</section>
<div id="copymsg">clipboard</div>
</main>
</body>
</html>
