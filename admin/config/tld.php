<?php
/**
This PHP script allows a user to select from a predefined list of top level domains to block.
The selected domains are output to tldlist.txt, which can be read by blockparser.py
New predefined domains can be added to tldtemplate.json.
*/

require('../include/global-vars.php');
require('../include/global-functions.php');
require('../include/config.php');
require('../include/menu.php');

ensure_active_session();

/*************************************************
*Constants                                       *
*************************************************/
define('TLDJSON', '../include/tldtemplate.json');
define('TLDLIST', '../settings/tldlist.txt');
/*************************************************
*Global Variables                                *
*************************************************/
$view = 0;

/*************************************************
*Global Arrays                                   *
*************************************************/
$tld_template = array();
$usersbl = array();


/**
 * Load Users TLD Blocklist
 *   Load tldlist.txt into $usersbl
 *   Regex Groups:
 *     1. Optional # (if # is present then line commented out, therefore TLD disabled
 *     2. .tld (2 to 63 characters)
 *
 */
function load_bl() {
  global $usersbl;

  $enabled = true;
  $line = '';
  $matches = array();

  //If users TLD list doesn't exist risk levels will be used instead
  if (! file_exists(TLDLIST)) {
    return;
  }

  $fh = fopen(TLDLIST, 'r') or die('Error unable to open '.TLDLIST);
  while(! feof($fh) && ($line = fgets($fh)) !== false) {
    if (preg_match('/^(#)?(\.(?:co\.|com\.|org\.|edu\.|gov\.)?[\w\-]{2,63})/', $line, $matches)) {
      $enabled = ($matches[1] == '#') ? false : true;
      $usersbl[$matches[2]] = $enabled;
    }
  }
  fclose($fh);
}


/**
 * Update Users TLD Block List
 *   1. Loop through domains in $tld_template
 *   2a. Check if TLD name is POST requests
 *   2b. Or if missing assume unticked and therefore false
 *   3. Save filelines to tldlist.txt
 *
 */
function save_bl() {
  global $tld_template, $usersbl;

  $checkboxname = '';                                     //Actual name for the checkboxes
  $tld = '';
  $filelines = array();
  $tldinfo = array();

  foreach($tld_template as $tld => $tldinfo) {
    $checkboxname = substr($tld, 1);
    if (array_key_exists($checkboxname, $_POST)) {
      $usersbl[$tld] = true;
      $filelines[] = "{$tld} #{$tldinfo['title']}".PHP_EOL;
    }
    else {
      $usersbl[$tld] = false;
      $filelines[] = "#{$tld} #{$tldinfo['title']}".PHP_EOL;
    }
  }

  //Save prepared filelines to users tldlist.txt
  if (file_put_contents(TLDLIST, $filelines) === false) {
    die('Unable to save settings to '.TLDLIST);
  }
}


/**
 * Draw Tabbed View
 *  1. Check which tab to set as checked
 *  2. Draw the tabbed elements
 *  3. Prepare tables
 *  4. Draw tables
 *  5. Draw help page
 *
 *  @param int Tab to View (set by GET/POST argument "v")
 *
 */
function draw_tabbedview($view) {
  $tab = filter_integer($view, 1, 6, 2);
  $checkedtabs = array('', '', '', '', '', '', '');
  $checkedtabs[$tab] = ' checked';

  echo '<form name="tld" action="?" method="post">'.PHP_EOL;
  echo '<input type="hidden" name="action" value="tld">'.PHP_EOL;

  echo '<section>', PHP_EOL;
  echo '<div id="tabbed">', PHP_EOL;                      //Start tabbed container
  echo '<input type="radio" name="tabs" id="tab-nav-1"', $checkedtabs[1], '><label for="tab-nav-1">Old Generic</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-2"', $checkedtabs[2], '><label for="tab-nav-2">New Generic</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-3"', $checkedtabs[3], '><label for="tab-nav-3">Country</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-4"', $checkedtabs[4], '><label for="tab-nav-4">New Country</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-5"', $checkedtabs[5], '><label for="tab-nav-5">International</label>', PHP_EOL;
  echo '<input type="radio" name="tabs" id="tab-nav-6"', $checkedtabs[6], '><label for="tab-nav-6">Help</label>', PHP_EOL;

  $tabledata = prepare_tables();
  echo '<div id="tabs">', PHP_EOL;                        //Start tabs
  draw_table(1, 'Old Generic Domains', $tabledata['oldgeneric']);
  draw_table(2, 'New Generic Top Level Domains', $tabledata['newgeneric']);
  draw_table(3, 'Country Domains', $tabledata['country']);
  draw_table(4, 'New Country Domains', $tabledata['newcountry']);
  draw_table(5, 'International Punycode', $tabledata['international']);
  help_table();
  echo '</div>', PHP_EOL;                                 //End tabs
  echo '</div>', PHP_EOL;                                 //End tabbed container
  echo '</section>', PHP_EOL;
  echo '</form>', PHP_EOL;
}


/**
 * Draw Table
 *  Output the processed data from template into a table
 *
 *  @param int Tab View Index
 *  @param str Section Title
 *  @param array Table Data
 *
 */
function draw_table($tabview, $title, $tabledata) {
  echo '<div>', PHP_EOL;                                  //Start Tab
  echo "<h5>{$title}:</h5>", PHP_EOL;
  echo '<table class="tld-table">', PHP_EOL;
  foreach ($tabledata as $tablerow) {
    echo $tablerow, PHP_EOL;
  }
  echo '<tr><td colspan="5"><button class="material-icon-button-text icon-tick" type="submit" name="v" value="', $tabview, '">Save Changes</button></td></tr>', PHP_EOL;
  echo '</table>', PHP_EOL;
  echo '</div>', PHP_EOL;                                 //End Tab
}

/**
 * Draw Welcome
 *  Draw Welcome is called when no value has been set for GET/POST argument "v"
 *
 */
function draw_welcome() {
  echo '<section>', PHP_EOL;
  echo '<div class="bl-flex-container">', PHP_EOL;
  echo '<a href="?v=1"><img src="../svg/wordclouds/classic_wordcloud.svg" alt=""><h6>Old Generic</h6></a>', PHP_EOL;
  echo '<a href="?v=2"><img src="../svg/wordclouds/gtld_wordcloud.svg" alt=""><h6>New Generic</h6></a>', PHP_EOL;
  echo '<a href="?v=3"><img src="../svg/wordclouds/country_wordcloud.svg" alt=""><h6>Country</h6></a>', PHP_EOL;
  echo '<a href="?v=5"><img src="../svg/wordclouds/international_wordcloud.svg" alt=""><h6>International</h6></a>', PHP_EOL;
  echo '<a href="?v=6"><img src="../svg/wordclouds/help_wordcloud.svg" alt=""><h6>Help</h6></a>', PHP_EOL;
  echo '</div>', PHP_EOL;
  echo '</section>', PHP_EOL;
}


/**
 * Draw Help Table
 *
 */
function help_table() {
  echo '<div>', PHP_EOL;                                  //Start tab 6 div
  echo '<h5>Help:</h5>', PHP_EOL;
  echo '<p>NoTrack has the ability to block certain top level domains, this comes in useful against certain domains which are abused by malicious actors. New domains can be created very quickly with the purpose of hosting malware and phishing sites, which can inflict a significant amount of damage before the security community can identify and block them.</p>', PHP_EOL;
  echo '<p>Top level domains are categorised by a risk level: High, Medium, Low, and Negligible. The risk level has been taken from various sources, such as <a href="https://www.spamhaus.org/statistics/tlds/">Spamhaus</a>, <a href="https://krebsonsecurity.com/tag/top-20-shady-top-level-domains/">Krebs on Security</a>, <a href="https://www.symantec.com/blogs/feature-stories/top-20-shady-top-level-domains">Symantec</a>, as well as my own experience of dealing with Malware and Phishing campaigns in an Enterprise environment</p>', PHP_EOL;

  echo '<h6>Risk Levels:</h6>', PHP_EOL;
  echo '<span class="key flag-icon-red">High</span>', PHP_EOL;
  echo '<p>High risk domains are home to a high percentage of malicious sites compared to legitimate sites. Often websites within these domains are cheap or even free, and the domains are not well policed.<br>', PHP_EOL;
  echo '<i>Please note: High risk domains are automatically blocked, unless you specifically untick them.</i></p>', PHP_EOL;

  echo '<span class="key flag-icon-orange">Medium</span>', PHP_EOL;
  echo '<p>Medium risk domains are home to a significant number of malicious sites, but are outnumbered by legitimate sites. You may want to consider blocking these, unless you live in, or utilise the websites of the affected country.</p>', PHP_EOL;

  echo '<span class="key flag-icon-yellow">Low</span>'.PHP_EOL;
  echo '<p>Low risk may still house some malicious sites, but they are vastly outnumbered by legitimate sites.</p>', PHP_EOL;

  echo '<span class="key flag-icon-green">Negligible</span>'.PHP_EOL;
  echo '<p>These domains are not open to the public, and therefore extremely unlikely to contain malicious sites.</p>', PHP_EOL;

  echo '</div>', PHP_EOL;                                 //End tab 6 div

}

/**
 * Prepare Tables
 *  Append formatted row HTML code to the table array specified by the type value
 *
 *  @return array of arrays containing HTML table code
 */
function prepare_tables() {
  global $tld_template;

  //Array of block lists types in order to build the HTML tables
  $tabledata = array(
    'oldgeneric' => array(),
    'newgeneric' => array(),
    'country' => array(),
    'newcountry' => array(),
    'international' => array(),
  );

  foreach($tld_template as $tld => $tldinfo) {
    //Append formatted row HTML code to the table array specified by the type value
    $tabledata[$tldinfo['type']][] = tldlist_row($tld, $tldinfo);
  }

  return $tabledata;
}


/**
 * Format a table row for block lists
 *  @param string TLD
 *  @param array JSON data for the TLD
 *  @return string HTML formtted table row
 */
function tldlist_row($tld, $tldinfo) {
  global $usersbl;

  $checked = '';
  $checkbox = '';
  $checkboxname = '';
  $desc = '';
  $flag = '';
  $flagclass = '';
  $flagword = '';
  $row = '';
  $title = '';

  //Get Enabled status
  //Firstly check users settings
  if (array_key_exists($tld, $usersbl)) {
    $enabled = $usersbl[$tld];
  }
  //Otherwise fallback to risk value
  elseif ($tldinfo['risk'] == 1) {
    $enabled = true;                                    //Risk 1 enabled by default
  }
  else {
    $enabled = false;                                   //Risk 2-5 disabled by default
  }

  //Set flag background class based on the risk value
  switch ($tldinfo['risk']) {
    case 1: $flagclass = 'flag-icon-red'; break;
    case 2: $flagclass = 'flag-icon-orange'; break;
    case 3: $flagclass = 'flag-icon-yellow'; break;
    case 5: $flagclass = 'flag-icon-green'; break;
  }
  $flagword = $tldinfo['flag'] ?? substr($tldinfo['title'], 0, 2);
  $flag = "<div class=\"flag-icon {$flagclass}\">{$flagword}</div>";

  //Set checkbox checked value then name - public suffix TLD without the leading dot
  //NOTE removing the leading dot prevents browser from malforming the checkbox name
  $checked = ($enabled) ? ' checked="checked"' : '';      //Set checkbox ticked value
  $checkboxname = substr($tld, 1);
  $checkbox = "<input type=\"checkbox\" name=\"{$checkboxname}\"{$checked}>";

  //Set title and description
  $title = $tldinfo['title'];
  $desc = $tldinfo['desc_en'];

  $row = "<tr><td>{$flag}</td><td>{$checkbox}</td><td>{$tld}</td><td>{$title}</td><td>{$desc}</td>";

  return $row;
}


$tld_template = load_json(TLDJSON);

//Deal with POST actions first, that way we can reload the page and remove POST requests from browser history.
if ((isset($_POST['action'])) && (isset($_POST['v']))) {
  save_bl();
  usleep(250000);                                          //Prevent race condition
  header('Location: ?v='.$_POST['v']);                     //Reload page
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link href="../css/master.css" rel="stylesheet" type="text/css">
  <link href="../css/flags.css" rel="stylesheet" type="text/css">
  <link href="../css/icons.css" rel="stylesheet" type="text/css">
  <link href="../css/tabbed.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png">
  <script src="../include/menu.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=0.9">
  <title>NoTrack - Top Level Domains</title>
</head>

<body>
<?php
draw_page_header('TLDs');
draw_page_nav();

echo '<main>', PHP_EOL;
load_bl();                                                 //Load users block list

if (isset($_GET['v'])) {
  draw_tabbedview($_GET['v']);
}
else {
  draw_welcome();
}
?>

</main>
</body>
</html>
