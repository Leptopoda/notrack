<?php
require('../include/global-vars.php');
require('../include/global-functions.php');
require('../include/config.php');
require('../include/menu.php');
require('../include/mysqlidb.php');

ensure_active_session();

/************************************************
*Constants                                      *
************************************************/
define('DNSLIST', ['dnsmasq', 'bind']);
define('OPTIONSLIST', [
  'Custom' => array('', '', '', ''),
  'AlternateDNS' => array('76.76.19.19', '76.223.122.150', '2602:fcbc::ad', '2602:fcbc:2::ad'),
  'Cloudflare' => array('1.1.1.1', '1.0.0.1', '2606:4700:4700::1111', '2606:4700:4700::1001'),
  'DNS.Watch' => array('84.200.69.80', '84.200.70.40', '2001:1608:10:25::1c04:b12f', '2001:1608:10:25::9249:d69b'),
  'Google' => array('8.8.8.8', '8.8.4.4', '2001:4860:4860::8888', '2001:4860:4860::8844'),
  'OpenDNS' => array('208.67.222.222', '208.67.220.220', '2620:0:ccc::2', '2620:0:ccd::2'),
  'Quad9' => array('9.9.9.9', '149.112.112.112', '2620:fe::fe', '2620:fe::9'),
  'Verisign' => array('64.6.64.6', '64.6.65.6', '2620:74:1b::1:1', '2620:74:1c::2:2'),
  'Yandex' => array('77.88.8.88', '77.88.8.2', '2a02:6b8::feed:bad', '2a02:6b8:0:1::feed:bad')
  ]);

/************************************************
*Global Variables                               *
************************************************/
$dbwrapper = new MySqliDb();


/**
 * Show DNS Server Section
 *  1. Find running dns server from DNSLIST using ps
 *  2. Split result of ps into an $pidarray using delimiter of one or more spaces
 *  3. $pidarray elements:
 *     0 - Process
 *     1 - PID
 *     2 - Date Opened
 *     3 - Memory Usage
 *
 */
function dns_status() {
  global $dbwrapper;

  $pidstr = '';
  $pidarray = array();

  foreach (DNSLIST AS $app) {
    $pidstr = exec("ps -eo fname,pid,stime,pmem | grep $app");

    //Has valid process been found?
    if ($pidstr != '') {
      $pidarray = preg_split('/\s+/', $pidstr);            //Explode into array
      $pidarray[0] = ucfirst($pidarray[0]).' is Active';   //Prettify process name
      break;
    }
  }

  //Fallback if no process hasn't been found
  if ($pidstr == '') {
    $pidarray = array('<span class="red">Inactive</span>', '-', '-', '-');
  }

  echo '<section id="dns">', PHP_EOL;
  echo '<h5>DNS Server Status</h5>', PHP_EOL;
  echo '<table class="conf-table">', PHP_EOL;
  echo "<tr><td>Status</td><td>{$pidarray[0]}</td></tr>", PHP_EOL;
  echo "<tr><td>Pid</td><td>{$pidarray[1]}</td></tr>", PHP_EOL;
  echo "<tr><td>Started On</td><td>{$pidarray[2]}</td></tr>", PHP_EOL;
  echo "<tr><td>Memory Used</td><td>{$pidarray[3]} MB</td></tr>", PHP_EOL;
  echo '<tr><td>Historical Logs</td><td>', $dbwrapper->queries_historical_days(), ' Days</td></tr>', PHP_EOL;
  echo '<tr><td>DNS Queries</td><td>', number_format($dbwrapper->count_total_queries()), '</td></tr>', PHP_EOL;
  echo '</table>', PHP_EOL;
  echo '</section>', PHP_EOL;
}


/**
 *  Draw Form
 *    DNS Form
 *
 */
function draw_form() {
  global $config;

  $help = '';
  $ipbox1 = '';
  $ipbox2 = '';
  $ipoptions = '';
  $ipversion = '';

  $ipoptions = menu_serveroptions($config->dns_server);
  $ipversion = menu_ipoptions($config->dns_serverip1);

  $ipbox1 = '<input type="text" name="serverIP1" id="serverIP1" value="'.$config->dns_serverip1.'" placeholder="208.67.222.222" pattern="[\da-fA-F:\.]+"><i> Primary Server</i>';
  $ipbox2 = '<input type="text" name="serverIP2" id="serverIP2" value="'.$config->dns_serverip2.'" placeholder="208.67.220.220" pattern="[\da-fA-F:\.]+"><i>Secondary Server</i>';

  echo '<form method="POST">', PHP_EOL;
  
  echo '<section>', PHP_EOL;
  echo '<h5>DNS Server Config</h5>', PHP_EOL;
  echo '<table class="conf-table">', PHP_EOL;

  echo '<tr><td>NoTrack Server Name</td><td><input type="text" name="dnsName" id="dnsName" value="', $config->dns_name, '" placeholder="notrack.local" pattern="[\w\-]{1,63}\.[\w\-\.]{2,253}"></td></tr>', PHP_EOL;

  echo "<tr><td>DNS Server</td><td>{$ipoptions}<br>{$ipbox1} {$ipbox2}</td></tr>", PHP_EOL;
  echo "<tr><td>IP Version</td><td>{$ipversion}</td></tr>", PHP_EOL;

  $help = '<i>Network interface that your DNS Server will listen on.</i>';
  echo '<tr><td>Listening Interface</td><td><input type="text" name="listenInterface" id="listenInterface" value="', $config->dns_interface, '" placeholder="eth0" pattern="\w+">', $help, '</td></tr>', PHP_EOL;

  $help = '<i>Local host IP will be either 127.0.0.1 or ::1</i>';
  echo '<tr><td>Listening IP</td><td><input type="text" name="listenIP" id="listenIP" value="', $config->dns_listenip, '" placeholder="127.0.0.1" pattern="[\da-fA-F:\.]+">', $help, '</td></tr>', PHP_EOL;

  $help = '<i>DNS Servers normally listen on Port 53 or 5353.</i>';
  echo '<tr><td>Listening Port</td><td><input type="number" name="listenPort" id="listenPort" value="', $config->dns_listenport, '" placeholder="53" min="1" max="65535">', $help, '</td></tr>', PHP_EOL;

  $help = '<i>Use local host IP, unless NoTrack is on a VPN device.</i>';
  echo '<tr><td>Block Page IP</td><td><input type="text" name="blockIP" id="blockIP" value="', $config->dns_blockip, '" placeholder="192.168.0.2" pattern="[\da-fA-F:\.]+">', $help, '</td></tr>', PHP_EOL;

  $help = '<i>Number of days to retain DNS logs for (use zero to retain forever).</i>';
  echo '<tr><td>Log Retention</td><td><input type="number" name="logRetention" id="logRetention" value="', $config->dns_logretention, '" placeholder="60" min="0" max="366" title="Log retention in days">', $help, '</td></tr>', PHP_EOL;

  echo '<tr><td colspan="2"><button class="material-icon-button-text icon-tick" type="submit">Save Changes</button></td></tr>', PHP_EOL;
  
  echo '</table>', PHP_EOL;
  echo '</section>', PHP_EOL;
  echo '</form>', PHP_EOL;
}


/**
 * Menu IP Version Options
 *  Generates two selectable radio buttons for IP version and selects either
 *  IPv4 or IPv6 based on the current DNS server IP
 *
 *  @param str IP address for a DNS server
 *  @return HTML code for radio buttons
 */
function menu_ipoptions($ip) {
  $ipv4checked = '';
  $ipv6checked = '';
  $str = '';

  //Is the current server IP an IPv6?
  if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
    $ipv6checked = ' checked="checked"';
  }
  //Otherwise assume IPv4
  else {
    $ipv4checked = ' checked="checked"';
  }

  $str .= '<label for="ipv4">IPv4<input type="radio" id="ipv4" name="ipVersion" value="ipv4" onclick="setIP()"'.$ipv4checked.'></label>';
  $str .= '<label for="ipv6">IPv6<input type="radio" id="ipv6" name="ipVersion" value="ipv6" onclick="setIP()"'.$ipv6checked.'></label>';

  return $str;
}


/**
 * Menu Server Options
 *  Generate a select menu of DNS servers based on OPTIONSLIST
 *
 *  @param str current $config->dns_server
 *  @return HTML code for select menu
 */
function menu_serveroptions($dnsserver) {
  $current = '';
  $selcted = '';
  $str = '';

  //Make sure the current selection is in OPTIONSLIST, set to Custom if it isn't
  $current = (array_key_exists($dnsserver, OPTIONSLIST)) ? $dnsserver : 'Custom';

  $str = '<select id="serverName" name="serverName" onchange="setIP()">';

  foreach (OPTIONSLIST as $key => $value) {
    $selected = ($key == $current) ? ' selected' : '';
    $str .= "<option value=\"{$key}\"{$selected}>{$key}</option>";
  }

  $str .= '</select>';

  return $str;
}


/**
 * Save Changes
 *  Get values from POST
 *  Carry out input validation and assign values to config
 *
 */
function save_changes() {
  global $config;

  $blockip = $_POST['blockIP'] ?? '';
  $dnsname = $_POST['dnsName'] ?? '';
  $listeninterface = $_POST['listenInterface'] ?? '';
  $listenip = $_POST['listenIP'] ?? '';
  $listenport = $_POST['listenPort'] ?? 53;
  $logretention = $_POST['logRetention'] ?? 60;
  $serverip1 = $_POST['serverIP1'] ?? '';
  $serverip2 = $_POST['serverIP2'] ?? '';
  $server = $_POST['serverName'] ?? '';

  $config->dns_blockip = filter_ip($blockip, $config->dns_blockip);
  $config->dns_interface = sanitize_string($listeninterface, $config->dns_interface, 255);
  $config->dns_listenip = filter_ip($listenip, $config->dns_listenip);
  $config->dns_listenport = filter_integer($listenport, 0, 65535, 53);
  $config->dns_logretention = filter_integer($logretention, 0, 366, 60);
  $config->dns_name = filter_domain($dnsname, false, $config->dns_name);
  $config->dns_server = sanitize_string($server, $config->dns_server, 255);
  $config->dns_serverip1 = filter_ip($serverip1, $config->dns_serverip1);
  $config->dns_serverip2 = filter_ip($serverip2, $config->dns_serverip2);

  $config->save_serversettings();
}

/**
 Main
*/

load_serversettings();

//Anything in POST to process?
if (sizeof($_POST) > 0) {
  save_changes();
  sleep(3);
  header('Location: '.$_SERVER['PHP_SELF']);
  exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link href="../css/master.css" rel="stylesheet" type="text/css">
  <link href="../css/icons.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png">
  <script src="../include/menu.js"></script>
  <script src="../include/customblocklist.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=0.9">
  <title>NoTrack - DNS Setup</title>
</head>
<body>
<?php

draw_page_header('DNS Server');
draw_page_nav();

echo '<main>', PHP_EOL;

draw_form();
dns_status();

?>
</main>
<script>

/**
 * Set IP addresses in serverIP1 and serverIP2 text boxes based on user selected serverName and IP ver
 *
 */
function setIP() {
  const serverList = <?php echo json_encode(OPTIONSLIST);?>

  let selectedIPv4 = false;
  let selectedIPv6 = false;
  let selectedServer = '';

  //Get user selections
  selectedServer = document.getElementById('serverName').value;
  selectedIPv4 = document.getElementById('ipv4').checked;
  selectedIPv6 = document.getElementById('ipv6').checked;

  //Check if serverName is in serverList object
  if (! serverList.hasOwnProperty(selectedServer)) {
    console.warn('Invalid server selected');
    return;
  }

  if (selectedIPv4) {
    document.getElementById('serverIP1').value = serverList[selectedServer][0];
    document.getElementById('serverIP2').value = serverList[selectedServer][1];
  }
  else if (selectedIPv6) {
    document.getElementById('serverIP1').value = serverList[selectedServer][2];
    document.getElementById('serverIP2').value = serverList[selectedServer][3];
  }
  else {
    console.warn('Invalid IP version selected');
    return;
  }
}
</script>
</body>
</html>
