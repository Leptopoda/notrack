<?php
/*A different view is displayed depending how user enters this page
* 1. No arguments set - Show searchbox
* 2. Domain set - display whois, and graph
* 3. Domain and time set - display surrounding queries, whois, and graph
*/
require('./include/global-vars.php');
require('./include/global-functions.php');
require('./include/config.php');
require('./include/menu.php');
require('./include/whoisapi.php');

ensure_active_session();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link href="./css/master.css" rel="stylesheet" type="text/css">
  <link href="./css/chart.css" rel="stylesheet" type="text/css">
  <link href="./css/icons.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="./favicon.png">
  <script src="./include/menu.js"></script>
  <script src="./include/queries.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=0.9">
  <title>NoTrack - Investigate</title>
</head>

<body>
<?php
draw_page_header('Investigate');
draw_page_nav();
echo '<main>', PHP_EOL;

/************************************************
*Global Variables                               *
************************************************/
$datetime = '';
$domain = '';                                              //site.com
$subdomain = '';                                           //subdomain.site.com
$forceupdate = false;                                      //Get latest info about a domain
$showraw = false;                                          //Raw view or fancy view
$sys = '';

/************************************************
*Arrays                                         *
************************************************/

/********************************************************************
 *  Create Who Is Table
 *    Run sql query to create whois table
 *  Params:
 *    None
 *  Return:
 *    None
 */
function create_whoistable() {
  global $db;

  $query = "CREATE TABLE whois (id SERIAL, save_time DATETIME, domain TINYTEXT, record MEDIUMTEXT)";

  $db->query($query);
}


/**
 *  Draw Filter Toolbar
 *
 */
function draw_filter_toolbar() {
  global $subdomain;

  echo '<form method="GET">', PHP_EOL;
  echo '<div class="filter-toolbar domains-filter-toolbar">', PHP_EOL;
  echo '<div><h3>Search Domain</h3></div>', PHP_EOL;
  echo '<div>&nbsp;</div>', PHP_EOL;
  echo '<div><input type="text" name="subdomain" class="input-conf" placeholder="site.com" value="', $subdomain, '"></div>', PHP_EOL;
  echo '<div><button class="material-icon-button-text icon-cloud-search" type="submit">Investigate</button></div>', PHP_EOL;
  echo '</div>', PHP_EOL;
  echo '</form>', PHP_EOL;
}


/**
 * Draw Search Box
 *  @note this is an alternate view when no search has been given
 *
 */
function draw_searchbox() {
  global $subdomain;

  echo '<div id="search-box"><div>'.PHP_EOL;
  echo '<form method="GET">'.PHP_EOL;
  echo '<input type="text" name="subdomain" placeholder="Search domain" value="'.$subdomain.'">&nbsp;'.PHP_EOL;
  echo '<button class="material-icon-button-text icon-cloud-search" type="submit">Investigate</button>'.PHP_EOL;
  echo '</form>'.PHP_EOL;
  echo '</div></div>'.PHP_EOL;
}


/******************************************************************
 *  Extract Emails from Raw $whois_record
 *    Regex match for Registrant, Abuse, and Tech emails
 *    Don't add Tech email if it matches Registrant
 *
 *  Params:
 *    Jsondata['raw'] string
 *  Return:
 *    Array of emails found on success
 *    or false if nothing found
 */
function extract_emails($raw) {
  $abuse = '';
  $registrant = '';
  $tech = '';
  $emails = array();


  if (preg_match('/Registrant\sEmail:\s([\w\.\-\_\+]+@[\w\.\-\_\+]+)/', $raw, $matches) > 0) {
    $registrant = strtolower($matches[1]);
    $emails['Registrant Email'] = $registrant;
  }

  if (preg_match('/Abuse\sContact\sEmail:\s([\w\.\-\_\+]+@[\w\.\-\_\+]+)/', $raw, $matches) > 0) {
    $abuse = strtolower($matches[1]);
    $emails['Abuse Contact'] = $abuse;
  }

  if (preg_match('/Tech\sEmail:\s([\w\.\-\_\+]+@[\w\.\-\_\+]+)/', $raw, $matches) > 0) {
    $tech = strtolower($matches[1]);
    if ($tech != $registrant) $emails['Tech Email'] = $tech;
  }

  if (count($emails) == 0) return false;

  return $emails;
}


/**
 * Setup Group By Query Time Table
 *  Carry out SQL Search for Group By Time
 *  Initialise Group By Name Table
 *  Export required constants to JavaScript
 */
function setup_investigate_table() {
  global $config, $db, $datetime, $subdomain, $sys;

  $query = '';

  $query = "SELECT * FROM dnslog WHERE sys = '$sys' AND log_time > SUBTIME('$datetime', '00:00:04') AND log_time < ADDTIME('$datetime', '00:00:03') ORDER BY log_time DESC LIMIT 100";

  $result = $db->query($query);

  if (! $result) {
    echo '<h4><img src="./svg/emoji_sad.svg" alt="">Error running query</h4>'.PHP_EOL;
    echo $db->error, '</div></main>', PHP_EOL;
    die();
  }

  if ($result->num_rows == 0) {                           //Leave if nothing found
    $result->free();
    echo '<h4><img src=./svg/emoji_sad.svg>No results found</h4>'.PHP_EOL;
    return;
  }

  echo '<table id="query-time-table">', PHP_EOL;          //Begin Time Table
  echo '<tr><th>&nbsp;</th><th>Time</th><th>System</th><th>Domain</th><th></th></tr>'.PHP_EOL;
  echo '</table>', PHP_EOL;                               //End Time Table

  echo '<script>', PHP_EOL;                               //Begin JavaScript Exports
  echo 'const BLOCKLISTS = ', json_encode($config->blocklists), ';', PHP_EOL;
  echo 'const DATA = ', json_encode($result->fetch_all()), ';', PHP_EOL;
  echo 'const SEARCH_ENGINE = "', $config->search_engine, '";', PHP_EOL;
  echo 'const SEARCH_URL = "', $config->search_url, '";', PHP_EOL;

  //Setup Investigate / Whois for popupmenu
  if ($config->whois_api == '') {
    echo 'const INVESTIGATE = "', $config->whois_provider, '";', PHP_EOL;
    echo 'const INVESTIGATE_URL = "', $config->whois_url, '";', PHP_EOL;
  }
  else {
    echo 'const INVESTIGATE = "Investigate";', PHP_EOL;
    echo 'const INVESTIGATE_URL = "./investigate.php?subdomain=";', PHP_EOL;
  }

  echo "populateInvestigateTable('$subdomain');", PHP_EOL;
  echo '</script>', PHP_EOL;
  echo '</section>', PHP_EOL;

  $result->free();
}


/********************************************************************
 *  Show Who Is Data
 *    Displays data from $whois_record
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function show_whoisdata($whois_date, $whois_record) {
  global $config, $subdomain;
  
  //$blockreason = '';
  //$notrack_row = '';

  if ($whois_record == null) {
    echo '<h4>&#x1F615; No data in Who Is record</h4>', PHP_EOL;
    echo '</section>';
    return;
  }
  if (isset($whois_record['error'])) {
    echo '<h5>Domain Information</h5>'.PHP_EOL;
    echo $whois_record['error'].PHP_EOL;
    echo '</secction>'.PHP_EOL;
    return;
  }

  $emails = extract_emails($whois_record['raw']);
  /*$blockreason = search_blockreason($subdomain);
  if ($blockreason != '') {
    $notrack_row = 'Blocked by '.$config->get_blocklistname($blockreason);
  }
  else {
    $notrack_row = 'Allowed';
  }*/

  echo '<h5>Domain Information</h5>'.PHP_EOL;
  echo '<table class="conf-table">'.PHP_EOL;
  draw_sysrow('Domain Name', $whois_record['domain'].'<span class="investigatelink"><a href="?subdomain='.$subdomain.'&amp;v=raw">View Raw</a></span>');
  //draw_sysrow('Status on NoTrack', $notrack_row);
  draw_sysrow('Created On', substr($whois_record['created_on'], 0, 10));
  draw_sysrow('Updated On', substr($whois_record['updated_on'], 0, 10));
  draw_sysrow('Expires On', substr($whois_record['expires_on'], 0, 10));
  draw_sysrow('Status', ucfirst($whois_record['status']));
  draw_sysrow('Registrar', $whois_record['registrar']['name']);
  
  if ($emails !== false) {
    foreach ($emails as $key => $value) {
      draw_sysrow($key, $value);
    }
  }
  
  if (isset($whois_record['nameservers'][0])) draw_sysrow('Name Servers', $whois_record['nameservers']['0']['name']);
  if (isset($whois_record['nameservers'][1])) draw_sysrow('', $whois_record['nameservers']['1']['name']);
  if (isset($whois_record['nameservers'][2])) draw_sysrow('', $whois_record['nameservers']['2']['name']);
  if (isset($whois_record['nameservers'][3])) draw_sysrow('', $whois_record['nameservers']['3']['name']);
  draw_sysrow('Last Retrieved', $whois_date.'<span class="investigatelink"><a href="?subdomain='.$subdomain.'&amp;update">Get Latest</a></span>');
  echo '</table>', PHP_EOL;
  echo '</section>', PHP_EOL;

  /*if (isset($whois_record['registrant_contacts'][0])) {
    draw_systable('Registrant Contact');
    draw_sysrow('Name', $whois_record['registrant_contacts']['0']['name']);
    draw_sysrow('Organisation', $whois_record['registrant_contacts']['0']['organization']);
    draw_sysrow('Address', $whois_record['registrant_contacts']['0']['address']);
    draw_sysrow('City', $whois_record['registrant_contacts']['0']['city']);
    draw_sysrow('Postcode', $whois_record['registrant_contacts']['0']['zip']);
    if (isset($whois_record['registrant_contacts'][0]['state'])) draw_sysrow('State', $whois_record['registrant_contacts']['0']['state']);
    draw_sysrow('Country', $whois_record['registrant_contacts']['0']['country']);
    if (isset($whois_record['registrant_contacts'][0]['phone'])) draw_sysrow('Phone', $whois_record['registrant_contacts']['0']['phone']);
    if (isset($whois_record['registrant_contacts'][0]['fax'])) draw_sysrow('Fax', $whois_record['registrant_contacts']['0']['fax']);
    if (isset($whois_record['registrant_contacts'][0]['email'])) draw_sysrow('Email', strtolower($whois_record['registrant_contacts']['0']['email']));
    echo '</table></div>'.PHP_EOL;
  }*/

}


/********************************************************************
 *  Show Raw Who Is Data
 *    Displays contents of raw item in $whois_record
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function show_rawwhoisdata($whois_record) {
  if ($whois_record == null) return null;                  //Any data in the array?

  if (isset($whois_record['error'])) {
    echo '<section>', PHP_EOL;
    echo '<h5>Domain Information</h5>'.PHP_EOL;
    echo $whois_record['error'].PHP_EOL;
    echo '</section>'.PHP_EOL;
    return null;
  }
  
  echo '</section>'.PHP_EOL;
  echo '<pre>'.PHP_EOL;
  echo $whois_record['raw'];
  echo '</pre>'.PHP_EOL;
}


/**
 * Show Who Is Error when no API has been set
 *  NOTE PHP execution stops after this function, so body and html close tags are required
 *
 */
function show_whoiserror() {
  echo '<section>', PHP_EOL;
  echo '<h5>Domain Information</h5>', PHP_EOL;
  echo '<p>In order to use this feature you will need to add a valid JsonWhois API key to NoTrack config</p>', PHP_EOL;
  echo '<p>Instructions:</p>', PHP_EOL;
  echo '<ol>', PHP_EOL;
  echo '<li>Sign up to <a href="https://jsonwhois.com/">JsonWhois.com</a></li>', PHP_EOL;
  echo '<li> Add your API key to NoTrack <a href="./config.php?v=general">Config</a></li>', PHP_EOL;
  echo '</ol>', PHP_EOL;
  echo '</section>', PHP_EOL;
  echo '</main>', PHP_EOL;
  echo '</body>', PHP_EOL;
  echo '</html>', PHP_EOL;
}


/********************************************************************
 *  Count Queries
 *    1. log_date by day, severity for site for past 30 days count and grouped by day
 *    2. Create associative array for each day (to account for days when no queries are made
 *    3. Copy known count values into associative array
 *    4. Move associative values into index array
 *    5. Draw line chart
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function count_queries() {
  global $db, $domain, $subdomain;

  $allowed_arr = array();
  $blocked_arr = array();
  $chart_labels = array();
  $link_labels = array();
  $currenttime = 0;
  $datestr = '';
  $query = '';

  $currenttime = time();

  $starttime = strtotime('-30 days');
  $endtime = strtotime('+1 days');

  if ($domain != $subdomain) {
    $query = "SELECT date_format(log_time, '%m-%d') as log_date, severity, COUNT(1) as count FROM dnslog WHERE dns_request LIKE '%$domain' GROUP BY severity, log_date";
  }
  else {
    $query = "SELECT date_format(log_time, '%m-%d') as log_date, severity, COUNT(1) as count FROM dnslog WHERE dns_request LIKE '%$subdomain' GROUP BY severity, log_date";
  }

  if(!$result = $db->query($query)){
    echo '<h4><img src=./svg/emoji_sad.svg>Error running query</h4>'.PHP_EOL;
    echo 'count_queries: '.$db->error;
    echo '</div>'.PHP_EOL;
    die();
  }

  for ($i = $starttime; $i < $endtime; $i += 86400) {      //Increase by 1 day from -30 days to today
    $datestr = date('m-d', $i);
    $allowed_arr[$datestr] = 0;
    $blocked_arr[$datestr] = 0;
    $chart_labels[] = $datestr;
    $link_labels[] = date('Y-m-d\T00:00:00', $i);
  }

  if ($result->num_rows == 0) {                            //Leave if nothing found
    $result->free();
    //echo '<h4><img src=./svg/emoji_sad.svg>No results found</h4>'.PHP_EOL;
    return false;
  }

  while($row = $result->fetch_assoc()) {                   //Read each row of results

    if (! array_key_exists($row['log_date'], $allowed_arr)) continue;

    if ($row['severity'] == 1) {
      $allowed_arr[$row['log_date']] = $row['count'];
    }
    else {
      $blocked_arr[$row['log_date']] = $row['count'];
    }
  }

  $result->free();

  linechart(array_values($allowed_arr), array_values($blocked_arr), $chart_labels, $link_labels, '/P1D&amp;searchbox=*'.$domain, 'Queries over past 30 days');   //Draw the line chart
}

/********************************************************************
 *Main
 */
$db = new mysqli(SERVERNAME, USERNAME, PASSWORD, DBNAME);  //Open MariaDB connection

if (isset($_GET['sys'])) {                                 //Any system set?
  if (filter_var($_GET['sys'], FILTER_VALIDATE_IP)) {
    $sys = $_GET['sys'];                                   //Just check for valid IP rather than if system is in dnslog
  }
}

if (isset($_GET['datetime'])) {                            //Filter for hh:mm:ss
  if (preg_match(REGEX_DATETIME, $_GET['datetime']) > 0) {
    $datetime = $_GET['datetime'];
  }
}

if (isset($_GET['subdomain'])) {
  if (filter_domain($_GET['subdomain'], false, false) !== false) {
    $subdomain = trim($_GET['subdomain']);
    $domain = extract_domain($subdomain);
  }
}

if (isset($_GET['update'])) {
  $forceupdate = true;
}

if (isset($_GET['v'])) {
  if ($_GET['v'] == 'raw') {
    $showraw = true;
  }
}

if (!table_exists('whois')) {                              //Does whois sql table exist?
  create_whoistable();                                     //If not then create it
  sleep(1);                                                //Delay to wait for MariaDB to create the table
}

if ($config->whois_api == '') {                            //Has user set an API key?
  show_whoiserror();                                       //No - Don't go any further
  $db->close();
  exit;
}

if ($domain == '') {                                       //No domain set, just show searchbox
  draw_searchbox();
}
else {                                                     //Load whois data?
  echo '<section>', PHP_EOL;
  draw_filter_toolbar();

  $whois = new WhoisApi($config->whois_api, $domain);
  if ($datetime != '') {                                   //Show queries time table if datetime in parameters
    setup_investigate_table();
    echo '<section>'.PHP_EOL;
  }

  if ($forceupdate) {                                      //Are we deleting the old record?
    $whois->delete_whoisrecord();
  }
  if (! $whois->search_whoisrecord()) {                    //Attempt to search whois table
    $whois->get_whoisdata(); //No record found - download it from JsonWhois
  }


  if ($showraw) {                                          //Show basic raw view?
    show_rawwhoisdata($whois->jsondata);
  }
  else {                                                   //Display fancy whois data
    show_whoisdata($whois->download_date, $whois->jsondata);
    count_queries();                                       //Show log data for last 30 days
  }
}

$db->close();

?>
<div id="copymsg">clipboard</div>
</main>
<div id="report-dialog">
<h2>Report Domain</h2>
<form action="https://quidsup.net/notrack/report.php" method="post" target="_blank">
<input type="hidden" name="site" id="reportInput" value="none">
<fieldset>
<legend id="reportTitle">domain</legend>
<input type="text" name="comment" placeholder="Optional comment">
</fieldset>
<fieldset>
<button type="submit">Confirm</button>
<button type="button" class="button-grey" onclick="hideDialogs()">Cancel</button>
</fieldset>
</form>
</div>

<div id="queries-dialog">
<h2 id="blocktitle">Block Domain</h2>
<form action="./config/customblocklist.php" method="POST" target="_blank">
<input type="hidden" name="action" id="blockAction" value="block">
<input type="hidden" name="comment" value="">
<fieldset id="blockitem1"></fieldset>
<fieldset id="blockitem2"></fieldset>
</form>
<fieldset>
<button type="button" class="button-grey" onclick="hideDialogs()">Cancel</button>
</fieldset>
</div>
<div id="fade" onclick="hideDialogs()"></div>
</body>
</html>
