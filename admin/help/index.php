<?php
require('../include/global-vars.php');
require('../include/global-functions.php');
require('../include/config.php');
require('../include/menu.php');

ensure_active_session();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link href="../css/master.css" rel="stylesheet" type="text/css">
  <link href="../css/icons.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png">
  <script src="../include/menu.js"></script>
  <title>NoTrack Help</title>
</head>

<body>
<?php
//-------------------------------------------------------------------
function LoadHelpPage($Page) {
  if (file_exists($Page.'.html')) {
    echo file_get_contents($Page.'.html');
  }
  else {
    echo 'Error: File not found'.PHP_EOL;
  }
}
//-------------------------------------------------------------------
draw_page_header('Help');
draw_page_nav();
echo '<main>', PHP_EOL;

if (isset($_GET['p'])) {
  switch($_GET['p']) {
    case 'position':
      LoadHelpPage('position');
      break;
    case 'newblocklist':
      LoadHelpPage('newblocklist');
      break;
    case 'security':
      LoadHelpPage('security');
      break;
    default:
      LoadHelpPage('list');
  }
}
else {
  LoadHelpPage('list');
}
?>
</main>
</body>
</html>
