<?php
require('./include/global-vars.php');
require('./include/global-functions.php');
require('./include/config.php');
require('./include/menu.php');

ensure_active_session();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=0.8">
  <link href="./css/master.css" rel="stylesheet" type="text/css">
  <link href="./css/icons.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="./favicon.png">
  <script src="./include/menu.js"></script>
  <script src="./include/queries.js"></script>
  <title>NoTrack - DNS Queries</title>
</head>

<body>
<?php
draw_page_header('DNS Queries');
draw_page_nav();
echo '<main>', PHP_EOL;

/************************************************
*Constants                                      *
************************************************/
define('DEF_SYSTEM', 'all');
define('GROUPLIST', array('queriesDomain', 'queriesTime'));

/************************************************
*Global Variables                               *
************************************************/
//Date Time Default range = Past 1 Day
$dt = array('search' => 'P1D', 'sql' => '', 'textbox' => '', 'start' => '', 'end'=> '');
$groupby = '';
$searchquery = '';
$sort = 'DESC';
$searchip = DEF_SYSTEM;
$searchseverity = 0;

//Date Time Duration from ISO 8601
//NOTE: PHP allows the values to be higher than the normal time period e.g 90S (seconds) instead of 1M30S (1 minute - 30 seconds)
//Start with P
//Lookahead to see if next letter is T and a number or a number
//Group 1: 1-9 Years (optional)
//Group 2: 1-99 Months (optional)
//Group 3: 1-999 Days (optional)
//Non-Capture Group for optional T (Time component)
//Group 4: 1-999 Hours (optional)
//Group 5: 1-999 Minutes (optional)
//Group 6: 1-999 Seconds (optional)
define('REGEX_DTDURATION', '/^P(?=T\d|\d)(\dY)?(\d{1,2}M)?(\d{1,3}D)?(?:T(\d{1,3}H)?(\d{1,3}M)?(\d{1,3}S)?)?$/');

//Date Time Single Y-M-DTh:m:s
//0000-9999 Years
//Non-Capture Group 10-12 or 0-9 Months
//Non-Capture Group 30-31 or 0 1-9 or 1-2 0-9 Days
//T
//Non-Capture Group 2 0-3 or 0-1 0-9 Hours
//0-5 0-9 Minutes
//0-5 0-9 Seconds
define('REGEX_DTSINGLE', '/^[0-9]{4}\-(?:1[0-2]|0[1-9])\-(?:3[01]|0[1-9]|[12][0-9])T(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$/');

//Date Time Range - Two ranges sepeareted by a /
define('REGEX_DTRANGE', '/^([0-9]{4}\-(?:1[0-2]|0[1-9])\-(?:3[01]|0[1-9]|[12][0-9])T(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9])\/([0-9]{4}\-(?:1[0-2]|0[1-9])\-(?:3[01]|0[1-9]|[12][0-9])T(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9])/');

//Date Time Start with Duration
//Combined date time with a Duration
//Group 1 - Date Time e.g. 2020-01-26T17:10:00
//Non-Capture Group 0000-9999 Years
//Non-Capture Group 10-12 or 0-9 Months
//Non-Capture Group 30-31 or 0 1-9 or 1-2 0-9 Days
//T
//Non-Capture Group 2 0-3 or 0-1 0-9 Hours
//Non-Capture Group 0-5 0-9 Minutes
//Non-Capture Group 0-5 0-9 Seconds
// / (split)
//Group 2 Duration (see REGEX_DTDURATION)
define('REGEX_DTSTARTDURATION', '/^((?:[0-9]{4})\-(?:1[0-2]|0[1-9])\-(?:3[01]|0[1-9]|[12][0-9])T(?:2[0-3]|[01][0-9]):(?:[0-5][0-9]):(?:[0-5][0-9]))\/(P(?=T\d|\d)(?:\dY)?(?:1[0-2]M|[0-9]M)?(?:[1-2][0-9]D|3[0-1]D|[0-9]D)?T?(?:[1-2][0-4]H|[0-9]H)?(?:[0-5]?[0-9]M)?(?:[0-5]?[0-9]S)?)$/');

//Date Time Named - Two word Duration points that PHP can understand
//Non Capture Group yesterday|last week / month
define('REGEX_DTFIXED', '/^(today|yesterday|(?:this|previous) (?:week|month))/');


/**
 * Build Link Text
 *  Returns a HTML link containing parameters used on the page
 *
 *  @return string of parameters for a-href link
 */
function buildlink() {
  global $dt, $groupby, $searchip, $searchquery, $searchseverity;

  $link = "groupby=$groupby";
  $link .= ($dt['search'] != '') ? '&amp;datetime='.rawurlencode($dt['search']) : '';
  $link .= "&amp;severity={$searchseverity}";
  $link .= ($searchip != DEF_SYSTEM) ? "&amp;searchip={$searchip}" : '';
  $link .= ($searchquery != '') ? "&amp;searchquery={$searchquery}" : '';

  return $link;
}


/**
 * Get CIDR Range
 *   Returns Start and End IP of a given IP/CIDR range
 *
 * @param str IP/CIDR - e.g. 192.168.0.0/24
 * @return array of Start and End IP range
 */
function cidr($cidr) {
  list($ip, $mask) = explode('/', $cidr);

  $maskBinStr =str_repeat("1", $mask ) . str_repeat("0", 32-$mask );          //net mask binary string
  $inverseMaskBinStr = str_repeat("0", $mask ) . str_repeat("1",  32-$mask ); //inverse mask

  $ipLong = ip2long( $ip );
  $ipMaskLong = bindec( $maskBinStr );
  $inverseIpMaskLong = bindec( $inverseMaskBinStr );
  $netWork = $ipLong & $ipMaskLong;

  $start = long2ip($netWork);
  $end = long2ip(($netWork | $inverseIpMaskLong));

  //echo "start $start end $end";                          //Uncomment to Debug
  return array($start, $end);
}


/**
 * Sanitize user provided IP Address
 *   Checks for various combinations of allowable searches for IP address
 *   1. IP/CIDR
 *   2. IPv4 / IPv6
 *
 *  @param str searchip GET parameter
 *  @return validated IP address or DEF_SYSTEM for invalid entry
 */
function sanitize_ipaddress($value) {
  $userinput = sanitize_string($value, DEF_SYSTEM, 50);

  if (preg_match(REGEX_IPCIDR, $userinput)) {              //Check if valid IPv4/CIDR
    return $userinput;
  }

  else {
    if (filter_var($userinput, FILTER_VALIDATE_IP)) {      //Or single IPv4 / IPv6
      return $userinput;
    }
  }

  return DEF_SYSTEM;                                       //Fallback for invalid input
}


/**
 * Draw Filter Toolbar
 *  Populates filter bar with Search Boxes and Search Button
 */
function draw_filter_toolbar() {
  global $searchquery, $searchseverity, $searchip, $sort, $groupby, $dt;

  $isactive = '';

  echo '<form method="get">', PHP_EOL;
  echo '<input type="hidden" name="sort" value="', $sort, '">', PHP_EOL;
  echo '<input type="hidden" name="groupby" value="', $groupby, '">', PHP_EOL;
  echo '<input type="hidden" name="datetime" id="datetime" value="', $dt['search'], '">', PHP_EOL;
  echo '<input type="hidden" name="severity" id="severity" value="', $searchseverity, '">', PHP_EOL;

  echo '<div class="filter-toolbar queries-filter-toolbar">'.PHP_EOL;

  //Column Headers and submit button
  echo '<div><h3>Domain</h3></div>'.PHP_EOL;
  echo '<div><h3>IP</h3></div>'.PHP_EOL;
  echo '<div><h3>Time</h3></div>'.PHP_EOL;
  echo '<div><h3>Severity</h3></div>'.PHP_EOL;
  echo '<div></div>'.PHP_EOL;

  //Group 1 - Domain Search
  echo '<div><input type="text" name="searchquery" id="filtersearch" value="',
    $searchquery, '" placeholder="site.com"></div>', PHP_EOL;

  //Start Group 2 - IP
  if ($searchip == DEF_SYSTEM) {
    echo '<div><input type="text" name="searchip" id="filtersys" placeholder="192.168.0.1/24"></div>', PHP_EOL;
  }
  else {
    echo '<div><input type="text" name="searchip" id="filtersys" value="',
      $searchip, '" placeholder="192.168.0.1/24"></div>', PHP_EOL;
  }
  //End Group 2 - IP

  //Start Group 3 - Time Picker
  echo '<div id="timepicker-dropdown" tabindex="0">', PHP_EOL;
  echo '<input type="text" id="timepicker-text" value="', $dt['textbox'], '">', PHP_EOL;
  echo '<div id="timepicker-group">', PHP_EOL;            //Start timepicker-group

  echo '<article>', PHP_EOL;
  echo '<h3>Presets</h3>', PHP_EOL;
  echo '<div class="timepicker-grid timepicker-grid-half">', PHP_EOL;
  echo '<ul id="timepicker-relative-list">', PHP_EOL;     //Relative Presets
  echo '<li data-dtduration="PT15M">Last 15 Minutes</li>', PHP_EOL;
  echo '<li data-dtduration="PT30M">Last 30 Minutes</li>', PHP_EOL;
  echo '<li data-dtduration="PT1H">Last 1 Hour</li>', PHP_EOL;
  echo '<li data-dtduration="PT4H">Last 4 Hours</li>', PHP_EOL;
  echo '<li data-dtduration="PT12H">Last 12 Hours</li>', PHP_EOL;
  echo '<li data-dtduration="P1D">Last 1 Day</li>', PHP_EOL;
  echo '<li data-dtduration="P7D">Last 7 Days</li>', PHP_EOL;
  echo '<li data-dtduration="P14D">Last 14 Days</li>', PHP_EOL;
  echo '<li data-dtduration="P30D">Last 30 Days</li>', PHP_EOL;
  echo '</ul>', PHP_EOL;
  echo '<ul id="timepicker-fixed-list">', PHP_EOL;        //Fixed Presets
  echo '<li data-dtduration="today">Today</li>', PHP_EOL;
  echo '<li data-dtduration="yesterday">Yesterday</li>', PHP_EOL;
  echo '<li data-dtduration="this week">Week-To-Date</li>', PHP_EOL;
  echo '<li data-dtduration="previous week">Previous Week</li>', PHP_EOL;
  echo '<li data-dtduration="this month">Month-To-Date</li>', PHP_EOL;
  echo '<li data-dtduration="previous month">Previous Month</li>', PHP_EOL;
  echo '</ul>', PHP_EOL;
  echo '</div>', PHP_EOL;
  echo '</article>', PHP_EOL;

  echo '<article>', PHP_EOL;
  echo '<h3>Date &amp; Time Range</h3>', PHP_EOL;
  echo '<div class="timepicker-grid timepicker-grid-dtl">', PHP_EOL;
  echo '<label for="dtl-from">From:</label>', PHP_EOL;
  echo '<input type="datetime-local" id="dtl-from" value="', $dt['start'], '" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}">', PHP_EOL;
  echo '<label for="dtl-to">To:</label>', PHP_EOL;                                  //Start date-start
  echo '<input type="datetime-local" id="dtl-to" value="', $dt['end'], '" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}">', PHP_EOL;
  echo '<div><button type="button" onclick="selectTimeDate()">Set Range</button></div>', PHP_EOL;
  echo '</div>', PHP_EOL;
  echo '</article>', PHP_EOL;

  echo '</div>', PHP_EOL;                                 //End timepicker-group
  echo '</div>', PHP_EOL;                                 //End timepicker-dropdown

  //Group 4 - Severity
  echo '<div class="filter-nav-group">', PHP_EOL;

  $isactive = ($searchseverity & SEVERITY_LOW) ? ' active' : '';
  echo '<span class="filter-nav-button', $isactive, '" title="Low - Connection Allowed" onclick="toggleNavButton(this, \'', SEVERITY_LOW, '\')">',
    '<img src="./svg/filters/severity_low.svg" alt=""></span>', PHP_EOL;

  $isactive = ($searchseverity & SEVERITY_MED) ? ' active' : '';
  echo '<span class="filter-nav-button', $isactive, '" title="Medium - Connection Blocked" onclick="toggleNavButton(this, \'', SEVERITY_MED, '\')">',
    '<img src="./svg/filters/severity_med.svg" alt=""></span>', PHP_EOL;

  $isactive = ($searchseverity & SEVERITY_HIGH) ? ' active' : '';
  echo '<span class="filter-nav-button', $isactive, '" title="High - Malware or Tracker Accessed" onclick="toggleNavButton(this, \'', SEVERITY_HIGH, '\')">',
    '<img src="./svg/filters/severity_high.svg" alt=""></span>', PHP_EOL;
  echo '</div>', PHP_EOL;                                 //End Group 4 - Severity

  echo '<div><button id="submit-button" type="submit">Search</button></div>', PHP_EOL;

  echo '</div>', PHP_EOL;                                 //End Div Group
  echo '</form>', PHP_EOL;

}


/**
 * Draw Group By Buttons
 *   groupby is a form which contains hidden elements from draw_filter_toolbar
 *   Selection between Domain / Time is made using radio box, which is missing the input box
 *   Radio box labels are styled to look like pag-nav
 */
function draw_groupby() {
  global $dt, $searchquery, $searchseverity, $sort, $searchip, $groupby;

  $domainactive = '';
  $timeactive = '';

  $active0 = ($groupby == GROUPLIST[0]) ? ' checked="checked"' : '';
  $active1 = ($groupby == GROUPLIST[1]) ? ' checked="checked"' : '';

  echo '<div class="table-toolbar-options">', PHP_EOL;      //Start div for Export Button
  echo '<button type="button" class="button-grey material-icon-centre icon-export"  title="Export" ',
    'onclick="exportTable(\'', $groupby, '\')">&nbsp;</button>', PHP_EOL;
  echo '</div>', PHP_EOL;                                   //End div for Export Button

  echo '<form method="get">', PHP_EOL;
  echo '<input type="hidden" name="sort" value=', json_encode($sort), '>', PHP_EOL;
  echo '<input type="hidden" name="searchquery" value=', json_encode($searchquery), '>', PHP_EOL;
  echo '<input type="hidden" name="datetime" value="', $dt['search'], '">', PHP_EOL;
  echo '<input type="hidden" name="sys" value=', json_encode($searchip), '>', PHP_EOL;
  echo '<input type="hidden" name="severity" value=', json_encode($searchseverity), '>', PHP_EOL;
  echo '<div id="groupby-container">', PHP_EOL;
  echo '<input type="radio" id="gbtab1" name="groupby" value=', json_encode(GROUPLIST[0]), ' onchange="submit()"', $active0, '><label for="gbtab1">Domain</label>', PHP_EOL;
  echo '<input type="radio" id="gbtab2" name="groupby" value=', json_encode(GROUPLIST[1]), ' onchange="submit()"', $active1, '><label for="gbtab2">Time</label>', PHP_EOL;
  echo '</div></form>';
                                    //End Table Toolbar Export
}


/**
 * Convert Relative Preset from DT Duration to Human readable form
 *  Converts ISO 8601 Abbrevated Duration from Relative Presets to a Human readable form
 *  e.g. P1M2DT15H22M8S = 1 Month 2 Days 15 Hours 22 Minutes 8 Seconds
 *
 *  @param string ISO 8601 duration from user input
 *  @return Human readable string of the Relative Preset
 */
function human_dtduration_text($duration) {
  $str = '';
  $i = 0;
  $dtnames = array('', 'Year', 'Month', 'Day', 'Hour', 'Minute', 'Second');

  //Use REGEX_DTDURATION to get grouped matches
  preg_match(REGEX_DTDURATION, $duration, $matches);

  for ($i = 1; $i < 7; $i++) {                           //Loop through array of matches
    //If match is set, get pluralised value from corresponding dtnames array
    if (isset($matches[$i])) {
      if ($matches[$i] != '') {
        $str .= pluralise((int)substr($matches[$i], 0, -1), $dtnames[$i]);
        $str .= ' ';
      }
    }
  }
  return $str;
}


/**
 * Populate DT (DateTime) Array
 *   Populates Human Readable and SQL search items in DT Array,
 *   based on input from $dt['search']
 */
function populate_datetime() {
  global $dt;

  $fixedstart = '';
  $fixedend = '';

  $matches = array();
  $SQLFORMAT = 'Y-m-d H:i:s';

  //Duration from ISO 8601
  if (preg_match(REGEX_DTDURATION, $dt['search'])) {
    $startdate = new DateTime('now');
    $enddate = new DateTime('now');
    $startdate->sub(new DateInterval($dt['search']));

    $dt['sql'] = "log_time > '".$startdate->format($SQLFORMAT)."'";
    $dt['textbox'] = 'Last '.human_dtduration_text($dt['search']);
  }

  //Start Duration
  //(2020-01-26T15:00:00)/(PT1H)
  //Group 1 Date Time
  //Group 2 Range to add
  elseif (preg_match(REGEX_DTSTARTDURATION, $dt['search'], $matches)) {
    $startdate = new DateTime($matches[1]);
    $enddate = new DateTime($matches[1]);
    $enddate->add(new DateInterval($matches[2]));

    $dt['sql'] = "log_time > '".$startdate->format($SQLFORMAT)."' AND log_time < '".$enddate->format($SQLFORMAT)."'";

    //Searching for one day, so drop the time
    if ($matches[2] == 'P1D') {
      $dt['textbox'] = $startdate->format('d M').' for '.human_dtduration_text($matches[2]);
    }
    else {
      $dt['textbox'] = $startdate->format('d M H:i').' for '.human_dtduration_text($matches[2]);
    }
  }

  elseif (preg_match(REGEX_DTRANGE, $dt['search'], $matches)) {
    $startdate = new DateTime($matches[1]);
    $enddate = new DateTime($matches[2]);

    //Check Unix timestamp to see if startdate > enddate
    if ($startdate->getTimestamp() > $enddate->getTimestamp()) {
      unset($startdate);                                  //Unset and swap values
      unset($enddate);
      $startdate = new DateTime($matches[2]);
      $enddate = new DateTime($matches[1]);
    }

    $dt['sql'] = "log_time > '".$startdate->format($SQLFORMAT)."' AND log_time < '".$enddate->format($SQLFORMAT)."'";

    $dt['textbox'] = $startdate->format('d M H:i').' to '.$enddate->format('d M H:i');
  }

  elseif (preg_match(REGEX_DTFIXED, $dt['search'])) {
    switch($dt['search']) {
      case 'today':
        $fixedstart = 'today midnight';
        $fixedend = 'now';
        $dt['textbox'] = 'Today';
        break;
      case 'previous week':
        $fixedstart = 'last week midnight';
        $fixedend = 'this week midnight';
        $dt['textbox'] = 'Previous Week';
        break;
      case 'this week':
        $fixedstart = 'this week midnight';
        $fixedend = 'now';
        $dt['textbox'] = 'Week-To-Date';
        break;
      case 'previous month':
        $fixedstart = 'last month midnight';
        $fixedend = 'this month midnight';
        $dt['textbox'] = 'Previous Month';
        break;
      case 'this month':
        $fixedstart = 'this month midnight';
        $fixedend = 'now';
        $dt['textbox'] = 'Month-To-Date';
        break;
      default:                                             //Default to yesterday
        $fixedstart = 'yesterday midnight';
        $fixedend = 'today midnight';
        $dt['textbox'] = 'Yesterday';
        break;
    }

    $startdate = new DateTime($fixedstart);
    $enddate = new DateTime($fixedend);
    $dt['sql'] = "log_time > '".$startdate->format($SQLFORMAT)."' AND log_time < '".$enddate->format($SQLFORMAT)."'";
  }

  $dt['start'] = $startdate->format('Y-m-d\TH:i');
  $dt['end'] = $enddate->format('Y-m-d\TH:i');
}


/**
 * Generate SQL for a DNS Query search
 *  @param string Users search string from searchquery
 *  @return SQL query string for DNS Request
 */
function query_dns($dns) {
  $query = '';

  if ($dns == '') {
    return '';
  }
  //Contains - *search*
  elseif (preg_match('/^\*([\w\.\-])+\*$/', $dns, $matches)) {
    $query = " AND dns_request LIKE '%{$matches[1]}%' ";
  }
  //Ends With - *search
  elseif (preg_match('/^\*([\w\.\-]+)$/', $dns, $matches)) {
    $query = " AND dns_request LIKE '%{$matches[1]}' ";
  }
  //Begins With - search*
  elseif (preg_match('/^([\w\.\-]+)\*$/', $dns, $matches)) {
    $query = " AND dns_request LIKE '{$matches[1]}%' ";
  }
  //Contains - search
  else {
    $query = " AND dns_request LIKE '%{$dns}%' ";
  }

  return $query;
}


/**
 * Generate SQL search for single IP or IP range
 *  @param string Users search string from searchip
 *  @return SQL query string for IP
 */
function query_ip($ipsearch) {
  $query = '';
  $ipstart = 0;
  $ipend = 0;

  if ($ipsearch == DEF_SYSTEM) {
    return '';
  }

  if (preg_match(REGEX_IPCIDR, $ipsearch) > 0) {
    list($ipstart, $ipend) = cidr($ipsearch);
    $query = " AND INET_ATON(sys) BETWEEN INET_ATON('$ipstart') AND INET_ATON('$ipend') ";
  }
  else {
    $query = " AND sys = '$ipsearch' ";
  }
  return $query;
}



/**
 * Generate SQL Query for Severity based on bitwise value for the severity buttons pressed
 *  @param int user input for severity
 *  @returns SQL query string for severity
 */
function query_severity($severity) {
  $query = '';

  switch($severity) {
    case SEVERITY_LOW:
      $query = " AND severity = '1' ";                    //Allowed, Cached, Local
      break;
    case SEVERITY_MED:
      $query = " AND severity = '2' ";                    //Blocked
      break;
    case SEVERITY_HIGH:
      $query = " AND severity = '3' ";                    //Malware, Tracker
      break;
    case SEVERITY_LOW + SEVERITY_MED:
      $query = " AND severity IN ('1','2') ";             //Allowed, Blocked
      break;
    case SEVERITY_LOW + SEVERITY_HIGH:
      $query = " AND severity IN ('1','3') ";             //Allowed, Malware, Tracker
      break;
    case SEVERITY_MED + SEVERITY_HIGH:
      $query = " AND severity IN ('2','3') ";             //Blocked, Malware, Tracker
      break;
    //default doesn't require a search
  }

  return $query;
}


/**
 * Setup Group By Query Name Table
 *  Carry out SQL Search for Group By Name
 *  Initialise Group By Name Table
 *  Export required constants to JavaScript
 */
function setup_name_table() {
  global $config, $db, $dt, $searchip, $searchquery, $searchseverity, $sort;
  $query = '';

  $query = "SELECT ROW_NUMBER() OVER (ORDER BY count $sort) AS row_num, dns_request, severity, bl_source, COUNT(dns_request) AS count FROM dnslog "
    . "WHERE {$dt['sql']}" . query_dns($searchquery)
    . query_ip($searchip) . query_severity($searchseverity)
    . " GROUP BY dns_request ORDER BY count {$sort}";

  $result = $db->query($query);

  if (! $result) {
    echo '<h4><img src="./svg/emoji_sad.svg" alt=":-(">Error running query</h4>', PHP_EOL;
    echo $db->error, '</div></main>', PHP_EOL;
    die();
  }

  echo '<div class="table-toolbar">', PHP_EOL;            //Start table-toolbar
  echo '<div class="pag-nav" id="pagination"></div>', PHP_EOL;
  draw_groupby();
  echo '</div>'.PHP_EOL;                                  //End table-toolbar

  if ($result->num_rows == 0) {                           //Leave if nothing found
    $result->free();
    echo '<h4><img src="./svg/emoji_sad.svg" alt=":-(">No results found</h4>', PHP_EOL;
    return;
  }

  echo '<table id="query-group-table">'.PHP_EOL;          //Begin Query Table
  echo '<tr><th>&nbsp;</th><th>#</th><th>Domain</th><th>Requests',
    '<a class="primarydark" href="?', buildlink(), '&amp;sort=DESC">&#x25BE;</a>',
    '<a class="primarydark" href="?', buildlink(), '&amp;sort=ASC">&#x25B4;</a></th>',
    '<th>&nbsp;</th></tr>'.PHP_EOL;
  echo '</table>', PHP_EOL;                               //End Query Table

  echo '<script>', PHP_EOL;                               //Begin JavaScript Exports
  echo 'const BLOCKLISTS = ', json_encode($config->blocklists), ';', PHP_EOL;
  echo 'const DATA = ', json_encode($result->fetch_all()), ';', PHP_EOL;
  echo 'const SEARCH_ENGINE = "', $config->search_engine, '";', PHP_EOL;
  echo 'const SEARCH_URL = "', $config->search_url, '";', PHP_EOL;

  //Setup Investigate / Whois for popupmenu
  if ($config->whois_api == '') {
    echo 'const INVESTIGATE = "', $config->whois_provider, '";', PHP_EOL;
    echo 'const INVESTIGATE_URL = "', $config->whois_url, '";', PHP_EOL;
  }
  else {
    echo 'const INVESTIGATE = "Investigate";', PHP_EOL;
    echo 'const INVESTIGATE_URL = "./investigate.php?subdomain=";', PHP_EOL;
  }

  echo 'populateNameTable(1);', PHP_EOL;
  echo '</script>', PHP_EOL;

  $result->free();
}


/**
 * Setup Group By Query Time Table
 *  Carry out SQL Search for Group By Time
 *  Initialise Group By Name Table
 *  Export required constants to JavaScript
 */
function setup_time_table() {
  global $config, $db, $dt, $searchip, $searchquery, $searchseverity, $sort;
  $query = '';

  $query = "SELECT * FROM dnslog WHERE {$dt['sql']}" . query_dns($searchquery) .
    query_ip($searchip) . query_severity($searchseverity) .
    " ORDER BY UNIX_TIMESTAMP(log_time) {$sort}";

  $result = $db->query($query);

  if (! $result) {
    echo '<h4><img src="./svg/emoji_sad.svg" alt=":-(">Error running query</h4>', PHP_EOL;
    echo $db->error, '</div></main>', PHP_EOL;
    die();
  }

  echo '<div class="table-toolbar">', PHP_EOL;            //Start table-toolbar
  echo '<div class="pag-nav" id="pagination"></div>', PHP_EOL;
  draw_groupby();
  echo '</div>'.PHP_EOL;                                  //End table-toolbar

  if ($result->num_rows == 0) {                           //Leave if nothing found
    $result->free();
    echo '<h4><img src="./svg/emoji_sad.svg" alt=":-(">No results found</h4>', PHP_EOL;
    return;
  }

  echo '<table id="query-time-table">', PHP_EOL;          //Begin Time Table
  echo '<tr><th>&nbsp;</th><th>Time',
    '<a class="primarydark" href="?', buildlink(), '&amp;sort=DESC">&#x25BE;</a>',
    '<a class="primarydark" href="?', buildlink(), '&amp;sort=ASC">&#x25B4;</a>',
    '</th><th>System</th><th>Domain</th><th>&nbsp;</th></tr>'.PHP_EOL;
  echo '</table>', PHP_EOL;                               //End Time Table

  echo '<script>', PHP_EOL;                               //Begin JavaScript Exports
  echo 'const BLOCKLISTS = ', json_encode($config->blocklists), ';', PHP_EOL;
  echo 'const DATA = ', json_encode($result->fetch_all()), ';', PHP_EOL;
  echo 'const SEARCH_ENGINE = "', $config->search_engine, '";', PHP_EOL;
  echo 'const SEARCH_URL = "', $config->search_url, '";', PHP_EOL;

  //Setup Investigate / Whois for popupmenu
  if ($config->whois_api == '') {
    echo 'const INVESTIGATE = "', $config->whois_provider, '";', PHP_EOL;
    echo 'const INVESTIGATE_URL = "', $config->whois_url, '";', PHP_EOL;
  }
  else {
    echo 'const INVESTIGATE = "Investigate";', PHP_EOL;
    echo 'const INVESTIGATE_URL = "./investigate.php?subdomain=";', PHP_EOL;
  }

  echo 'populateTimeTable(1);', PHP_EOL;
  echo '</script>', PHP_EOL;

  $result->free();
}


/********************************************************************
 * Main
 */
$db = new mysqli(SERVERNAME, USERNAME, PASSWORD, DBNAME);

$searchseverity = $_GET['severity'] ?? 0;
$searchseverity = filter_integer($searchseverity, 0, 7, 0);

if (isset($_GET['sort'])) {
  if ($_GET['sort'] == 'ASC') $sort = 'ASC';
}

//searchip uses custom function to verify valid search
if (isset($_GET['searchip'])) {
  $searchip = sanitize_ipaddress($_GET['searchip']);
}

$groupby = $_GET['groupby'] ?? GROUPLIST[0];
$groupby = sanitize_string($groupby, GROUPLIST[0], 14);
if (! in_array($groupby, GROUPLIST)) {
  $groupby = GROUPLIST[0];
}

$searchquery = $_GET['searchquery'] ?? '';
$searchquery = sanitize_string($searchquery, '', 255);

//Validate datetime against one of the multiple regex options
if (isset($_GET['datetime'])) {
  if ((preg_match(REGEX_DTDURATION, $_GET['datetime'])) ||
      (preg_match(REGEX_DTRANGE, $_GET['datetime'])) ||
      (preg_match(REGEX_DTSTARTDURATION, $_GET['datetime'])) ||
      (preg_match(REGEX_DTFIXED, $_GET['datetime']))) {
    $dt['search'] = $_GET['datetime'];
  }
}

populate_datetime();

echo '<section>', PHP_EOL;
draw_filter_toolbar();                                     //Draw filter-toolbar

if ($groupby == GROUPLIST[0]) {
  setup_name_table();
}
elseif ($groupby == GROUPLIST[1]) {
  setup_time_table();
}

echo '</section>', PHP_EOL;

$db->close();
?>
<div id="copymsg">clipboard</div>
</main>
<div id="report-dialog">
<h2>Report Domain</h2>
<form action="https://quidsup.net/notrack/report.php" method="post" target="_blank">
<input type="hidden" name="site" id="reportInput" value="none">
<fieldset>
<legend id="reportTitle">domain</legend>
<input type="text" name="comment" placeholder="Optional comment">
</fieldset>
<fieldset>
<button type="submit">Confirm</button>
<button type="button" class="button-grey" onclick="hideDialogs()">Cancel</button>
</fieldset>
</form>
</div>

<div id="queries-dialog">
<h2 id="blocktitle">Block Domain</h2>
<form action="./config/customblocklist.php" method="POST" target="_blank">
<input type="hidden" name="action" id="blockAction" value="block">
<input type="hidden" name="comment" value="">
<fieldset id="blockitem1"></fieldset>
<fieldset id="blockitem2"></fieldset>
</form>
<fieldset>
<button type="button" class="button-grey" onclick="hideDialogs()">Cancel</button>
</fieldset>
</div>
<div id="fade" onclick="hideDialogs()"></div>

<script>
function timePresetOnClick() {
  if (! this.dataset.hasOwnProperty('dtduration')) {
    console.error('Missing data-dtduration');
    return;
  }
  setTimePreset(this.dataset.dtduration, this.innerText);
}


function timePresetOnKeypress(e) {
  if (e.keyCode == 13) {
    e.preventDefault();
    if (! this.dataset.hasOwnProperty('dtduration')) {
      console.error('Missing data-dtduration');
      return;
    }
    setTimePreset(this.dataset.dtduration, this.innerText);
  }
}

function setupTimePicker() {
  const tpFixedItems = document.getElementById('timepicker-fixed-list').children;
  const tpRelativeItems = document.getElementById('timepicker-relative-list').children;

  for (let i = 0; i < tpFixedItems.length; i++) {
    tpFixedItems[i].addEventListener('click', timePresetOnClick);
    tpFixedItems[i].addEventListener('keypress', timePresetOnKeypress);
    tpFixedItems[i].tabIndex = 0;
  }

  for (let i = 0; i < tpRelativeItems.length; i++) {
    tpRelativeItems[i].addEventListener('click', timePresetOnClick);
    tpRelativeItems[i].addEventListener('keypress', timePresetOnKeypress);
    tpRelativeItems[i].tabIndex = 0;
  }
}
setupTimePicker();
</script>
</body>
</html>
