<?php
/**
 * 1. Draw initial page with PHP. Most of the functionality onwards is done clientside with javascript.
 * 2. Fill livetable with blank rows and cells up to MAX_LINES.
 * 3. Run a timer.
 * 4. Send POST request to API asking for contents of DNS_LOG file.
 * 5. Parse DNS Log file (in a similar method to logparser.py) into requestBuffer map indexed by serial number from the log file.
 * 6. Flush requestBuffer removing incomplete entries and displayed items after 12 mins.
 * 7. Move limited number of items from requestBuffer to readyBuffer based on size of requestBuffer.
 * 8. Show contents of readyBuffer in table livetable.
 */

require('./include/global-vars.php');
require('./include/global-functions.php');
require('./include/config.php');
require('./include/menu.php');

ensure_active_session();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link href="./css/master.css" rel="stylesheet" type="text/css">
  <link href="./css/icons.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="./favicon.png">
  <script src="./include/menu.js"></script>
  <script src="./include/queries.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=0.8">
  <title>NoTrack - Live</title>
</head>

<body>
<?php
draw_page_header('Live');
draw_page_nav();
?>
<main>
<section>
<div class="filter-toolbar live-filter-toolbar">
<div><h3>Domain</h3></div>
<div><h3>System IP</h3></div>
<div><h3>Severity</h3></div>
<div></div>

<!--Group 1 - Domain Search-->
<div><input type="text" id="filterdomain" value="" placeholder="site.com"></div>

<!--Group 2 - System IP Search-->
<div><input type="text" id="filtersys" value="" placeholder="192.168.0.1"></div>

<!--Group 3 - Severity-->
<div class="filter-nav-group">
<span class="filter-nav-button" title="Low - Connection Allowed" onclick="toggleSeverity(this, '<?php echo SEVERITY_LOW?>')"><img src="./svg/filters/severity_low.svg" alt=""></span>
<span class="filter-nav-button" title="Medium - Connection Blocked" onclick="toggleSeverity(this, '<?php echo SEVERITY_MED?>')"><img src="./svg/filters/severity_med.svg" alt=""></span>
<span class="filter-nav-button" title="High - Malware or Tracker Accessed" onclick="toggleSeverity(this, '<?php echo SEVERITY_HIGH?>')"><img src="./svg/filters/severity_high.svg" alt=""></span>
</div>

<!--Group 4-->
<div class="filter-nav-group">
<div id="livepause" class="filter-nav-button active icon-pause material-icon-centre" title="Pause" onclick="pauseQueue()"></div>
<div class="filter-nav-button icon-refresh material-icon-centre" title="Refresh" onclick="clearQueue()"></div>
</div>
</div>

<table id="livetable">
</table>
</section>
<footer id="copymsg">clipboard</footer>
<div id="report-dialog">
<h2>Report Domain</h2>
<form action="https://quidsup.net/notrack/report.php" method="post" target="_blank">
<input type="hidden" name="site" id="reportInput" value="none">
<fieldset>
<legend id="reportTitle">domain</legend>
<input type="text" name="comment" placeholder="Optional comment">
</fieldset>
<fieldset>
<button type="submit">Confirm</button>
<button type="button" class="button-grey" onclick="hideDialogs()">Cancel</button>
</fieldset>
</form>
</div>

<div id="queries-dialog">
<h2 id="blocktitle">Block Domain</h2>
<form action="./config/customblocklist.php" method="POST" target="_blank">
<input type="hidden" name="action" id="blockAction" value="block">
<input type="hidden" name="comment" value="">
<fieldset id="blockitem1"></fieldset>
<fieldset id="blockitem2"></fieldset>
</form>
<fieldset>
<button type="button" class="button-grey" onclick="hideDialogs()">Cancel</button>
</fieldset>
</div>
<div id="fade" onclick="hideDialogs()"></div>
</main>

<script>
<?php
echo 'const SEARCH_ENGINE = ', json_encode($config->search_engine), ';', PHP_EOL;
echo 'const SEARCH_URL = "', $config->search_url, '";', PHP_EOL;

if ($config->whois_api == '') {                            //Setup Investigate / Whois for popupmenu
  echo 'const INVESTIGATE = ', json_encode($config->whois_provider), ';', PHP_EOL;
  echo 'const INVESTIGATE_URL = "', $config->whois_url, '";', PHP_EOL;
}
else {
  echo 'const INVESTIGATE = "Investigate";', PHP_EOL;
  echo 'const INVESTIGATE_URL = "./investigate.php?subdomain=";', PHP_EOL;
}
?>
const MAX_LINES = 27;
const SEVERITY_HIGH = <?php echo SEVERITY_HIGH?>;
const SEVERITY_MED = <?php echo SEVERITY_MED?>;
const SEVERITY_LOW = <?php echo SEVERITY_LOW?>;
const BLOCKLISTS = {"invalid":[true,"invalid","Blocked Domain"]}; //Fake blocklists as there is no search against real block list
const dnsmasqLine = /^(?<log_month>\w{3})  ?(?<log_day>\d{1,2}) (?<log_time>\d{2}:\d{2}:\d{2}) dnsmasq\[\d{1,7}\]: (?<serial>\d+) (?<sys>[\d\.:]+)\/\d+ (?<action>query|reply|config|cached|\/etc\/localhosts\.list)(?:\[A{1,4}\])? (?![\d\.:])(?<domain>[\w\.\-]{2,254}) (?:is|to|from) (?<res>[\w\.\-:<>]+)$/;
var regexp = /^(?<log_date>\w{3}  ?\d{1,2} \d{2}:\d{2}:\d{2}) dnsmasq\[\d{1,7}\]: (?<serial>\d+) (?<sys>[\d\.:]+)\/\d+ (?<action>query|reply|config|cached|\/etc\/localhosts\.list)(?:\[A{1,4}\])? (?<domain>[\w\.\-]{2,254}) (?:is|to|from) (?<res>[\w\.:<>]*)$/

var paused = false;
var throttleApiRequest = 0;                 //Throttle read requests for DNS_LOG
var readyBuffer = new Array();              //Requests displayed on livetable
var requestBuffer = new Map();              //Requests read from DNS_LOG waiting to be moved to readyBuffer
var searchSeverity = 0;

drawTable();
loadApi();
moveBuffer();
displayRequests();


/**
 * Basic Search
 *  Carry out a basic search to see if users supplied input exists in the source string.
 *  @note values are converted to lowercase as includes is case sensitive.
 *
 *  @param str source value
 *  @param str search string
 *  @return bool has result been found
 *
 */
function basicSearch(src, search) {
  if (search == '') {                                     //No search to carry out
    return true;
  }

  return src.toLowerCase().includes(search);
}

/**
 * Beautify Domain
 *  Drop www. from beginning of DNS Request
 *
 *  @param str domain to beautify
 *  @return beautified domain string
 *
 */
function beautifyDomain(domain) {
  if (domain.startsWith('www.')) {
    return domain.substr(4);
  }
  return domain;
}


/**
 * Check Severity
 *   Called from moveBuffer to determine if an item should be added if a severity
 *    has been selected
 *
 *   @param int Severity value from requestBuffer
 *   @return bool confirming if entry should be added to requestBuffer
 *
 */
function checkSeverity(severity) {
  //No search active
  if (searchSeverity == 0) {
    return true;
  }

  //Severity search is active, check whether the item should be added
  if ((severity == 1) && (searchSeverity & SEVERITY_LOW)) {
    return true;
  }
  if ((severity == 2) && (searchSeverity & SEVERITY_MED)) {
    return true;
  }
  if ((severity == 3) && (searchSeverity & SEVERITY_HIGH)) {
    return true;
  }

  return false;
}

/**
 * Draw Table
 *
 */
function drawTable() {
  const liveTable = document.getElementById('livetable');
  for (let i = 0; i < MAX_LINES - 1; i++) {
    let row = liveTable.insertRow(i);
    row.insertCell(0);
    row.insertCell(1);
    row.insertCell(2);
    row.insertCell(3);
    row.insertCell(4);
    liveTable.rows[i].cells[0].innerHTML = '&nbsp;';
  }
}


/**
 * Count Buffer Size
 *  Count the number of undisplayed items in requestBuffer
 *  @return Number of items undisplayed
 *
 */
function countBufferSize() {
  let bufferSize = 0;

  for (let values of requestBuffer.values()) {
    if (values.displayed == false) {
      bufferSize++;
    }
  }

  //console.log(`Display ${bufferSize} total ${requestBuffer.size}`);
  return bufferSize;
}


/**
 * Clear Queue
 *  Action is carried out when user clicks Clear button
 *
 */
function clearQueue() {
  const liveTable = document.getElementById('livetable');

  //Clear all values from readyBuffer and requestBuffer
  readyBuffer = [];
  requestBuffer.clear();

  //Remove all values from the table
  for (let i = 0; i < MAX_LINES - 1; i++) {
    //liveTable.rows[i].className = '';
    liveTable.rows[i].cells[0].innerHTML = '&nbsp;';
    liveTable.rows[i].cells[1].innerHTML = '&nbsp;';
    liveTable.rows[i].cells[2].innerHTML = '&nbsp;';
    liveTable.rows[i].cells[3].innerHTML = '&nbsp;';
    liveTable.rows[i].cells[4].innerHTML = '&nbsp;';
  }
}


/**
 * Display Queue
 *  Show the results from readyBuffer array in livetable
 *  Array is displayed from end-to-start (latest-to-earliest)
 *
 */
function displayRequests() {
  const liveTable = document.getElementById('livetable');
  const queuesize = readyBuffer.length;
  let blSource = '';
  let clipboard = '';
  let currentRow = 0;
  let domain = '';
  let imgSrc = '';
  let imgTitle = '';
  let severity = 0;

  //Dont update the table when paused
  if (paused) {
    return;
  }

  //Start with latest entry first
  for (let i = queuesize - 1; i > 0; i--) {
    blSource = readyBuffer[i].result;
    domain = readyBuffer[i].dns_request;
    severity = readyBuffer[i].severity;

    clipboard = `<div class="icon-clipboard" onclick="setClipboard('${domain}')" title="Copy domain">&nbsp;</div>`;

    liveTable.rows[currentRow].cells[0].innerHTML = createImageCell(severity, blSource);
    liveTable.rows[currentRow].cells[1].innerHTML = readyBuffer[i].log_time;
    liveTable.rows[currentRow].cells[2].innerHTML = `${domain}${clipboard}`;
    liveTable.rows[currentRow].cells[3].innerHTML = readyBuffer[i].sys
    liveTable.rows[currentRow].cells[4].innerHTML = createPopupMenu(domain, severity, blSource, true);
    currentRow++;
  }
}


/**
 * Flush Request Buffer
 *  Limit memory utilisation of requestBuffer by removing items which have been displayed.
 *
 *  Remove incomplete / errored entries.
 *  Keep displayed items in for 12 Mins to allow for logparser.py to flush the log file.
 *  Keep non-displayed items in for 60 Mins to limit client memory utilisation.
 */
function flushBuffer() {
  const clearTime = Date.now() - 720000;                  //12 Mins
  const purgeTime = Date.now() - 3600000;                 //60 Mins

  for (let [key, values] of requestBuffer.entries()) {
    //Remove incomplete / errored entries
    if (values.result == 'unknown') {
      requestBuffer.delete(key);
    }

    //Remove displayed items older than 12 Mins
    else if ((values.displayed) && (values.epoch < clearTime)) {
      requestBuffer.delete(key);
    }

    //Remove non-displayed items older than 60 Mins
    else if ((! values.displayed) && (values.epoch < purgeTime)) {
      requestBuffer.delete(key);
    }
  }
}


/**
 * Move requestBuffer to readyBuffer
 *  Moves a certain number of items from requestBuffer map into readyBuffer array
 *
 */
function moveBuffer() {
  let i = 0;
  let bufferSize = 0;
  let target = 0.0;                    //Number of items to add into readyBuffer
  let searchDomain = '';
  let searchIP = '';

  //Don't move the queue if user has paused
  if (paused) {
    return;
  }

  bufferSize = countBufferSize();                //Number of non-displayed items
  if (bufferSize == 0) {
    //Prevent div by zero
    return;
  }

  //Setup search strings
  searchDomain = document.getElementById('filterdomain').value.trim().toLowerCase();
  searchIP = document.getElementById('filtersys').value.trim().toLowerCase();

  //Target is based on a percentage of the bufferSize with no more than 60% of the buffer to be moved
  target = Math.ceil((bufferSize / MAX_LINES) * 3.5);
  if (target > MAX_LINES * 0.6) target = Math.ceil(MAX_LINES * 0.6);
  //If nothing much in readyBuffer, add 9 requests
  if (readyBuffer.size < 9) target = 9;

  for (let [key, values] of requestBuffer.entries()) {
    if (values.displayed) continue;

    //Searching for a Domain or IP?
    if ((! basicSearch(values.dns_request, searchDomain)) || (! basicSearch(values.sys, searchIP))) {
      //Search successful and result not found - mark as displayed then skip
      requestBuffer.get(key).displayed = true;
      continue;
    }

    //Searching for a severity?
    if (! checkSeverity(values.severity)) {
      requestBuffer.get(key).displayed = true;
      continue;
    }

    //Add object to readyBuffer and set value to displayed
    readyBuffer.push(values);
    requestBuffer.get(key).displayed = true;
    i++;

    if (readyBuffer.length > MAX_LINES) {
      //Delete trailing lines
      readyBuffer.shift();
    }
    if (i >= target) {
      break;
    }
  }
}


/**
 * Pause Queue
 *  Activated when user clicks Pause / Play button
 *
 */
function pauseQueue() {
  const pauseButton = document.getElementById('livepause');
  paused = !paused;

  if (paused) {
    pauseButton.classList.remove('icon-pause');
    pauseButton.classList.add('icon-play');
  }
  else {
    pauseButton.classList.remove('icon-play');
    pauseButton.classList.add('icon-pause');
  }
}


/**
 * Process DNS log lines from the API
 *  Parse JSON output of DNS log file into requestBuffer
 *
 *  @param array JSON Data
 */
function processLog(apiResponse) {
  const currentYear = new Date().getFullYear();           //Dnsmasq log doesn't store year
  const currentMonth = new Date().getMonth() + 1;         //Replace short month with month number
  let action = '';
  let line = '';
  let domain = '';
  let result = '';
  let serial = '';
  let matches = new Object();

  for (key in apiResponse) {
    line = apiResponse[key];                              //Get log line
    matches = dnsmasqLine.exec(line);

    //Ignore forward entries and any other system info from dnsmasq
    if (matches === null) {
      continue;
    }

    action = matches.groups.action
    domain = matches.groups.domain;
    result = matches.groups.res;
    serial = matches.groups.serial;

    //Ignore reply query is duplicate log line
    if (result == 'duplicate') {
      continue;
    }

    if (! requestBuffer.has(serial)) {
      //Create new request record to complete in later log entries
      requestBuffer.set(serial, {
        displayed: false,
        dns_request: beautifyDomain(domain),
        epoch: Date.parse(`${currentYear}-${currentMonth}-${matches.groups.log_day} ${matches.groups.log_time}Z`),
        log_time: formatTime(`${currentYear}-${currentMonth}-${matches.groups.log_day} ${matches.groups.log_time}`),
        result: 'unknown',
        severity: 0,
        sys: matches.groups.sys,
      });
    }

    //Allowed Request - cached or reply
    if ((action == 'cached') || (action == 'reply')) {
      requestBuffer.get(serial).severity = 1

      //CNAME results in another query against the serial number
      if (result == '<CNAME>') {
        requestBuffer.get(serial).result = 'cname';
        //Rename record with domain appended to serial number
        requestBuffer.set(`${serial}${domain}`, requestBuffer.get(serial));
        requestBuffer.delete(serial);
      }

      //Non-Existant Domain
      else if (result == 'NXDOMAIN') {
        requestBuffer.get(serial).result = 'nxdomain';
      }

      //Issue reaching remote DNS server for this domain
      else if (result == 'SERVFAIL') {
        requestBuffer.get(serial).result = 'servfail';
      }

      //Disregard NODATA-IPv4 and NODATA-IPv6 as there was likely another query
      else if (result.startsWith('NODATA')) {
        continue;
      }

      else {
        if (action == 'cached') {
          requestBuffer.get(serial).result = 'cached';
        }
        else {
          requestBuffer.get(serial).result = 'allowed';
        }
      }
    }

    //Blocked Request
    else if (action == 'config') {
      //TODO Find out which blocklist prevented the DNS lookup
      requestBuffer.get(serial).severity = 2;
      requestBuffer.get(serial).result = 'invalid';
    }

    //LAN Query
    else if (action == '/etc/localhosts.list') {
      requestBuffer.get(serial).severity = 1;
      requestBuffer.get(serial).result = 'local';
    }
  }
}


/**
 * Toggle Nav Button
 *  Toggle active state of severity button and adjust severity search
 *
 *  @param element button to toggle
 *  @param int severity value assigned to button
 *
 */
function toggleSeverity(item, sev) {
  if (item.classList.contains('active')) {
    searchSeverity -= Number(sev);
    item.classList.remove('active');
  }
  else {
    searchSeverity += Number(sev);
    item.classList.add('active');
  }
}


/**
 * Load API
 *  Send POST requst to NoTrack API asking for the contents of DNS log file
 *
 */
function loadApi() {
  let xmlhttp = new XMLHttpRequest();
  let url = './include/api.php';
  let params = 'livedns=1';

  xmlhttp.open('POST', url, true);
  xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      let apiResponse = JSON.parse(this.responseText);
      processLog(apiResponse);
      flushBuffer();
    }
  }
  xmlhttp.send(params);
}


/**
 * Timer
 *
 */
setInterval(function() {
  if (throttleApiRequest >= 5) {                           //Throttle loading of DNS_LOG
    loadApi();
    throttleApiRequest = 0;
  }

  if (document.visibilityState == 'visible') {             //Move queue if window is visible
    moveBuffer();
    displayRequests();
  }
  throttleApiRequest++;

}, 2000);


</script>
</body>
</html>
