const ROWSPERPAGE = 100;
const TOPDOMAIN_REGEX = /[\w\-]{1,63}\.(?:(?:net?|org|com?|ac|edu?|go[bv]?|ltd|mil)\.)?[\w\-]{2,63}$/;
const EVENT_NAMES = {
  'advert': 'Allowed Advert',
  'allowed': 'Allowed',
  'cached': 'Cached',
  'cname': 'CNAME',
  'invalid': 'Blocked',
  'local': 'Local',
  'nxdomain': 'Non-Existent Domain',
  'servfail': 'Server Fail',
  'tracker': 'Allowed Tracker',
}

/**
 * Multidimensional Array Exact String Search
 *  Used to search an array for a set value from a textbox selector
 *  @param array Array to search
 *  @param str String to find an exact match against
 *  @param int Column to search in
 */
function arrayStringSearch(searchArray, searchValue, searchColumn) {
  return searchArray.filter(item => {
    return item[searchColumn] === searchValue;
  });
}

/**
 * Multidimensional Array Flexible String Search
 *  Offers mutliple different methods of utilising a text string search:
 *  1. Substring match
 *  2. Starts with*
 *  3. *Ends with
 *  4. Regular Expression
 *  @param array Array to search
 *  @param str string formatted any of the above four ways
 *  @param int Column to search in
 */
function arrayFlexSearch(searchArray, searchValue, searchColumn) {
  let searchStr = '';

  searchStr = searchValue.trim().toLowerCase();

  //Substring search with no special characters
  if (/^[\w\-\.]{1,253}$/.test(searchStr)) {
    return searchArray.filter(item => {
      return item[searchColumn].includes(searchStr);
    });
  }
  //String Starts with
  else if (/^[\w\-\.]{1,253}\*$/.test(searchStr)) {
    searchStr = searchStr.substring(0, searchStr.length - 1);
    return searchArray.filter(item => {
      return item[searchColumn].startsWith(searchStr);
    });
  }
  //String Ends with
  else if (/^\*[\w\-\.]{1,253}$/.test(searchStr)) {
    searchStr = searchStr.substring(1);                   //Drop asterisk from search
    return searchArray.filter(item => {
      return item[searchColumn].endsWith(searchStr);
    });
  }

  //Attempt to create a regular expression with the users input
  try {
    re = new RegExp(searchStr, 'i');
    return searchArray.filter(item => {
      return re.test(item[searchColumn]);
    });
  }
  catch(e) {
    //No need to warn, it could be a partially constructed statement
    return searchArray;
  }

  return searchArray;
}

/**
 * Multidimensional Array Numeric Sort
 *  @param array Array to sort
 *  @param int Column to sort
 *  @param str Direction to sort - ASC or DESC
 */
function arraySortNumeric(sortArray, column, direction) {
  if (direction == 'ASC') {
    return sortArray.sort((a, b) => a[column] - b[column]);
  }
  else if (direction == 'DESC') {
    return sortArray.sort((a, b) => b[column] - a[column]);
  }
  return sortArray;
}


/**
 * Multidimensional Array Text Sort
 *  @param array Array to sort
 *  @param int Column to sort
 *  @param str Direction to sort - ASC or DESC
 */
function arraySortText(sortArray, column, direction) {
  if (direction == 'ASC') {
    return sortArray.sort((a, b) => {
      if (a[column] === b[column]) return 0;
      return a[column] < b[column] ? -1 : 1;
    });
  }
  else if (direction == 'DESC') {
    return sortArray.sort((a, b) => {
      if (a[column] === b[column]) return 0;
      return a[column] > b[column] ? -1 : 1;
    });
  }
  return sortArray;
}


/**
 * Flip Sort Direction
 *  Switch sortDirection between ASC / DESC
 *
 */
function flipSort() {
  if (sortDirection == 'ASC') {
    sortDirection = 'DESC';
  }
  else {
    sortDirection = 'ASC';
  }
}

/**
 * Format Log Time
 *  Converts log time in UTC into local time
 *  Date is dropped if log time is today
 *
 *  @param str ISO 8601 formatted date time
 *  @returns str Local time
 *
 */
function formatTime(dateTimeISO) {
  const dt = new Date(`${dateTimeISO}Z`);
  const midnight = new Date();
  midnight.setHours(0, 0, 0, 0);

  let year = dt.getFullYear();
  let mon = `0${dt.getMonth() + 1}`;
  let day = `0${dt.getDate()}`;
  let hour = `0${dt.getHours()}`;
  let mins = `0${dt.getMinutes()}`;
  let secs = `0${dt.getSeconds()}`;

  if (midnight > dt) {
    return `${year}-${mon.substr(-2)}-${day.substr(-2)} ${hour.substr(-2)}:${mins.substr(-2)}:${secs.substr(-2)}`;
  }
  else {
    return `${hour.substr(-2)}:${mins.substr(-2)}:${secs.substr(-2)}`;
  }
}

/**
 * Simplify Domain
 *  Limit the length of a domain in dialog boxes
 *  1. Check if length is under MAXDOMAINLEN with a bit of extra leeway (don't want to dot if not much of a gain)
 *  2. Extract domain and get length
 *  3. Check if the focus should be at reducing the overall domain or just subdomain
 *
 *  @param str Domain
 *  @return str Simplified domain
 */
function simplifyDomain(subDomain) {
  const MAXDOMAINLEN = 34;
  let subDomainLen = subDomain.length;
  let availChars = 0

  if (subDomainLen <= (MAXDOMAINLEN + 2)) {                          //Some extra leeway
    return `${subDomain}<wbr>`;
  }

  let domainName = TOPDOMAIN_REGEX.exec(subDomain)[0];
  let domainLen = domainName.length;

  if (domainLen < (MAXDOMAINLEN - 2)) {
    availChars = Math.round((MAXDOMAINLEN - domainLen) / 2);
    return subDomain.substring(0, availChars)+'…'+subDomain.substring(subDomainLen - (domainLen + availChars))+'<wbr>';
  }
  else {
    availChars = MAXDOMAINLEN / 2;
    return subDomain.substring(0, (availChars - 3))+'…'+subDomain.substring((subDomainLen - 3) - availChars+'<wbr>');
  }
}


/**
 * Block Domain
 *  Called by popup menu "Allow / Block Domain"
 *  Prepares elements on queries-dialog to allow user to submit either a
 *  subdomain or top public domain to customblocklist.php
 *
 *  @param str Domain name
 *  @param str Action to take - allow or block
 */
function blockDomain(domain, action) {
  let button1 = '';                                       //Text for buttons and legends
  let button2 = '';
  let legend1 = '';
  let legend2 = '';
  let simplifiedDomain = '';                              //Alternate text for domain to fit in message box
  let simplifiedTopDomain = '';

  const blockTitle = document.getElementById('blocktitle');

  if ((action != 'allow') && (action != 'block')) {
    console.error('Invalid action specified');
    return;
  }

  //Check if supplied domain at least matches a basic top domain.
  if (! TOPDOMAIN_REGEX.test(domain)) {
    console.error('Invalid domain specified');
    return;
  }

  topDomain = TOPDOMAIN_REGEX.exec(domain)[0];            //Get just the top public domain part of the domain

  simplifiedDomain = simplifyDomain(domain);
  simplifiedTopDomain = simplifyDomain(topDomain);

  //Just allowing or blocking a single top public domain
  if (action == 'allow') {
    blockTitle.innerText = 'Allow Domain';
    legend1 = `<legend>Add <b>${simplifiedTopDomain}</b> to Allow List</legend>`;
    button1 = `<button name="singledomain" value="${topDomain}" type="submit">Allow Entire Top Domain</button>`;
  }
  else if (action == 'block') {
    blockTitle.innerText = 'Block Domain';
    legend1 = `<legend>Add <b>${simplifiedTopDomain}</b> to Block List</legend>`;
    button1 = `<button name="singledomain" value="${topDomain}" type="submit">Block Entire Top Domain</button>`;
  }

  //Offering choice of top public domain or subdomains to allow or block
  if (topDomain != domain) {
    if (action == 'allow') {
      legend2 = `<legend>Add <b>${simplifiedDomain}</b> to Allow List</legend>`;
      button2 = `<button name="singledomain" value="${domain}" type="submit">Allow Sub-Domain</button>`;
    }
    else if (action == 'block') {
      legend2 = `<legend>Add <b>${simplifiedDomain}</b> to Block List</legend>`;
      button2 = `<button name="singledomain" value="${domain}" type="submit">Block Sub-Domain</button>`;
    }
  }

  //Fill in the HTML for blockitem1 and blockitem2 fieldsets
  document.getElementById('blockitem1').innerHTML = `${legend1}${button1}`;
  document.getElementById('blockitem2').innerHTML = `${legend2}${button2}`;
  document.getElementById('blockitem2').style.display = (legend2 == '') ? 'none' : 'block';

  document.getElementById('blockAction').value = action;

  //Display fade and queries-box
  document.getElementById('fade').style.display = 'block';
  document.getElementById('queries-dialog').style.display = 'block';
}


/**
 * Report Domain
 *  Fill in elements for report-dialog
 *
 *  @param str domain name
 */
function reportDomain(domain) {
  let simplifiedDomain = '';                              //Alternate text for domain to fit in message box

  simplifiedDomain = simplifyDomain(domain);

  document.getElementById('reportTitle').innerHTML = simplifiedDomain;
  document.getElementById('reportInput').value = domain;

  //Display fade and report-dialog
  document.getElementById('fade').style.display = 'block';
  document.getElementById('report-dialog').style.display = 'block';
}


/********************************************************************
 *  Hide Dialogs
 *
 */
function hideDialogs() {
  document.getElementById('queries-dialog').style.display = 'none';
  document.getElementById('report-dialog').style.display = 'none';
  document.getElementById('fade').style.display = 'none';
}


/**
 * Hide Report Dialog
 *
 */
function hideReportDialog() {
  document.getElementById('report-dialog').style.display = 'none';
  document.getElementById('fade').style.display = 'none';
}


/**
 * Format Date
 *  Convert ISO 8601 Time String to Short Month-Day HH:MM
 *  @param str ISO 8601 timedate string
 *  @return Formatted datetime string
 */
function formatSearchDate(dateTimeISO) {
  let shortMonth = '';
  let tempDate= new Date(dateTimeISO);

  //Get 3 letter month from Intl DateTimeFormat
  shortMonth = new Intl.DateTimeFormat('default', {month: 'short'}).format(tempDate);

  //Extract the original Date and Time out of regex YYYY-MM-(DD)T(HH:MM)
  matches = dateTimeISO.match(/\d{4}\-\d{2}\-(\d{2})T(\d{1,2}:\d{1,2})/);

  return `${matches[1]} ${shortMonth} ${matches[2]}`;
}


/**
 * Set Time Preset from timepicker menu
 *  @param str dtPreset from data-dtduration
 *  @param str presetDisplay from innerText of selected LI element
 *
 */
function setTimePreset(dtPreset, presetDisplay) {
  document.getElementById('timepicker-text').value = presetDisplay;
  document.getElementById('datetime').value = dtPreset;

  document.getElementById('timepicker-dropdown').blur();   //Move focus to the submit button
  document.getElementById('submit-button').focus();
}


/**
 * Select Time & Date Range from Time Picker Menu
 *   Set timepicker-text with formatSearchDate value of start and end dates & times
 *   Set in datetime value with selected dates and  with added seconds 00 to 59
 *
 */
function selectTimeDate() {
  let startDate = document.getElementById('dtl-from').value;
  let endDate = document.getElementById('dtl-to').value;

  if (startDate === '') return;
  if (endDate === '') return;


  //Check if startDate in Unix time is greater than endDate in Unix time
  if (Date.parse(startDate) > Date.parse(endDate)) {
     [startDate, endDate] = [endDate, startDate];           //Swap the values
  }

  document.getElementById('timepicker-text').value = formatSearchDate(startDate) + ' to ' + formatSearchDate(endDate);

  //Need to add the seconds
  document.getElementById('datetime').value = startDate + ':00/' + endDate + ':59';

  document.getElementById('timepicker-dropdown').blur();   //Move focus to the submit button
  document.getElementById('submit-button').focus();
}


/********************************************************************
 *  Toggle Nav Button
 *    Toggle active state of severity button
 *
 *  Params:
 *    Button, Value to increase or decrease severity by
 *  Return:
 *    None
 */
function toggleNavButton(item, value) {
  let severity = document.getElementById('severity');
  let severityInt = Number(severity.value);

  if (item.classList.contains('active')) {
    severityInt -= Number(value);
    item.classList.remove('active');
  }
  else {
    severityInt += Number(value);
    item.classList.add('active');
  }

  severity.value = severityInt.toString();
}


/**
 * Delete all rows below the table header
 *  @param object Table Element
 */
function clearTable(tb) {
  while(tb.rows.length > 1) {
    tb.deleteRow(1);
  }
}


/**
 * Create Domain Cell
 *  Generate the HTML elements which go after the the domain name
 *
 *  @param str Domain name / DNS request
 *  @param number Severity from SQL Data
 *  @param string Block List Source from SQL Data
 *  @return HTML elements for clipboard and blocked by x list
 */
function createDomainCell(dnsRequest, severity, blSource) {
  let blName = '';
  let clipboard = ''

  //Create clipboard image and text
  clipboard = `<div class="icon-clipboard" onclick="setClipboard('${dnsRequest}')" title="Copy domain">&nbsp;</div>`;

  //Severity 1 (allowed) requires no further elements
  if (severity == 1) {
    return clipboard;
  }
  //Severity 2 (blocked) add blocklist name
  if (severity == 2) {
    if (BLOCKLISTS.hasOwnProperty(blSource)) {
      blName = '<i>Blocked by ' + getBlocklistName(blSource) + '</i>';
    }
  }
  //Severity 3 add either advert / tracker accessed or blocklist name
  else if (severity == 3) {
    if ((blSource == 'advert') || (blSource == 'tracker')) {
      blName = '<i>' + blSource[0].toUpperCase() +
        blSource.substr(1) + ' Accessed</i>';
    }
    else {
      blName = '<i>Domain in ' + getBlocklistName(blSource) + ' Accessed</i>';
    }
  }

  return `${clipboard}${blName}`;
}

/**
 * Create Image Cell
 *  Generate the HTML Img tag for the block list type
 *
 *  @param number Severity from SQL Data
 *  @param string Block List Source from SQL Data
 *  @return string Formatted Img tag
 */
function createImageCell(severity, blSource) {
  let blType = ''                                          //Block List Type
  let imgTitle = ''

  //Severity 1 (allowed) is already the correct type
  if (severity == 1) {
    blType = blSource;
    imgTitle = EVENT_NAMES[blType];
  }
  //Severity 2 (blocked) requires looking up the type of block list
  else if (severity == 2) {
    if (BLOCKLISTS.hasOwnProperty(blSource)) {
      blType = BLOCKLISTS[blSource][1];
      imgTitle = 'Blocked';
    }
  }
  //Severity 3 (allowed advert/tracker or blocked malware)
  else if (severity == 3) {
    //Was it a found Advert or Tracker?
    if ((blSource == 'advert') || (blSource == 'tracker')) {
      blType = blSource;
      imgTitle = EVENT_NAMES[blSource];
    }
    //Or lookup the block list type
    else {
      if (BLOCKLISTS.hasOwnProperty(blSource)) {
        blType = BLOCKLISTS[blSource][1];
      }
      imgTitle = 'Blocked';
    }
  }

  //Nothing found - Likely to be a custom block list
  if (blType == '') {
    blType = 'custom';
  }

  return `<img title="${imgTitle}" src="./svg/events/${blType}${severity}.svg" alt="">`;
}


/**
 * Create Popup Menu
 *  Generate HTML Menu for queries table
 *  An allowed request results in option to block
 *  A blocked request results in option to allow
 *
 *  @param string DNS Request from SQL Data
 *  @param number Severity from SQL Data
 *  @param string Block List Source from SQL Data
 *  @return string Formatted Menu
 */
function createPopupMenu(dnsRequest, severity, blSource, pauseQueue=false) {
  let menu = '';
  let mouseFunctions = '';

  if (pauseQueue) {
    mouseFunctions = ' onmouseover="pauseQueue()" onmouseout="pauseQueue()"';
  }

  menu = `<div class="dropdown-container"${mouseFunctions}><span class="dropbtn"></span><div class="dropdown">`;

  //Severity 1 with Allowed or Cached response (ignore local lookups)
  if ((severity == 1) && (blSource != 'local')) {
    menu += `<span onclick="reportDomain('${dnsRequest}')">Report Domain</span>`;
    menu += `<span onclick="blockDomain('${dnsRequest}', 'block')">Block Domain</span>`;
  }
  //Severity 2 (blocked)
  else if (severity == 2) {
    menu += `<span onclick="blockDomain('${dnsRequest}', 'allow')">Allow Domain</span>`;
  }
  //Severity 3 (allowed advert/tracker or blocked malware)
  else if (severity == 3) {
    //Allowed advert/tracker
    if ((blSource == 'advert') || (blSource == 'tracker')) {
      menu += `<span onclick="reportDomain('${dnsRequest}')">Report Domain</span>`;
      menu += `<span onclick="blockDomain('${dnsRequest}', 'block')">Block Domain</span>`;
    }
    //Malware that would have been blocked, but was allowed
    else {
      menu += `<span onclick="blockDomain('${dnsRequest}', 'allow')">Allow Domain</span>`;
    }
  }

  //Other options - Investigate, Search, VirusTotal
  menu += `<a href="${INVESTIGATE_URL}${dnsRequest}">${INVESTIGATE}</a>`;
  menu += `<a href="${SEARCH_URL}${dnsRequest}" target="_blank">${SEARCH_ENGINE}</a>`;
  menu += `<a href="https://www.virustotal.com/gui/domain/${dnsRequest}/summary" target="_blank">VirusTotal</a>`;
  menu += '</div></div>';

  return menu;
}


/**
 * Export Table to users downloads
 *  Used for all client-side production of tables.
 *  1. Set table headers based on the tableName.
 *  2. Build csvData from each row of DATA, adding in newline character.
 *  3. Create a temporary link with csvData as a blob.
 *
 *  @param str Unique table name to set table headers and download name.
 */
function exportTable(tableName) {
  let dataLength = 0;
  let fileName = '';
  let tableHeader = '';
  let csvData = [];

  //Zero results from SQL query will result in DATA not being defined by PHP script,
  //although the export button will be available.
  if (typeof DATA === 'undefined') {
    console.error('No table data defined');
    return;
  }

  dataLength = DATA.length;

  //Set fileName and table headers based on value of tableName parameter.
  switch(tableName) {
    case 'domainsBlocked':
      fileName = 'notrack_blocklist.csv';
      tableHeader = 'row,blocklist,domain,comment\n';
      break;
    case 'queriesDomain':
      fileName = 'queries_by_domain.csv';
      tableHeader = 'row,dns_request,severity,bl_source,count\n';
      break;
    case 'queriesTime':
      fileName = 'queries_by_time.csv';
      tableHeader = 'row,time,system,dns_request,severity,bl_source\n';
      break;
    default:
      console.error(`Unknown table - ${tableName}`);
      return;
  }

  csvData += tableHeader;
  //Add all rows from DATA, appending a newline character, otherwise all data will be on one row in the CSV.
  for(let i = 0; i < dataLength; i++) {
    csvData += DATA[i].join(',') + '\n';
  }

  //Create a temporary link element containing csvData
  csvFile = new Blob([csvData], {type: 'text/csv'});
  downloadLink = document.createElement("a");
  downloadLink.download = fileName;
  downloadLink.href = window.URL.createObjectURL(csvFile);
  downloadLink.style.display = "none";

  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);
}


/**
 * Get Human readable block list name from BLOCKLISTS array
 *  @param blSource from SQL column bl_source
 *  @return Human readable block list name, or custom as a fallback
 */
function getBlocklistName(blSource) {
  if (BLOCKLISTS.hasOwnProperty(blSource)) {
    return BLOCKLISTS[blSource][2];
  }

  return 'Custom';
}


/**
 * Populate Domains Blocked
 *  Populates table from SQL data
 *  @param int Current Page
 *
 */
function populateBlockedTable(currentPage) {
  const tb = document.getElementById('block-table');
  const dataLength = DATA.length;

  startPoint = (currentPage - 1) * ROWSPERPAGE;
  if (startPoint > dataLength) {
    currentPage = 1;
    startPoint = 0;
  }

  endPoint = startPoint + ROWSPERPAGE;
  endPoint = (endPoint > dataLength) ? dataLength : endPoint;

  pagination(dataLength, currentPage, 'populateBlockedTable');
  clearTable(tb);

  for(let i = startPoint; i < endPoint; i++) {
    let row = tb.insertRow();
    let cell0 = row.insertCell(0);
    let cell1 = row.insertCell(1);
    let cell2 = row.insertCell(2);
    let cell3 = row.insertCell(3);

    cell0.innerText = DATA[i][0]
    cell1.innerText = getBlocklistName(DATA[i][1]);
    cell2.innerHTML = DATA[i][2] + createDomainCell(DATA[i][2], 1, '');
    cell3.innerText = DATA[i][3];
  }
}


/**
 * Populate Name Table
 *  Populates table from SQL data
 *  @param int Current Page
 */
function populateNameTable(currentPage) {
  let dnsRequest = '';
  let severity = 0;
  let blSource = '';
  let dataLength = 0;
  let endPoint = 0;
  let startPoint = 0;
  let tb = document.getElementById('query-group-table');

  dataLength = DATA.length;

  startPoint = (currentPage - 1) * ROWSPERPAGE;
  if (startPoint > dataLength) {
    currentPage = 1;
    startPoint = 0;
  }

  endPoint = startPoint + ROWSPERPAGE;
  endPoint = (endPoint > dataLength) ? dataLength : endPoint;

  pagination(dataLength, currentPage, 'populateNameTable');
  clearTable(tb);

  for(let i = startPoint; i < endPoint; i++) {
    let row = tb.insertRow();
    let cell0 = row.insertCell(0);
    let cell1 = row.insertCell(1);
    let cell2 = row.insertCell(2);
    let cell3 = row.insertCell(3);
    let cell4 = row.insertCell(4);

    dnsRequest = DATA[i][1];
    severity = DATA[i][2];
    blSource = DATA[i][3];

    cell0.innerHTML = createImageCell(severity, blSource);
    cell1.innerText = DATA[i][0];                         //row_num
    cell2.innerHTML = dnsRequest + createDomainCell(dnsRequest, severity, blSource);
    cell3.innerText = DATA[i][4];                         //count
    cell4.innerHTML = createPopupMenu(dnsRequest, severity, blSource);
  }
}


/**
 * Populate Time Table
 *  Populates table from SQL data
 *  @param int Current Page
 */
function populateTimeTable(currentPage) {
  let blSource = '';
  let dateTime = '';
  let domainURL = '';
  let dnsRequest = '';
  let severity = 0;
  let sysIP = '';
  let dataLength = 0;
  let j = 0;
  let startPoint = 0;
  let tb = document.getElementById('query-time-table');

  dataLength = DATA.length;

  startPoint = (currentPage - 1) * ROWSPERPAGE;
  if (startPoint > dataLength) {
    currentPage = 1;
    startPoint = 0;
  }

  pagination(dataLength, currentPage, 'populateTimeTable');
  clearTable(tb);

  for(let i = startPoint; i < dataLength; i++) {
    let row = tb.insertRow();
    let cell0 = row.insertCell(0);
    let cell1 = row.insertCell(1);
    let cell2 = row.insertCell(2);
    let cell3 = row.insertCell(3);
    let cell4 = row.insertCell(4);

    dateTime = DATA[i][1];
    sysIP = DATA[i][2];
    dnsRequest = DATA[i][3];
    severity = DATA[i][4];
    blSource = DATA[i][5];

    //Create link to Investigate page using dateTime, dnsRequest, sysIP
    domainURL = '<a href="./investigate.php?datetime=' + encodeURIComponent(dateTime);
    domainURL += '&amp;subdomain=' + encodeURIComponent(dnsRequest) + '&amp;sys=' + encodeURIComponent(sysIP) + '">';
    domainURL += dnsRequest + '</a>';

    cell0.innerHTML = createImageCell(severity, blSource);
    cell1.innerText = formatTime(dateTime)
    cell2.innerText = sysIP;
    cell3.innerHTML = domainURL + createDomainCell(dnsRequest, severity, blSource);
    cell4.innerHTML = createPopupMenu(dnsRequest, severity, blSource);

    j++;
    if (j >= ROWSPERPAGE) break;
  }
}


/**
 * Populate Investigate Table
 *  Similar to Time Table, pagination not required, and need to highlight searched domain
 *  @param str Domain to highlight when its found in SQL data
 */
function populateInvestigateTable(highlightDomain) {
  let blSource = '';
  let dateTime = '';
  let domainURL = '';
  let dnsRequest = '';
  let severity = 0;
  let sysIP = '';
  let dataLength = 0;
  let startPoint = 0;
  let tb = document.getElementById('query-time-table');

  dataLength = DATA.length;

  clearTable(tb);

  for(let i = 0; i < dataLength; i++) {
    let row = tb.insertRow();
    let cell0 = row.insertCell(0);
    let cell1 = row.insertCell(1);
    let cell2 = row.insertCell(2);
    let cell3 = row.insertCell(3);
    let cell4 = row.insertCell(4);

    dateTime = DATA[i][1];
    sysIP = DATA[i][2];
    dnsRequest = DATA[i][3];
    severity = DATA[i][4];
    blSource = DATA[i][5];

    //Create link to Investigate page using dateTime, dnsRequest, sysIP
    domainURL = '<a href="./investigate.php?datetime=' + encodeURIComponent(dateTime);
    domainURL += '&amp;subdomain=' + encodeURIComponent(dnsRequest) + '&amp;sys=' + encodeURIComponent(sysIP) + '">';
    domainURL += dnsRequest + '</a>';

    if (dnsRequest == highlightDomain) {                  //tr highlighting to add cyan class
      row.classList.add('cyan');
    }

    cell0.innerHTML = createImageCell(severity, blSource);
    cell1.innerText = formatTime(dateTime);
    cell2.innerText = sysIP;
    cell3.innerHTML = domainURL + createDomainCell(dnsRequest, severity, blSource);
    cell4.innerHTML = createPopupMenu(dnsRequest, severity, blSource);
  }
}


/**
 * Create Pagination Buttons
 *  Draw up to 7 buttons
 *  Current Page == 1 [ ] [c] [2] [3] [>]
 *  Current Page != 1 [<] [1] [c] [c+1] [c+2] [c+3] [Last] [>]
 *
 *  @param int Total Number of Rows in SQL Results
 *  @param int Current Page which can be controlled client-side
 *  @param str populateNameTable or populateTimeTable
 */
function pagination(totalRows, currentPage, view) {
  let totalPages = 0;
  let startLoop = 0;
  let endLoop = 0;

  let ul = '<ul>';

  if (totalRows <= ROWSPERPAGE) {                         //Is Pagination required?
    document.getElementById('pagination').innerHTML = '';
    return;
  }
  totalPages = Math.ceil(totalRows / ROWSPERPAGE);        //Calculate List Size

  if (currentPage == 1) {                                 // [ ] [1]
    ul += '<li><span>&nbsp;&nbsp;</span></li>';
    ul += '<li class="active" onClick="' + view + '(1)"><span>1</span></li>'
    startLoop = 2;
    if (totalPages > 4)  {
      endLoop = currentPage + 4;
    }
    else {
      endLoop = totalPages;
    }
  }
  else {                                                  // [<] [1]
    ul += '<li onClick="' + view + '(' + (currentPage-1) + ')"><span>&#x00AB;</span></li>';
    ul += '<li onClick="' + view + '(1)"><span>1</span></li>'

    if (totalPages < 5) {
      startLoop = 2;                                      // [1] [2] [3] [4] [L]
    }
    else if ((currentPage > 2) && (currentPage > totalPages -4)) {
      startLoop = (totalPages - 3);                       //[1]  [x-1] [x] [L]
    }
    else {
      startLoop = currentPage;                            // [1] [x] [x+1] [L]
    }

    if ((totalPages > 3) && (currentPage < totalPages - 2)) {
      endLoop = currentPage + 3;                          // [y] [y+1] [y+2] [y+3]
    }
    else {
      endLoop = totalPages;                               // [1] [x-2] [x-1] [y] [L]
    }
  }

  for (let i = startLoop; i < endLoop; i++) {             //Loop to draw 3 buttons
    if (i == currentPage) {
      ul += '<li class="active" onClick="' + view + '(' + i + ')"><span>' + i + '</span></li>';
    }
    else {
      ul += '<li onclick="' + view + '(' + i + ')"><span>' + i + '</span></li>';
    }
  }

  if (currentPage == totalPages) {                        // [Final] [ ]
    ul += '<li class="active" onclick = "' + view + '(' + totalPages + ')"><span>' + totalPages + '</span></li>';
    ul += '<li><span>&nbsp;&nbsp;</span></li>';
  }
  else {                                                  // [Final] [>]
    ul += '<li onClick="' + view + '(' + totalPages + ')"><span>' + totalPages + '</span></li>';
    ul += '<li onClick="' + view + '(' + (currentPage+1) + ')"><span>&#x00BB;</span></li>';
  }

  ul += '</ul>';
  document.getElementById('pagination').innerHTML = ul;
}


/**
 * Sort Blocked Domain Table
 *  Search and Sort ORIGINDATA into DATA array
 *  1. Carry out user searches
 *  2. Update table headers to show which column is being sorted
 *  3. Sort by selected column
 *
 *  @param int optional sort column - specified by TH onclick event
 *  @note this function can also be triggered by text search input update,
 *   and therefore wouldn't require sortColumn to be specified
 */
function sortBlockedTable(newSortColumn = -1) {
  const searchText = document.getElementById('searchbox').value;
  const searchBL = document.getElementById('searchbl').value;

  //Reset DATA back to ORIGINDATA
  DATA = [];
  DATA = ORIGINDATA;

  if (searchBL != 'all') {                                //Blocklist search
    DATA = arrayStringSearch(ORIGINDATA, searchBL, 1);
  }
  if (searchText != '') {                                 //Domain name search
    DATA = arrayFlexSearch(DATA, searchText, 2);
  }

  if (newSortColumn != -1) {                              //Has a TH been clicked?
    if (newSortColumn == sortColumn) {                    //Same TH has current column
      flipSort();
    }
    else {                                                //Different column
      sortColumn = newSortColumn;
    }
    //Update sort direction and sort column highlighting
    updateTableHeaders('block-table', sortColumn, sortDirection);
  }

  if (sortColumn == 0) {                                  //# selected
    DATA = arraySortNumeric(DATA, sortColumn, sortDirection);
  }
  else {                                                  //Blocklist or Domain selected
    DATA = arraySortText(DATA, sortColumn, sortDirection);
  }

  //Reset pagination position, as there is no global variable for current location
  populateBlockedTable(1);
}


/**
 * Update Table Headers
 *  Updates all TH elements containing sortable class to indicate selected
 *   sort column and direction of sort.
 *
 *  @param str Table ID
 *  @param int Active column to highlight
 *  @param str Sort direction
 *  @note sort direction text is specified by CSS class asc/desc.
 *
 */
function updateTableHeaders(tableName, activeColumn, direction) {
  const headerRow = document.getElementById(tableName).rows[0];
  const headerRowLength = headerRow.children.length;

  for (let i = 0; i < headerRowLength; i++) {
    let cell = headerRow.children[i];
    if (cell.classList.contains('sortable')) {
      if (activeColumn == i) {
        cell.className = 'sortable active ' + direction.toLowerCase();
      }
      else {
        cell.className = 'sortable';
      }
    }
  }
}
