<?php
/**
 * Draw Navigation Panel
 *
 */
function draw_page_nav() {
  $alert_count = 0;
  $alert_count = nav_alert_count();

  echo '<nav>', PHP_EOL;
  echo '<a class="icon-dashboard" href="/admin/" title="Dashboard">Dashboard</a>', PHP_EOL;
  echo '<a class="icon-dns" href="/admin/queries.php" title="DNS Queries">DNS Queries</a>', PHP_EOL;
  echo '<a class="icon-network" href="/admin/dhcp.php" title="Network">Network</a>', PHP_EOL;
  echo '<a class="icon-television" href="/admin/live.php" title="Live">Live</a>', PHP_EOL;

  //Only display an alert count if its above zero
  if ($alert_count == 0) {
    echo '<a class="icon-bell" href="/admin/analytics.php" title="Alerts">Alerts</a>', PHP_EOL;
  }
  else {
    echo '<a class="icon-bell" href="/admin/analytics.php" title="Alerts">Alerts<span>', formatnumber($alert_count), '</span></a>', PHP_EOL;
  }

  echo '<a class="icon-investigate" href="/admin/investigate.php" title="Investigate">Investigate</a>', PHP_EOL;
  echo '<a class="icon-cog" href="/admin/config" title="Config">Config</a>', PHP_EOL;
  echo '<a class="icon-help" href="/admin/help" title="Help">Help</a>', PHP_EOL;

  nav_system_status();
  echo '</nav>', PHP_EOL;
}


/**
 * Draw Header Panel
 *
 *  @param str page title
 *
 */
function draw_page_header($title) {
  global $config;

  echo '<header>', PHP_EOL;
  echo '<div onclick="openNav()" role="button" tabindex="0" aria-pressed="false" aria-haspopup="true">&#9776;</div>', PHP_EOL;  //Hamburger menu to show nav on mobile view

  echo '<a href="/admin/"><span><b>No</b>Track - </span><span>', $title, '</span></a>', PHP_EOL;

  if ($config->status & STATUS_ENABLED) {
    echo '<div id="stop-button" class="icon-stop" role="button" tabindex="0" aria-pressed="false" title="Disable Blocking" onclick="enableNoTrack()"></div>', PHP_EOL;
  }
  else {
    echo '<div id="stop-button" class="icon-play" role="button" tabindex="0" aria-pressed="false" title="Enable Blocking" onclick="enableNoTrack()"></div>', PHP_EOL;
  }

  //Dropdown menu for default pause times
  echo '<div class="icon-pause" role="button" tabindex="0" aria-pressed="false" aria-haspopup="true" title="Pause for...">', PHP_EOL;
  echo '<ul id="pause-menu">', PHP_EOL;
  echo '<li onclick="pauseNoTrack(5)">Pause for 5 minutes</li>', PHP_EOL;
  echo '<li onclick="pauseNoTrack(15)">Pause for 15 minutes</li>', PHP_EOL;
  echo '<li onclick="pauseNoTrack(30)">Pause for 30 minutes</li>', PHP_EOL;
  echo '<li onclick="pauseNoTrack(60)">Pause for 1 Hour</li>', PHP_EOL;
  echo '<li onclick="pauseNoTrack(120)">Pause for 2 Hours</li>', PHP_EOL;
  echo '</ul></div>', PHP_EOL;

  if ($config->status & STATUS_INCOGNITO) {
    echo '<div id="incognito-button" class="icon-incognito-off" role="button" tabindex="0" aria-pressed="false" title="Incognito Mode" onclick="menuIncognito()"></div>', PHP_EOL;
  }
  else {
    echo '<div id="incognito-button" class="icon-incognito" role="button" tabindex="0" aria-pressed="false" title="Incognito Mode" onclick="menuIncognito()"></div>', PHP_EOL;
  }

  //Show Logout button if use has set a password
  if ($config->is_password_protection_enabled()) {
    echo '<div class="icon-logout" role="button" tabindex="0" aria-pressed="false" title="Logout" onclick="location.href=\'/admin/logout.php\'"></div>', PHP_EOL;
  }

  echo '</header>', PHP_EOL;
}


/**
 * Side Menu Alert Count
 *  1. Attempt to load alert_count value from Memcache
 *  2. count_alert function is provided from $dbwrapper, but the current page may not contain that object
 *  3. Once obtained store the value in Memcache for 1 hour
 *
 *  @return  alert_count
 */
function nav_alert_count() {
  global $dbwrapper, $mem;

  $alert_count = 0;

  $alert_count = $mem->get('alert_count');

  if (empty($alert_count)) {
    if (isset($dbwrapper)) {
      $alert_count = $dbwrapper->analytics_count();
      $mem->set('alert_count', $alert_count, 0, 3600);
    }
  }

  return $alert_count;
}

/**
 * Side Menu Status
 *  Draw the system status section on Nav bar
 *
 */
function nav_system_status() {
  global $config;

  $figure_class = '';
  $sysload0 = 0.0;
  $sysload1 = 0.0;
  $sysload2 = 0.0;

  //Get the server load averages and format the numbers into 2 decimal places
  $sysload = sys_getloadavg();
  $sysload0 = number_format($sysload[0], 2);
  $sysload1 = number_format($sysload[1], 2);
  $sysload2 = number_format($sysload[2], 2);

  //Get the server memory usage from the output of free -m
  $freemem = preg_split('/\s+/', exec('free -m | grep Mem'));
  $mempercentage = round(($freemem[2]/$freemem[1])*100);

  echo '<div class="icon-status">System Status</div>', PHP_EOL;

  //Line 1 - Blocking Status
  if ($config->status & STATUS_ENABLED) {
    if (file_exists(NOTRACK_LIST)) {
      echo '<figure id="nav-blocking" class="status_green"><figcaption>Blocking: Enabled</figcaption></figure>', PHP_EOL;
    }
    else {
      echo '<figure id="nav-blocking" class="status_red"><figcaption>Blocklist Missing</figcaption></figure>', PHP_EOL;
    }
  }
  elseif ($config->status & STATUS_PAUSED) {
    echo '<figure id="nav-blocking" class="status_yellow"><figcaption>Blocking: Paused - ', date('H:i', $config->unpausetime), '</figcaption></figure>', PHP_EOL;
  }
  elseif ($config->status & STATUS_DISABLED) {
    echo '<figure id="nav-blocking" class="status_red"><figcaption>Blocking: Disabled</figcaption></figure>', PHP_EOL;
  }

  //Line 2 - Memory Usage
  if ($mempercentage > 75) {
    $figure_class = 'status_red';
  }
  elseif ($mempercentage > 65) {
    $figure_class = 'status_yellow';
  }
  else {
    $figure_class = 'status_green';
  }
  echo "<figure class=\"{$figure_class}\"><figcaption>Memory Used: {$mempercentage}%</figcaption></figure>", PHP_EOL;

  //Line 3 - Server load
  if ($sysload0 > 0.85) {
    $figure_class = 'status_red';
  }
  elseif ($sysload0 > 0.60) {
    $figure_class = 'status_yellow';
  }
  else {
    $figure_class = 'status_green';
  }
  echo "<figure class=\"{$figure_class}\"><figcaption>Load: {$sysload0} | {$sysload1} | {$sysload2}</figcaption></figure>", PHP_EOL;
}
