<?php
require('./global-vars.php');
require('./global-functions.php');
require('./config.php');
require('./mysqlidb.php');

header('content-Type: application/json; charset=UTF-8');

/************************************************
*Constants                                      *
************************************************/
define('API_INVALID', 0);
define('API_READONLY', 1);
define('API_READWRITE', 2);

/************************************************
*Global Variables                               *
************************************************/
$response = array();


/********************************************************************
 *  Blocklist Status
 *    Set new status of a blocklist
 *    Save blocklist settings
 *    Return a friendly message
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function api_blocklist_status() {
  global $config, $response;

  $newstatus = false;                                      //True / False status
  $response_text = '';                                     //Friendly response

  $blname = $_POST['blname'] ?? '';
  $blstatus = $_POST['blstatus'] ?? '';

  $config->load_blocklists();                              //Load the existing settings

  if ($blstatus == 'true') {
    $newstatus = true;
    $response_text = 'Enabled';
  }
  else {
    $response_text = 'Disabled';
  }

  if ($config->set_blocklist_status($blname, $newstatus)) {//Was status update successful?
    $config->save_blocklists();                            //Save new blocklist settings
    $response['message'] = $config->get_blocklistname($blname).' '.$response_text;
  }
  else {                                                   //Failed update
    http_response_code(400);
    $response['error_code'] = 'missing_required_parameter';
    $response['error_message'] = 'Invalid blocklist name '.$blname;
  }
}


/********************************************************************
 *  Enable NoTrack
 *    Enable or Disable NoTrack Blocking
 *
 *  Params:
 *    None
 *  Return:
 *    None
 */
function api_enable_notrack() {
  global $config, $mem, $response;

  $newstatus = 0;

  if ($config->status & STATUS_ENABLED) {
    $newstatus = $config->status - STATUS_ENABLED;
    $newstatus += STATUS_DISABLED;
  }
  elseif ($config->status & STATUS_PAUSED) {
    $newstatus = $config->status - STATUS_PAUSED;
    $newstatus += STATUS_ENABLED;
  }
  elseif ($config->status & STATUS_DISABLED) {
    $newstatus = $config->status - STATUS_DISABLED;
    $newstatus += STATUS_ENABLED;
  }
  else {                                                   //Fallback in case of error
    $newstatus = STATUS_ENABLED;
  }

  $config->save_status($newstatus, 0);
  $response['status'] = $newstatus;
}


/********************************************************************
 *  Pause NoTrack
 *    Pause NoTrack with time parsed in POST mins
 *    Delete conf-settings from memcache so we force a load of config next page user views
 *
 *  Params:
 *    None
 *  Return:
 *    false on error
 *    true on success
 */
function api_pause_notrack() {
  global $config, $mem, $response;

  $mins = 0;
  $newstatus = 0;
  $unpausetime = 0;

  if (! isset($_POST['mins'])) {
    $response['error'] = 'api_pause_notrack: Mins not specified';
    return false;
  }

  $mins = filter_integer($_POST['mins'], 1, 1440, 5);      //1440 = 24 hours in mins
  $unpausetime = time() + ($mins * 60);

  if ($config->status & STATUS_INCOGNITO) {
    $newstatus = STATUS_INCOGNITO + STATUS_PAUSED;
  }
  else {
    $newstatus = STATUS_PAUSED;
  }

  $config->save_status($newstatus, $unpausetime);
  $response['status'] = $newstatus;
  $response['unpausetime'] = date('H:i', $unpausetime);

  return true;
}


/********************************************************************
 *  API Incognito
 *    Switch incognito status based on bitwise value of config->status
 *  Params:
 *    None
 *  Return:
 *    None
 */
function api_incognito() {
  global $config, $response;
  $newstatus = 0;

  if ($config->status & STATUS_INCOGNITO) {
    $newstatus = $config->status - STATUS_INCOGNITO;
  }
  else {
    $newstatus = $config->status + STATUS_INCOGNITO;
  }

  $config->save_status($newstatus, $config->unpausetime);
  $response['status'] = $newstatus;
}


/********************************************************************
 *  API Load DNS
 *    Load DNS Log file
 *  Params:
 *    None
 *  Return:
 *    None
 */
function api_load_dns() {
  global $response;

  $line = '';
  $linenum = 1;


  if (! file_exists(DNS_LOG)) {
    http_response_code(410);                               //File Gone
    $response['error_code'] = 'file_not_found';
    $response['error_message'] = DNS_LOG.' not found';
    return;
  }

  $fh = fopen(DNS_LOG, 'r') or die('Error unable to open '.DNS_LOG);
  while (!feof($fh)) {
    $line = trim(fgets($fh));                              //Read and trim line of file

    $response[$linenum] = $line;
    $linenum++;
  }
  fclose($fh);                                             //Close file

}


/**
 * API Recent Queries
 *  Get recent DNS queries
 *  Optional value of interval (in minutes) can be specified
 *
 *  @param object MySqliDb class
 *
 */
function api_recent_queries($dbwrapper) {
  global $response;

  $interval = 4;             //Assume 4 mins (default log collecting interval)

  //Check that interval if specified is within a valid range of one hour
  if (isset($_GET['interval'])) {
    $interval = filter_integer($_GET['interval'], 1, 60, 4);
  }

  $response = $dbwrapper->recent_queries($interval);
}


/**
 * Do GET Action
 *  Review the specified action on GET parameter
 *  Create new sqli wrapper class
 *  Carry out the action specified by user
 *
 */
function do_get_action() {
  global $response;

  $access_rights = 0;

  $access_rights = validate_api_key();
  if ($access_rights == API_INVALID) {
    http_response_code(401);
    $response['error_code'] = 'invalid_request';
    $response['error_message'] = 'Your client ID is invalid';
    return;
  }

  $dbwrapper = new MySqliDb;

  $action = $_GET['action'] ?? '';

  if (($access_rights == API_READWRITE) || ($access_rights == API_READONLY)) {
    switch ($action) {
      case 'count_blocklisted_domains':
        $response = $dbwrapper->count_blocklisted_domains();
        break;
      case 'count_queries_today':
        $response = $dbwrapper->count_queries_today();
        break;
      case 'count_total_queries':
        $response['queries'] = $dbwrapper->count_total_queries();
      case 'get_status':
        $response['status'] = $dbwrapper->get_status();
        break;
      case 'recent_queries':
        api_recent_queries($dbwrapper);
        break;
      default:
        http_response_code(400);
        $response['error_code'] = 'missing_required_parameter';
        $response['error_message'] = 'Your request was missing an action parameter';
    }
  }
}


/**
 * Validate provided API key provided to GET request api_key
 *
 *  @return int access rights 0 - None, 1 - readonly, 2 - full
 *    None
 */
function validate_api_key() {
  global $config;

  $key = '';

  if ($config->api_key == '') return false;

  $key = $_GET['api_key'] ?? '';

  if (preg_match(REGEX_VALIDAPI, $key)) {
    if ($key == $config->api_key) {
      return API_READWRITE;
    }
    elseif ($key == $config->api_readonly) {
      return API_READONLY;
    }
  }

  return API_INVALID;
}
//Main---------------------------------------------------------------

/************************************************
*POST REQUESTS                                  *
************************************************/

if (isset($_POST['operation'])) {
  ensure_active_session();
  switch ($_POST['operation']) {
    case 'blocklist_status': api_blocklist_status(); break;
    case 'disable': api_enable_notrack(); break;
    case 'enable': api_enable_notrack(); break;
    case 'pause': api_pause_notrack(); break;
    case 'incognito': api_incognito(); break;
    default:
      http_response_code(400);
      $response['error_code'] = 'missing_required_parameter';
      $response['error_message'] = 'Your request was missing an operation parameter';
      break;
  }
}

elseif (isset($_POST['livedns'])) {
  ensure_active_session();
  api_load_dns();
}

elseif (sizeof($_GET) > 0) {
  do_get_action();
}

else {
  //Bad Request
  http_response_code(400);
  $response['error_code'] = 'missing_required_parameter';
  $response['error_message'] = 'Your request was missing an action parameter';
}
echo json_encode($response);
?>
