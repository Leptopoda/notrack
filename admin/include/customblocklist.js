const CSV_REGEX = /^"?([\w\-]{1,63}\.[\w\-\.]{2,251}|\.[\w\-]{1,63})"?,"?([^"]*)"?/;
const DEFANGED_REGEX = /^(?:f[tx]p|h[tx][tx]ps?):\/\/([\w\-]{1,63}\[?[d\.]\]?[\w\-\[\]\.]{2,251})/i;
const EASY_REGEX = /^\|\|([\w\-]{1,63}\.[\w\-\.]{2,251})(\^|\^\$third\-party|\^\$popup|\^\$popup,third\-party)$/;
const PLAIN_REGEX = /^([\w\-]{1,63}\.[\w\-\.]{2,251}|\.[\w\-]{1,63})\s*#?(.*)$/
const UNIX_REGEX = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+([\w\-]{1,63}\.[\w\-\.]{2,251})\s*#?(.*)$/;
const DNSMASQ_REGEX = /^address=\/([\w\-]{1,63}\.[\w\-\.]{2,251}|\.[\w\-]{1,63})\//;

/**
 * Match CSV Line
 *  Extracts matches against CSV_REGEX, looking for lines (domain),(optional comment)
 *
 *  @param string line from users input.
 *  @return array of matched domain and optional comment on successful match, or false.
 */
function matchCSVLine(line) {
  let matches = CSV_REGEX.exec(line)

  if (matches !== null) {
    if (matches[2] !== undefined) {                        //Comment may not be filled in
      return [matches[1], matches[2], true];
    }
    else {
      return [matches[1], '', true];
    }
  }
  return false;
}


/**
 * Match Defanged Line
 *  Extracts matches against DEFANGED_REGEX, replaces defanged dots with normal dot.
 *
 *  @param string line from users input.
 *  @return array of matched domain and optional comment on successful match, or false.
 */
function matchDefangedLine(line) {
  let matches = DEFANGED_REGEX.exec(line)
  let domain = '';

  if (matches !== null) {
    domain = matches[1].replace('[.]', '.');              //Un-defang domain name
    domain = domain.replace('[d]', '.');
    return [domain, '', true];
  }
  return false;
}


/**
 * Match Dnsmasq Line
 *  Extracts matches against DNSMASQ_REGEX, looking for lines address=/(domain)/
 *
 *  @param string line from users input.
 *  @return array of matched domain and optional comment on successful match, or false.
 */
function matchDnsmasqLine(line) {
  let matches = DNSMASQ_REGEX.exec(line)

  if (matches !== null) {
    return [matches[1], '', true];
  }
  return false;
}


/**
 * Match Easy Line
 *  Extracts matches against EASY_REGEX, looking for lines ||(domain)^
 *
 *  @param string line from users input.
 *  @return array of matched domain and optional comment on successful match, or false.
 */
function matchEasyLine(line) {
  let matches = EASY_REGEX.exec(line)

  if (matches !== null) {
    return [matches[1], '', true];
  }
  return false;
}


/**
 * Match Plain Line
 *  Extracts matches against PLAIN_REGEX, looking for lines (domain) #(optional comment)
 *
 *  @param string line from users input.
 *  @return array of matched domain and optional comment on successful match, or false.
 */
function matchPlainLine(line) {
  let matches = PLAIN_REGEX.exec(line)

  if (matches !== null) {
    if (matches[2] !== undefined) {                        //Comment may not be filled in
      return [matches[1], matches[2], true];
    }
    else {
      return [matches[1], '', true];
    }
  }
  return false;
}


/**
 * Match Unix Line
 *  Extracts matches against UNIX_REGEX, looking for lines 0.0.0.0 (domain) #(optional comment)
 *
 *  @param string line from users input.
 *  @return array of matched domain and optional comment on successful match, or false.
 */
function matchUnixLine(line) {
  let matches = UNIX_REGEX.exec(line);

  if (matches !== null) {
    if (matches[2] !== undefined) {                        //Comment may not be filled in
      return [matches[1], matches[2], true];
    }
    else {
      return [matches[1], '', true];
    }
  }
  return false;
}


/**
 * Evaluate Bulk Box
 *  Create an array of newline seperated lines from bulkBox.
 *  Evaluate each line against known regex's, then add domains to foundDomains array.
 *
 *  @return array of domains found
 */
function evaluateBulkBox() {
  let line = '';
  let foundDomains = [];

  const bulkBox = document.getElementById('bulkBox');
  const bulkLines = bulkBox.innerText.split('\n');
  const bulkLinesLength = bulkLines.length;

  for (let i = 0; i < bulkLinesLength; i++) {
    line = bulkLines[i].trim();
    if (/^#/.test(line)) continue; //Skip commented lines

    matches = matchUnixLine(line);
    if (matches !== false) {
      foundDomains.push(matches);
      continue;
    }

    matches = matchCSVLine(line);
    if (matches !== false) {
      foundDomains.push(matches);
      continue;
    }

    matches = matchDnsmasqLine(line);
    if (matches !== false) {
      foundDomains.push(matches);
      continue;
    }

    matches = matchPlainLine(line);
    if (matches !== false) {
      foundDomains.push(matches);
      continue;
    }

    matches = matchEasyLine(line);
    if (matches !== false) {
      foundDomains.push(matches);
      continue;
    }

    matches = matchDefangedLine(line);
    if (matches !== false) {
      foundDomains.push(matches);
      continue;
    }

  }

  return foundDomains;
}


/**
 * Submit Bulk Box
 *  Firstly identify domains from bulkBox with evaluateBulkBox
 *  Add list of items to newdomains as a JSON encoded string
 *  Submit blocklistform
 *
 */
function submitBulkBox() {
  let domainList = evaluateBulkBox();

  document.getElementById('newdomains').value = JSON.stringify(domainList);
  document.getElementById('blocklistform').submit();
}


/**
 * Populate Custom Block List Table
 *  Populates contenteditable table with all the domains parsed via DATA
 *
 */
function populateCustomBlTable() {
  let checkBox = '';
  let dataLength = 0;
  let i = 0;

  const tb = document.getElementById('custombl-table');

  dataLength = DATA.length;

  for(i = 0; i < dataLength; i++) {
    let row = tb.insertRow();
    let cell0 = row.insertCell(0);
    let cell1 = row.insertCell(1);
    let cell2 = row.insertCell(2);
    let cell3 = row.insertCell(3);

    if (DATA[i][2] === true) {
      checkBox = `<input type="checkbox" name="${DATA[i][0]}" checked="checked">`;
    }
    else {
      checkBox = `<input type="checkbox" name="${DATA[i][0]}">`;
    }

    cell0.innerHTML = checkBox;
    cell1.innerHTML = `<div contenteditable="true" placeholder="site.com">${DATA[i][0]}</div>`;
    cell2.innerHTML = `<div contenteditable="true" placeholder="Optional Comment">${DATA[i][1]}</div>`;
    cell3.innerHTML = `<button class="button-grey material-icon-centre icon-delete" type="button" onclick="deleteRow(this)">&nbsp;</button>`;
  }

  finaliseCustomBlTable(i);
}

/**
 * Finalise Custom Block List Table
 *  Adds blank row on contenteditable table and submit button
 *
 */
function finaliseCustomBlTable() {
  const tb = document.getElementById('custombl-table');

  let row = tb.insertRow();
  let cell0 = row.insertCell(0);
  let cell1 = row.insertCell(1);
  let cell2 = row.insertCell(2);
  let cell3 = row.insertCell(3);

  cell0.innerHTML = '<input type="checkbox" checked="checked">';
  cell1.innerHTML = '<div contenteditable="true" placeholder="site.com"></div>';
  cell2.innerHTML = '<div contenteditable="true" placeholder="Optional Comment"></div>';
  cell3.innerHTML = '<button class="button-grey material-icon-centre icon-plus" type="button" onclick="addRow(this)">&nbsp;</button>';

  row = tb.insertRow();
  cell0 = row.insertCell(0);
  cell0.colSpan = 4;
  cell0.innerHTML = '<button class="material-icon-button-text icon-tick" type="button" onclick=submitTable()>Save Changes</button>';
}


/**
 * Add new row to contenteditable table
 *  1. Make the Add button a Del button.
 *  2. Insert a new Table row 1 up from the end (above Save Changes button).
 *  3. Create Cells in new row.
 *  4. Create a new Add button in the new row.
 *
 *  @param object the button which was clicked
 */
function addRow(btn) {
  const tb = document.getElementById('custombl-table');

  //Make Add button a Delete button
  btn.className = 'button-grey material-icon-centre icon-delete';
  btn.setAttribute('onClick', 'deleteRow(this)');

  //Insert new row 1 up from end of the table
  let newRow = tb.insertRow(tb.rows.length -1);

  //Insert cells
  let cell0 = newRow.insertCell(0);
  let cell1 = newRow.insertCell(1);
  let cell2 = newRow.insertCell(2);
  let cell3 = newRow.insertCell(3);

  //Set contents of the cells
  cell0.innerHTML = '<input type="checkbox" checked="checked">';
  cell1.innerHTML = '<div contenteditable="true" placeholder="site.com"></div>';
  cell2.innerHTML = '<div contenteditable="true" placeholder="Optional Comment"></div>';
  cell3.innerHTML = '<button class="button-grey material-icon-centre icon-plus" type="button" onclick="addRow(this)">&nbsp;</button>';
}


/**
 * Delete Row from contenteditable table
 *  Get the table row for the button which was pressed
 *  Get the rows parent to delete it
 *
 *  @param object the button which was clicked
 */
function deleteRow(btn) {
  const row = btn.parentNode.parentNode;
  row.parentNode.removeChild(row);
}


/**
 * Export Table to users downloads
 *  Used for all client-side production of tables.
 *  1. Set table headers based on the tableName.
 *  2. Build csvData from each row of DATA, adding in newline character.
 *  3. Create a temporary link with csvData as a blob.
 *
 *  @param str Unique table name to set table headers and download name.
 */
function exportTable(tableName) {
  let fileName = '';
  let csvData = [];
  let domainComment = '';
  let domainName = '';

  const tb = document.getElementById('custombl-table');
  const tbLength = tb.rows.length - 1;

  //Set fileName based on value of tableName parameter.
  switch(tableName) {
    case 'allow':
      fileName = 'custom_allow_list.csv';
      break;
    case 'block':
      fileName = 'custom_block_list.csv';
      break;
    default:
      console.error(`Unknown table - ${tableName}`);
      return;
  }

  csvData += 'domain,comment\n';

  for (let i = 1; i < tbLength; i++) {
    //Only add rows with an enabled checkbox
    if (tb.rows[i].cells[0].children[0].checked === false) {
      continue;
    }

    domainName = tb.rows[i].cells[1].innerText;
    domainComment = tb.rows[i].cells[2].innerText;

    //Ignore the new row at the end of the table, which has a blank domain name.
    if (domainName.length == 0) {
      continue;
    }

    csvData += `${domainName},${domainComment}\n`;
  }

  //Create a temporary link element containing csvData
  csvFile = new Blob([csvData], {type: 'text/csv'});
  downloadLink = document.createElement("a");
  downloadLink.download = fileName;
  downloadLink.href = window.URL.createObjectURL(csvFile);
  downloadLink.style.display = "none";

  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);
}


/**
 * Submit Form
 *  Collect contenteditable data then add it as a JSON encoded value to newdomains.
 *  Submit the blocklistform back to PHP server for input validation
 *
 */
function submitTable() {
  let domainList = [];

  const tb = document.getElementById('custombl-table');
  const tbLength = tb.rows.length - 1;                    //Ignore save changes button row

  for (let i = 1; i < tbLength; i++) {
    domainEnabled = tb.rows[i].cells[0].children[0].checked;
    domainName = tb.rows[i].cells[1].innerText;
    domainComment = tb.rows[i].cells[2].innerText;
    domainList.push([domainName, domainComment, domainEnabled]);
  }

  document.getElementById('newdomains').value = JSON.stringify(domainList);
  document.getElementById('blocklistform').submit();
}
