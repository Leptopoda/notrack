/**
 * Change Status
 *  Update stop-button and nav-blocking based on new status value
 *  TODO Home-Nav box to update
 *
 *  @param int output from API $config->status
 *
 */
function changeStatus(newStatus) {
  const STATUS_ENABLED = 1;
  const STATUS_DISABLED = 2;
  const STATUS_PAUSED = 4;
  const stopButton = document.getElementById('stop-button');
  const navBlocking = document.getElementById('nav-blocking');

  if (newStatus & STATUS_ENABLED) {
    stopButton.className = 'icon-stop';
    stopButton.title = 'Disable Blocking';
    navBlocking.className = 'status_green';
    navBlocking.innerHTML = '<figcaption>Blocking: Enabled</figcaption>';
  }
  else if (newStatus & STATUS_DISABLED) {
    stopButton.className = 'icon-play';
    stopButton.title = 'Enable Blocking';
    navBlocking.className = 'status_red';
    navBlocking.innerHTML = '<figcaption>Blocking: Disabled</figcaption>';
  }
  else if (newStatus & STATUS_PAUSED) {
    stopButton.classList.remove('icon-stop');
    stopButton.className = 'icon-play';
    stopButton.title = 'Enable Blocking';
  }
}


/**
 * Menu incognito
 *  POST Incognito operation to API
 *  API returns new status value
 *  Set incognito button to red colour
 *
 */
function menuIncognito() {
  const STATUS_INCOGNITO = 8;
  const incognitoButton = document.getElementById("incognito-button");
  const oReq = new XMLHttpRequest();
  const url = "/admin/include/api.php";
  const params = "operation=incognito";

  oReq.open("POST", url, true);
  oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  oReq.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //console.log(this.responseText);
      let apiResponse = JSON.parse(this.responseText);
      if (apiResponse['status'] & STATUS_INCOGNITO) {
        incognitoButton.className = 'icon-incognito-off';
      }
      else {
        incognitoButton.className = 'icon-incognito';

      }
    }
  }
  oReq.send(params);
}


/**
 * Enable NoTrack
 *  POST enable operation to API
 *  API returns new status value
 *  @note This is the same function for Enable and Disable, let the API work out what to do
 *
 */
function enableNoTrack() {
  const oReq = new XMLHttpRequest();
  const url = "/admin/include/api.php";
  const params = "operation=enable";

  oReq.open("POST", url, true);
  oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  oReq.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //console.log(this.responseText);
      let apiResponse = JSON.parse(this.responseText);
      changeStatus(apiResponse['status']);
    }
  }
  oReq.send(params);
}


/**
 * Pause NoTrack
 *  POST enable operation to API
 *  API returns new status value with unpausetime
 *  Update navBlocking with unpausetime
 *
 *  @param int minutes to pause for
 *
 */
function pauseNoTrack(mins) {
  const oReq = new XMLHttpRequest();
  const url = "/admin/include/api.php";
  const params = "operation=pause&mins="+mins;
  const navBlocking = document.getElementById('nav-blocking')

  oReq.open("POST", url, true);
  oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  oReq.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      let apiResponse = JSON.parse(this.responseText);
      changeStatus(apiResponse['status']);
      navBlocking.className = 'status_yellow';
      navBlocking.innerHTML = `<figcaption>Blocking: Paused - ${apiResponse['unpausetime']}</figcaption>`;
    }
  }
  oReq.send(params);

  document.getElementById('pause-menu').parentElement.blur();
}


/**
 * Open Navigation Panel
 *  Check width of nav element
 *  Expand to 14rem if zero
 *  Reduce to zero for any other size
 */

function openNav() {
  const navMenu = document.getElementsByTagName('NAV')[0];
  if ((navMenu.style.width == '0rem') || (navMenu.style.width == '')) {
    navMenu.style.width = '14rem';
  }
  else {
    navMenu.style.width = '0rem';
  }
}


/**
 *  Set Clipboard Data
 *    Copy supplied text to clipboard using a one-off event listener.
 *    Show popup copymsg for 3 seconds.
 *
 *  @param str Text to copy to clipboard
 */
function setClipboard(copyText) {
  document.addEventListener('copy', function() {
    event.clipboardData.setData('text/plain', copyText);
    event.preventDefault();
  }, {'once': true});
  document.execCommand('copy');

  //Show copymsg element
  document.getElementById('copymsg').innerText = `${copyText} copied to clipboard`;
  document.getElementById('copymsg').style.display = 'block';

  //Delay for 3 seconds, then Hide  copymsg element
  setTimeout(function(){
    document.getElementById('copymsg').style.display = 'none';
  }, 3000);
}

