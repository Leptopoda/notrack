#!/usr/bin/env python3
#Title       : NoTrack Analytics
#Description : Analyse dns_logs table for queries to malicious or unknown tracking domains
#Author      : QuidsUp
#Date        : 2020-06-20
#Version     : 22.09
#Usage       : python3 ntrk-analytics.py

#Standard Imports
import re

#Local imports
import share.errorlogger
from ntrkmariadb import DBWrapper

#Create logger
logger = share.errorlogger.createlogger(__name__)

class NoTrackAnalytics():
    _domainsfound = set()                        #Prevent duplicate domains
    _allowlist = list()                          #Users Allow List
    _blocklists = list()                         #Active Block Lists

    #Additional regex pattern of domains to ignore
    _ignorelist = {
      r'akadns\.net$',
      r'amazonaws\.com$',
      r'edgekey\.net$',
      r'elasticbeanstalk\.com$',
      r'streetviewpixels\-pa\.googleapis\.com$', #Google Streetview Tiles
      r'\w{3}pixel\.[\w-]{1,63}$',               #Pixel part of domain, e.g. retropixel
    }


    def __init__(self):
        self._dbwrapper = DBWrapper()                     #Declare MariaDB Wrapper
        self._dbwrapper.analytics_createtable()           #Check analytics table exists

        #Load the allow and block lists from blocklist table
        self._allowlist = self._dbwrapper.blocklist_getwhitelist()
        self._blocklists = self._dbwrapper.blocklist_getactive()


    def __del__(self):
        self._domainsfound.clear()
        self._allowlist.clear()
        self._blocklists.clear()


    def __searchmalware(self, bl: str) -> None:
        """
        Search for results from a specific malware blocklist
        Parse found results to __review_results

        Parameters:
            Blocklist name
        """
        tabledata = list()                                #Results of MariaDB search

        tabledata = self._dbwrapper.dnslog_searchmalware(bl)

        if len(tabledata) == 0:                           #Leave if nothing found
            return

        self.__review_results(bl, tabledata)              #Specify name of malware list


    def __searchregex(self, pattern: str, listtype: str) -> None:
        """
        Search for specified results pattern
        Parse found results to __review_results

        Parameters:
            pattern (str): Regex pattern to search
            listtype (str): advert, tracker
        """
        tabledata = list()                                #Results of MariaDB search

        tabledata = self._dbwrapper.dnslog_searchregex(pattern)

        if len(tabledata) == 0:                           #Leave if nothing found
            return

        self.__review_results(listtype, tabledata)


    def __is_ignorelist(self, domain: str) -> bool:
        """
        Check if domain is in ignore list
        Some domains should be ignored as they're secondary DNS lookups or CDN's

        Parameters:
            Domain to check
        Returns:
            True: Domain is in ignorelist
            False: Domain is not in ignorelist
        """
        pattern: str                                      #Regex pattern from ignore list

        for pattern in self._ignorelist:
            if re.search(pattern, domain) is not None:
                logger.debug(f'{domain} in ignorelist')
                return True

        return False


    def __is_allowlist(self, domain: str) -> bool:
        """
        Check if domain is in allow list.
        Treat allow list results as a regex pattern in order to check for 'domain'
        being a subdomain to an item in the allow list.

        Parameters:
            Domain to check
        Returns:
            True: Domain is in allow list
            False: Domain is not in allow list
        """
        if len(self._allowlist) == 0:                      #Don't check against empty list
            return False

        pattern: str                                       #Regex pattern from allow list

        for pattern in self._allowlist:
            #check for a match from the end backwards to allow for subdomain match
            if re.search(pattern + '$', domain) is not None:
                logger.debug(f'{domain} matched pattern {pattern} in whitelist')
                return True

        return False


    def __is_domainadded(self, domain: str) -> bool:
        """
        Check if domain has already been added this run

        Parameters:
            Domain to check
        Returns:
            True: Domain has been added
            False: Domain has not been added
        """
        if domain in self._domainsfound:
            logger.debug(f'{domain} has already been added')
            return True

        return False


    def __review_results(self, issue: str, tabledata: list) -> None:
        """
        Check the results found before adding to the analytics table

        Parameters:
            issue (str): Tracker, or Malware-x
            tabledata (list): list of data found from the MariaDB search
        """
        analytics_severity: str
        new_severity: str                                 #New new_severity for dnslog
        new_bl_source: str
        rowid: int
        datetime: object
        sysip: str
        dns_request: str
        severity: str
        bl_source: str

        for row in tabledata:
            rowid = row[0]
            datetime = row[1]
            sysip = row[2]
            dns_request = row[3]
            severity = row[4]
            bl_source = row[5]

            new_severity = '3'
            new_bl_source = issue

            if self.__is_ignorelist(dns_request):
                continue

            if self.__is_allowlist(dns_request):
                continue

            #Set the new DNS result for advert or tracker accessed
            if issue == 'advert' or issue == 'tracker':
                analytics_severity = '1'

            #Malware been allowed or blocked, requires old severity to be checked
            else:
                if severity == '2':                        #Malware blocked
                    analytics_severity = '2'
                    new_bl_source = bl_source              #Don't change bl_source
                    new_severity = '2'
                else:                                      #Malware accessed
                    analytics_severity = '3'

            #Update dnslog record to show malware / tracker accessed instead of blocked / allowed
            self._dbwrapper.dnslog_updaterecord(rowid, new_severity, new_bl_source)

            #Make sure that domain has not been added to analytics recently?
            if not self.__is_domainadded(dns_request):
                self._domainsfound.add(dns_request)         #Add domain to domainsfound dict
                self._dbwrapper.analytics_insertrecord(datetime, sysip, dns_request, analytics_severity, issue)

            logger.debug(f'{issue} - {rowid}, {datetime.isoformat(sep=" ")}, {dns_request}')


    def checkmalware(self) -> None:
        """
        Check if any domains from all the enabled malware blocklists have been accessed
        """
        if 'bl_notrack_malware' in self._blocklists:
            self.__searchmalware('bl_notrack_malware')
        if 'bl_hexxium' in self._blocklists:
            self.__searchmalware('bl_hexxium')


    def checktrackers(self) -> None:
        """
        Check if any accessed domains match known tracker or advertising patterns
        Checks for Pixels, Telemetry, and Trackers
        """
        self.__searchregex('^analytics\\\.', 'tracker')                  #analytics as a subdomain
        self.__searchregex('^beacons?\\\.', 'tracker')                   #beacon(s) as a subdomain
        self.__searchregex('^cl(ck?|kstat)\\\.', 'tracker')       #clc, clck, clicks?, clkstat as a subdomain
        #22.02 Update - remove |ck|icks?
        self.__searchregex('^counter\\\.', 'tracker')                    #counter as a subdomain
        self.__searchregex('^eloq(ua)?(\-trackings?)?\\\.', 'tracker')   #Oracle eloq, eloqua, eloqua-tracking
        self.__searchregex('^log(s|ger)?\\\.', 'tracker')                #log, logs, logger as a subdomain (exclude login.)
        self.__searchregex('^pxl?\\\.', 'tracker')                       #px, pxl, as a subdomain
        self.__searchregex('pixel[^\\\.]{0,8}\\\.', 'tracker')           #pixel, followed by 0 to 8 non-dot chars anywhere
        self.__searchregex('^(aa\-|app|s)?metri[ck][as]\\\.', 'tracker') #aa-metrics, appmetrics, smetrics, metrics, metrika as a subdomain
        self.__searchregex('telemetry', 'tracker')                       #telemetry anywhere
        self.__searchregex('trk[^\\\.]{0,3}\\\.', 'tracker')             #trk, followed by 0 to 3 non-dot chars anywhere
        #Have to exclude tracker. (bittorent), security-tracker (Debian), and tracking-protection (Mozilla)
        self.__searchregex('^trace\\\.', 'tracker')                      #trace as a subdomain
        self.__searchregex('track(ing|\\\-[a-z]{2,8})?\\\.', 'tracker')  #track, tracking, track-eu as a subdomain / domain.
        self.__searchregex('^visit\\\.', 'tracker')                      #visit as a subdomain
        self.__searchregex('^v?stats?\\\.', 'tracker')                   #vstat, stat, stats as a subdomain

        #Checks for Advertising
        self.__searchregex('^ad[sv]\\\.', 'advert')                      #ads or adv
        self.__searchregex('^adserv(er|ice)', 'advert')                  #adserver or adservice
        self.__searchregex('^advert', 'advert')
        self.__searchregex('^(pre)?bid(der)?\\\.', 'advert')             #prebid, bid, bidder


def main():
    print('NoTrack DNS Log Analytics')
    ntrkanalytics = NoTrackAnalytics()

    ntrkanalytics.checkmalware()
    ntrkanalytics.checktrackers()

    print('NoTrack log analytics complete :-)')
    print()

if __name__ == "__main__":
    main()
