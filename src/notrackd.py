#!/usr/bin/env python3
#Title       : NoTrackD
#Description : NoTrack Daemon. Controls root actions for NoTrack and monitors config files being updated by PHP frontend
#Author      : QuidsUp
#Date        : 2020-07-24
#Version     : 22.02
#Usage       : sudo python3 notrackd

"""
* Utilises an event queuing based on epoch time to control when Log Parser, Analytics, Block List Update, and Software Updates are carried out.
* Events are stored in the event list by latest epoch time first.
* Every five seconds the current epoch time is compared with the first event timestamp in event list.
* If the timestamp has been exceeded, then that event action is carried out and the queue moved forward.
* A new event is created in the queue before action is carried out.
"""

#Standard imports
import time
import signal

#Local imports
import folders
import logparser
import share
from analytics import NoTrackAnalytics
from blockparser import BlockParser
from config import NoTrackConfig
from ntrkservices import Services
from ntrkupgrade import NoTrackUpgrade
from statusconsts import *

endloop: bool = False
events = list()

#Create logger
logger = share.createlogger('NoTrackD')

dnslogparser = logparser.NoTrackParser()
config = NoTrackConfig()


def run_analytics() -> None:
    """
    Run NoTrack Analytics
    """
    ntrkanalytics = NoTrackAnalytics()
    ntrkanalytics.checkmalware()
    ntrkanalytics.checktrackers()


def run_trim() -> None:
    """
    Trim DNS Logs
    """
    logretention: int

    logretention = int(config.get('server', 'dns_logretention', default=30))
    dnslogparser.trimlogs(logretention)


def blocklist_update() -> None:
    """
    Once a day update of the NoTrack blocklists
    Actions:
        1. Update Block List
        2. Wait until MariaDB completes the blocked domain update
        3. Get the dnslogparser to reload the block list
    """
    blockparser = BlockParser()
    blockparser.create_blocklist()
    time.sleep(6)
    del blockparser
    dnslogparser.readblocklist()


def check_upgrade() -> None:
    """
    Once a day check for updates
    """
    ntrkupgrade = NoTrackUpgrade()
    ntrkupgrade.is_upgrade_available()
    #TODO auto upgrade feature


def change_status() -> None:
    """
    Change status of NoTrack
    """
    blockparser = BlockParser()

    if config.status & STATUS_ENABLED:
        blockparser.enable_blockling()
    else:
        blockparser.disable_blocking()

    del blockparser


def check_pause(current_time: float) -> None:
    """
    Check if pause time has been reached
    Function should only be called if status is STATUS_PAUSED

    Parameters:
        Current epoch timestamp
    """
    if config.unpausetime < current_time:
        blockparser = BlockParser()
        blockparser.enable_blockling()
        config.status -= STATUS_PAUSED
        config.status += STATUS_ENABLED
        config.unpausetime = 0
        del blockparser


def get_status(currentstatus: int) -> int:
    """
    Confirm system status is as currentstatus specifies
    e.g. main or temp blocklist could be missing

    Parameters:
        The current belived status
    Returns:
        Actual Status
    """
    mainexists = share.fileutils.file_exists(folders.main_blocklist)
    tempexists = share.fileutils.file_exists(folders.temp_blocklist)

    if currentstatus & STATUS_ENABLED and mainexists:      #Check Enabled Status
        return currentstatus
    elif currentstatus & STATUS_ENABLED:
        return STATUS_ERROR

    if currentstatus & STATUS_DISABLED and tempexists:     #Check Disabled Status
        return currentstatus
    elif currentstatus & STATUS_DISABLED:
        return STATUS_ERROR

    if currentstatus & STATUS_PAUSED and tempexists:       #Check Paused Status
        return currentstatus
    elif currentstatus & STATUS_PAUSED:
        return STATUS_ERROR

    return STATUS_ERROR                                    #Shouldn't get here


def exit_gracefully(signum: int, frame: int) -> None:
    """
    End the main loop when SIGINT, SIGABRT or SIGTERM is received

    Parameters:
        Signal
        Frame
    """
    global endloop
    endloop = True                                         #Trigger breakout of main loop


def parse_dns_logs():
    """
    Run NoTrack Log Parser
    """
    if config.status & STATUS_INCOGNITO:                   #No parsing with incognito
        dnslogparser.blank_dnslog()
    else:
        dnslogparser.parsedns()


def check_config_files():
    """
    Check config to see if any files have been modified
    Action any changes depending on which config file has been updated
    If statements are ordered in the most likely config to be updated
    """
    filechanged = ''

    filechanged = config.check_modified_times()            #Check for the first file modified

    if filechanged != '':
        logger.info(f'Config file {filechanged} has been updated')

    if filechanged == 'status.php':                        #Status Config
        if get_status(config.status) != config.status:
            change_status()

    elif filechanged == 'bl.php':                          #Blocklist Config
        blocklist_update()

    #One of the domain list files
    elif filechanged == 'blacklist.txt' or filechanged == 'whitelist.txt' or filechanged == 'tldlist.txt':
        blocklist_update()

    elif filechanged == 'server.php':                      #Server Config
        restart_dns()


def restart_dns():
    """
    Restart the DNS server
    """
    services = Services()
    services.restart_dnsserver()


def action_event(event: dict):
    """
    Action an event, which is the first item in events list which is popped out and
    parsed to this function.
    Actions:
        1. Check the event name
        2. Create next event at the appropriate time difference
        3. Carry out the action
    """
    if event['event'] == 'parser':
        insert_event('parser', time.time() + 240)         #+4 Mins
        parse_dns_logs()

    elif event['event'] == 'analytics':
        insert_event('analytics', time.time() + 3600)     #+1 Hour
        run_analytics()

    elif event['event'] == 'blocklist':
        insert_event('blocklist', time.time() + 86400)    #+1 Day
        blocklist_update()
        check_upgrade()

    elif event['event'] == 'trim':
        insert_event('trim', time.time() + 86400)         #+1 Day
        run_trim()


def insert_event(eventname: str, timestamp: float) -> None:
    """
    Add an event to events list making sure to keep the items in soonest
    (lowest epoch value) first
    """
    global events
    i: int
    totalevents: int

    event = {
        'event': eventname,
        'timestamp': timestamp,
    }

    totalevents = len(events)
    if totalevents == 0:
        events.append(event)
        return

    for i in range (0, totalevents):
        if events[i]['timestamp'] >= timestamp:
            events.insert(i, event)
            return

    events.append(event)


def main():
    global events, endloop
    current_time: float

    share.userutils.check_root()

    #Create Signals to kill NoTrackD via Ctrl+C
    signal.signal(signal.SIGINT, exit_gracefully)         #2 Inturrupt
    signal.signal(signal.SIGABRT, exit_gracefully)        #6 Abort
    signal.signal(signal.SIGTERM, exit_gracefully)        #9 Terminate

    #Create events
    current_time = time.time()
    insert_event('blocklist', current_time + 150)         #+02:30
    insert_event('parser', current_time + 360)            #+06:00
    insert_event('trim', current_time + 2220)             #+37:00
    insert_event('analytics', current_time + 3440)        #+57:20

    if get_status(config.status) != config.status:
        change_status()
        time.sleep(4)

    dnslogparser.readblocklist()

    while not endloop:
        current_time = time.time()

        check_config_files()

        if config.status & STATUS_PAUSED:
            check_pause(current_time)

        if current_time >= events[0]['timestamp']:
            action_event(events.pop(0))

        time.sleep(4)


if __name__ == "__main__":
    main()
