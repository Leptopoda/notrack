#!/usr/bin/env python3
#Title       : NoTrack Config
#Description : Loads config settings from front-end PHP and can create config for back-end components
#Author      : QuidsUp
#Date        : 2020-07-30
#Version     : 22.02
#Usage       : python3 config.py

#Standard Imports
import os
import time

#Local imports
import share.fileutils
import share.errorlogger
import folders
import regex
from statusconsts import *

#Create logger
logger = share.errorlogger.createlogger('NoTrackConf')


FILTER_DOMAIN = r'[\w\-]{1,63}\.[\w\-\.]{2,254}'

FILTER_MAC = r'^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$'



FILTER_INT = r'^\d+$'

strtobool = {
    'true': True,
    'false': False,
}

class NoTrackConfig:
    _conf = {
        'blocklist': {
            'status': {},
            'custom': '',
        },
        'server': {
            'dhcp_authoritative': {
                'value': 0,
                'vartype': 'bool',
            },
            'dhcp_enabled': {
                'value': 0,
                'vartype': 'bool',
            },
            'dhcp_leasetime': {
                'value': '24h',
                'vartype': 'duration',
            },
            'dhcp_gateway': {
                'value': '192.168.0.0',
                'vartype': 'ip',
            },
            'dhcp_rangestart': {
                'value': '192.168.0.64',
                'vartype': 'ip',
            },
            'dhcp_rangeend': {
                'value': '192.168.0.254',
                'vartype': 'ip',
            },
            'dns_blockip': {
                'value': '127.0.0.1',
                'vartype': 'ip',
            },
            'dns_interface': {
                'value': 'eth0',
                'vartype': 'str',
            },
            'dns_listenip': {
                'value': '127.0.0.1',
                'vartype': 'ip',
            },
            'dns_listenport': {
                'range': range(1, 65536),
                'value': 53,
                'vartype': 'int',
            },
            'dns_logretention': {
                'range': range(0, 366),
                'value': 60,
                'vartype': 'int',
            },
            'dns_name': {
                'value': 'notrack.local',
                'vartype': 'str',
            },
            'dns_server': {
                'value': 'OpenDNS',
                'vartype': 'str',
            },
            'dns_serverip1': {
                'value': '208.67.222.222',
                'vartype': 'ip',
            },
            'dns_serverip2': {
                'value': '208.67.220.220',
                'vartype': 'ip',
            },
            'dhcp_hosts': [],
        },
        'status': {
            'status': STATUS_ENABLED,
            'unpausetime': 0,
        }
    }
    def __init__(self):
        """
        Get the modified times for the various config files
        Load server config files
        """
        self._file_names = {                            #Actual config filenames
            'bl.php': f'{folders.webconfigdir}/bl.php',
            'server.php': f'{folders.webconfigdir}/server.php',
            'status.php': f'{folders.webconfigdir}/status.php',
            'blacklist.txt': f'{folders.webconfigdir}/blacklist.txt',
            'whitelist.txt': f'{folders.webconfigdir}/whitelist.txt',
            'tldlist.txt': f'{folders.webconfigdir}/tldlist.txt',
        }

        self._modified_times = {                           #Config Last modified times
            'bl.php': share.fileutils.get_modified_time(self._file_names['bl.php']),
            'server.php': share.fileutils.get_modified_time(self._file_names['server.php']),
            'status.php': share.fileutils.get_modified_time(self._file_names['status.php']),
            'blacklist.txt': share.fileutils.get_modified_time(self._file_names['blacklist.txt']),
            'whitelist.txt': share.fileutils.get_modified_time(self._file_names['whitelist.txt']),
            'tldlist.txt': share.fileutils.get_modified_time(self._file_names['tldlist.txt']),
        }

        self._conf['status']['status'] = STATUS_ENABLED                     #Default value for status
        self._conf['status']['unpausetime'] = 0                             #Default value for unpausetime

        self.load_status()
        self.load_server_config()


    def __del__(self):
        self._conf.clear()
        self._modified_times.clear()

    """@property
    def dns_blockip(self):
        return self.__config['dns_blockip']

    @property
    def dns_logretention(self):
        return int(self.__config['dns_logretention'])
    """
    @property
    def status(self):
        return self._conf['status']['status']

    @status.setter
    def status(self, value):
        self._conf['status']['status'] = value

    @property
    def unpausetime(self):
        return self._conf['status']['unpausetime']

    @unpausetime.setter
    def unpausetime(self, value):
        self._conf['status']['unpausetime'] = value


    def __dhcp_addhost(self, line):
        matches = regex.AddHost_Line.match(line)
        if matches is None:
            logger.warning(f'Invalid dhcp_addhost line {line}')
            return

        self._conf['server']['dhcp_hosts'].append({
            'ip': matches[1],
            'mac': matches[2],
            'sysname': matches[3],
            'sysicon': matches[4],
        })

    def __set_value(self, section: str, varname: str, value: str):
        """
        Check that the specified section and variable name exist in the default config
        Then go on to __validate_value
        """
        if section not in self._conf:
            logger.warning(f'Invalid section specified {section}')
            return

        if varname in self._conf[section]:
            if self.__validate_value(section, varname, value):
                self._conf[section][varname]['value'] = value


    def __validate_value(self, section: str, varname: str, value: str):
        """
        Carry out input validation on a value
        """
        vartype = ''
        vartype = self._conf[section][varname]['vartype']

        if vartype == 'bool':
            if regex.Filter_Bool.match(value) is not None:
                return True

        elif vartype == 'duration':
            if regex.Filter_Duration.match(value) is not None:
                return True

        elif vartype == 'ip':
            if regex.Filter_IP.match(value) is not None:
                return True
            else:
                logger.warning(f'Invalid IP Address {value} specified for {varname}')
                return False

        elif vartype == 'int':
            if value.isdigit():
                if int(value) in self._conf[section][varname]['range']:
                      return True
                else:
                    logger.warning(f'{value} outside of valid range for {varname}')
                    return False
            else:
                logger.warning(f'{value} should be an integer for {varname}')
                return False

        elif vartype == 'str':
            if regex.Filter_Str.match(value) is not None:
                return True

        logger.warning(f'Invalid value {value} specified for {varname}')

        return False


    def check_modified_times(self) -> str:
        """
        Check the last modified times for all config files

        Returns:
            First filename encountered which has been modified
        """
        new_mtime = 0

        for conffile, mtime in self._modified_times.items():
            new_mtime = share.fileutils.get_modified_time(self._file_names[conffile])
            if new_mtime != mtime:
                if conffile == 'status.php':
                    self.load_status()
                elif conffile == 'server.php':
                    self.load_server_config()
                    self.save_dhcpconf()
                    self.save_localhosts()
                    self.save_serverconf()
                self._modified_times[conffile] = new_mtime;

                return conffile

        return ''


    def get(self, section: str, key: str, **kwargs):
        """
        Optional Parameter:
            default - Default value to use if request is missing from config
        """
        if section == 'blocklist':
            if key == 'custom':
                return self._conf['blocklist']['custom']
            elif key in self._conf['blocklist']['status']:
                return self._conf['blocklist']['status'][key]

        elif section == 'server':
            if key in self._conf[section]:
                return self._conf[section][key]['value']

        if 'default' in kwargs:
            return kwargs['default']

        return None


    def load_blocklists(self):
        """
        Load the contents of bl.php into blocklist section of config
        """
        filelines = list()

        value = False
        self._conf['blocklist']['status'].clear()

        filelines = share.fileutils.load_file(self._file_names['bl.php'])

        for line in filelines:
            matches = regex.BlockListStatus.match(line)
            if matches is not None:
                self._conf['blocklist']['status'][matches[1]] = strtobool[matches[2]]
                continue

            matches = regex.BlockListCustom.match(line)
            if matches is not None:
                self._conf['blocklist']['custom'] = matches[1]


    def load_server_config(self):
        """
        Load server.php and read all the values from it
        """
        filelines = []
        self._conf['server']['dhcp_hosts'].clear()

        filelines = share.fileutils.load_file(self._file_names.get('server.php'))

        for line in filelines:
            matches = regex.PHPLine.match(line)
            if matches is not None:
                if matches[1] == 'dhcp_addhost':
                    self.__dhcp_addhost(matches[2])
                else:
                    self.__set_value('server', matches[1], matches[2])


    def load_status(self):
        """
        Load status.php to get status and unpausetime
        """
        filelines = []

        filelines = share.fileutils.load_file(self._file_names.get('status.php'))
        for line in filelines:
            matches = regex.StatusLine.match(line)
            if matches is not None:
                self._conf['status']['status'] = int(matches[1])
                self._conf['status']['unpausetime'] = int(matches[2])
                #print(f'Status: {self._conf['status']['status']}, Unpausetime: {self._conf['status']['unpausetime']}')
                break

        #Make sure status should be paused by checking if unpausetime < current time
        if self._conf['status']['status'] & STATUS_PAUSED and self._conf['status']['unpausetime'] > 0:
            if self._conf['status']['unpausetime'] < time.time():
                logger.info('Unpause time exceeded, setting as unpaused')
                self._conf['status']['status'] -= STATUS_PAUSED
                self._conf['status']['unpausetime'] = 0


    def save_dhcpconf(self):
        """
        Create dhcp.conf, which is read by dnsmasq
        Consists of DHCP Status, DHCP Range, and Static hosts
        """
        filelines = list()

        #If dhcp is disabled, then delete dhcp.conf file
        if self._conf['server']['dhcp_enabled'] == '0':
            #delete(folders.dhcpconf)
            return

        filelines.append(f"dhcp-option=3,{self._conf['server']['dhcp_gateway']['value']}\n")
        filelines.append(f"dhcp-range={self._conf['server']['dhcp_rangestart']['value']},{self._conf['server']['dhcp_rangeend']['value']},{self._conf['server']['dhcp_leasetime']['value']}\n")

        #Only add dhcp_authoritative if its enabled
        if self._conf['server']['dhcp_authoritative']['value'] == '1':
            filelines.append("dhcp-authoritative\n")

        filelines.append("\n")                            #Seperator

        for host in self._conf['server']['dhcp_hosts']:   #Add all localhosts
            #Commented Name, Type, followed by uncommented MAC, IP
            filelines.append(f"#{host['sysname']},{host['sysicon']}\n")
            filelines.append(f"dhcp-host={host['mac']},{host['ip']}\n")

        share.fileutils.save_file(filelines, folders.dhcpconf)


    def save_localhosts(self):
        """
        Prepare and save /etc/localhosts.list
        """
        filelines = list()

        filelines.append(f"{self._conf['server']['dns_blockip']['value']}\t{self._conf['server']['dns_name']['value']}\n")

        for host in self._conf['server']['dhcp_hosts']:   #Add all localhosts
            filelines.append(f"{host['ip']}\t{host['sysname']}\n")

        share.fileutils.save_file(filelines, folders.localhosts)


    def save_serverconf(self):
        """
        Prepare and save /etc/dnsmasq/server.conf
        Consists of upstream DNS server IP's, listening interface and port
        """
        filelines = []

        filelines.append(f"server={self._conf['server']['dns_serverip1']['value']}\n")
        filelines.append(f"server={self._conf['server']['dns_serverip2']['value']}\n")
        filelines.append(f"interface={self._conf['server']['dns_interface']['value']}\n")
        filelines.append(f"listen-address={self._conf['server']['dns_listenip']['value']}\n")

        if self._conf['server']['dns_listenport']['value'] != '53':        #Only add a non-standard port
            filelines.append(f"port={self._conf['server']['dns_listenport']['value']}\n")

        share.fileutils.save_file(filelines, folders.serverconf)


def main():
    ntrkconfig = NoTrackConfig()
    ntrkconfig.save_localhosts()
    ntrkconfig.save_dhcpconf()
    ntrkconfig.save_serverconf()


if __name__ == "__main__":
    main()
