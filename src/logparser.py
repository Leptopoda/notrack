#!/usr/bin/env python3
#Title       : NoTrack Log Parser
#Description : Parse DNS Logs to MariaDB
#Author      : QuidsUp
#Date        : 2020-07-24
#Version     : 22.10
#Usage       : sudo python3 logparser.py

#Standard imports
from datetime import date

#Local imports
from ntrkmariadb import DBWrapper
import share
import regex

#Create logger
logger = share.createlogger('logparser')

class NoTrackParser():
    _dnslog_file: str = '/var/log/notrack.log'

    def __init__(self):
        self._blocklist_sources = dict()                  #Domains and bl_source from blocklist table
        self._quick_blsources = dict()                    #Quick lookup for domains and bl_source
        self._dbwrapper = DBWrapper()                     #Declare MariaDB Wrapper
        self._dbwrapper.dnslog_createtable()              #Make sure dnslog table exists


    def __del__(self):
        self._blocklist_sources.clear()
        self._quick_blsources.clear()


    def __beautify_domain(self, domain: str) -> str:
        """
        #Remove preceding www. from a domain

        Parameters:
            Domain
        Returns:
            Beautified Domain
        """
        if domain.startswith('www.'):
            domain = domain.lstrip('www.')

        return domain


    def __format_date(self, curyear: int, curmonth: int, log_day: str) -> str:
        """
        Format date for MariaDB
        """
        return date(curyear, curmonth, int(log_day)).isoformat()


    def __get_subdomains(self, domain: str, top_private_domain: str) -> list:
        """
        Get the subdomains from a domain

        Parameters:
            Domain
            Top Private Domain (site.com or site.co.uk from domain)
        Returns:
            List subdomains if any subdomains have been requested
            Or empty list for no subdomains
        """
        subdomains: str

        #Are there any subdomains?
        if domain == top_private_domain:
            return []

        #Drop the Top Private Domain to give a string of subdomains
        subdomains = domain[:-(len(top_private_domain) + 1)]

        #Return list of subdomains split by dot
        return subdomains.split('.')


    def __get_top_private_domain(self, domain: str) -> str:
        """
        Get the top domain from a domain, i.e. site.com or site.co.uk

        Parameters:
            Domain
        Returns:
            Top Private Ddomain
        """
        matches = regex.Domain.search(domain)
        if matches is None:                                #Shouldn't happen
            return domain
        else:
            return f"{matches.group('top_private_domain')}{matches.group('tld')}"


    def __get_tld(self, domain):
        """
        Get Top Level Domain
        """
        matches = regex.TLD.search(domain)
        if matches is None:
            return domain
        else:
            return matches[1]


    def __identify_blocklist(self, domain: str) -> str:
        """
        Returns the blocklist resposible for blocking a certain domain
        Quick sources is provided as user may get repetitive subdomains being requested

        Parameters:
            Domain blocked
        Returns:
            Block list name
        """
        #1. Check quick list
        if domain in self._quick_blsources:
            return self._quick_blsources[domain]

        #2. Check the Top Private Domain
        top_private_domain: str
        top_private_domain = self.__get_top_private_domain(domain)

        if top_private_domain in self._blocklist_sources:
            self._quick_blsources[domain] = self._blocklist_sources[top_private_domain]
            return self._blocklist_sources[top_private_domain]

        #3. Check the domain in DNS log
        if domain in self._blocklist_sources:
            self._quick_blsources[domain] = self._blocklist_sources[domain]
            return self._blocklist_sources[domain]

        #4. Check against bl_tld (Top Level Domain blocking)
        tld: str                                          #Top Level Domain
        tld = self.__get_tld(domain)
        if tld in self._blocklist_sources:
            self._quick_blsources[domain] = self._blocklist_sources[tld]
            return self._blocklist_sources[tld]

        #5. Check all subdomains in reverse order
        checkstr: str = top_private_domain
        subdomains: list

        subdomains = self.__get_subdomains(domain, top_private_domain)
        for i in range(len(subdomains) -1, 0, -1):
            checkstr = f'{subdomains[i]}.{checkstr}'      #Add current subdomain

            if checkstr in self._blocklist_sources:
                self._quick_blsources[checkstr] = self._blocklist_sources[checkstr]
                return self._blocklist_sources[checkstr]

        logger.info(f'Failed to find why {domain} was blocked')
        return 'invalid'                                  #Fallback response


    def blank_dnslog(self):
        """
        Blank DNS log is also called when in incognito mode
        """
        share.blank_file(self._dnslog_file)


    def parsedns(self):
        """
        Parse the dnslog file into dnslog table on MariaDB
        """
        filelen: int
        rows: int
        filelines: list
        requests: dict

        filelines = share.load_file(self._dnslog_file)
        filelen = len(filelines)
        logger.info(f'Read {filelen} lines from DNS Log file')

        #Is the file big enough to bother processing and causing a disk write
        if filelen < 16:
            logger.info('Nothing in dnslog, skipping')
            return

        self.blank_dnslog()
        requests = self.process_log(filelines)
        rows = self.process_requests(requests)
        logger.info(f'Added {rows} rows to dnslog table')


    def process_log(self, filelines: list) -> dict:
        """
        Dnsmasq stores log entries of a request and its result on multiple lines
         which are indexed by a serial number.
        Regex is used to split log lines into serial, action, request, result.
        Log lines for forward and other updates are dropped.

        A dict is used to store the action / result and is based on serial number.
        The block list behind a blocked request is queried and stored.

        Parameters:
            Log file lines
        Returns
            DNS Requests
        """
        curyear = date.today().year    #Current Year (Missing from dnsmasq)
        curmonth = date.today().month  #Current Month (Numeric value required)
        action: str
        domain: str
        line: str
        log_date: str
        result: str
        serial: str                    #serial number assigned by dnsmasq
        matches: dict
        requests = dict()

        for line in filelines:
            matches = regex.DnsmasqLine.match(line)

            #Ignore forward entries and any other system info from dnsmasq
            if matches is None:
                continue

            action = matches.group('action')
            domain = matches.group('domain')
            result = matches.group('res')
            serial = matches.group('serial')

            #Ignore reply query is duplicate log line
            if result == 'duplicate':
                continue

            if serial not in requests:
                #Create new request record
                log_date = self.__format_date(curyear, curmonth, matches.group('log_day'))

                requests[serial] = {
                    'dns_request': self.__beautify_domain(domain),
                    'log_time': f'{log_date} {matches.group("log_time")}',
                    'result': 'unknown',
                    'severity': '0',
                    'sys': matches.group('sys'),
                }

            #Allowed Request - cached or reply
            if action == 'cached' or action == 'reply':
                requests[serial]['severity'] = '1'

                #CNAME results in another query against the serial number
                if result == '<CNAME>':
                    requests[serial]['result'] = 'cname'
                    #Rename record with domain appended to serial number
                    requests[f'{serial}{domain}'] = requests.pop(serial)

                #Non-Existant Domain
                elif result == 'NXDOMAIN':
                    requests[serial]['result'] = 'nxdomain'

                #Issue reaching remote DNS server for this domain
                elif result == 'SERVFAIL':
                    requests[serial]['result'] = 'servfail'

                #Disregard NODATA-IPv4 and NODATA-IPv6 as there was likely another query
                elif result.startswith('NODATA'):
                    continue

                else:
                    if action == 'cached':
                        requests[serial]['result'] = 'cached'
                    else:
                        requests[serial]['result'] = 'allowed'

            #Blocked Request
            elif action == 'config':
                #Find out which blocklist prevented the DNS lookup
                requests[serial]['severity'] = '2'
                requests[serial]['result'] = self.__identify_blocklist(domain)

            #LAN Query
            elif action == '/etc/localhosts.list':
                requests[serial]['severity'] = '1'
                requests[serial]['result'] = 'local'

        return requests


    def process_requests(self, requests: dict) -> int:
        """
        Process the requests from process_log function and build a list of data
         to insert into dnslog SQL table.
        Drop items any where the result is unknown.

        Parameters:
            Dict of requests
        Returns:
            Count of rows added
        """
        req: dict
        sqldata = list()

        for req in requests.values():
            #Drop incomplete / errored items from requests
            if req['result'] == 'unknown':
                continue

            sqldata.append(tuple([req['log_time'], req['sys'], req['dns_request'], req['severity'], req['result']]))
            #print([req['log_time'], req['sys'], req['dns_request'], req['severity'], req['result']]) #DEBUG

        if len(sqldata) == 0:
            return 0

        return self._dbwrapper.dnslog_insertdata(sqldata)


    def readblocklist(self):
        """
        Load blocklist domain table into _blocklist_sources as a dict
        """
        tabledata: list

        tabledata = self._dbwrapper.blocklist_getdomains_listsource()

        self._blocklist_sources.clear()
        self._quick_blsources.clear()

        self._blocklist_sources = dict(tabledata)
        logger.info(f'Number of domains in blocklist: {len(self._blocklist_sources)}')


    def trimlogs(self, days: int):
        """
        Trim rows older than a specified number of days from analytics and dnslog table

        Parameters:
            Interval of days to keep (NOTE When days is set to zero nothing will be deleted)
        """
        self._dbwrapper.analytics_trim(days)
        self._dbwrapper.dnslog_trim(days)


def main():
    print('NoTrack Log Parser')
    logger.setLevel(20)                                   #Change log level to INFO
    ntrkparser = NoTrackParser()
    ntrkparser.readblocklist()
    ntrkparser.parsedns()

    print('NoTrack log parser complete :-)')
    print()


if __name__ == "__main__":
    main()
