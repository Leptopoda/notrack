#NoTrack Regular Expressions
#Author: QuidsUp

#Standard Imports
import re

#Faulty domain matches in dnsmasq - a double hyphen at chars 2-3, not punycode xn--
#Match is carried out agains the reverse string
BadDomain = re.compile(r'\w\-\-[^n][^x]\.')

Defanged = re.compile('^(?:f[txX]p|h[txX][txX]ps?)\[?:\]?\/\/([\w\.\-_\[\]]{1,250}\[?\.\]?[\w\-]{2,63})')

#Regex to extract domain.co.uk from subdomain.domain.co.uk
Domain = re.compile(r'(?P<top_private_domain>[\w\-]{1,63})(?P<tld>\.(?:net?\.|org\.|com?\.|ac\.|edu?\.|go[bv]?\.|ltd\.|mil\.)?[\w\-]{2,63}$)')

#Top Level Domain
TLD = re.compile(r'(\.(?:net?\.|org\.|com?\.|ac\.|edu?\.|go[bv]?\.|ltd\.|mil\.)?[\w\-]{1,63})$')

#Regex to validate a domain entry
ValidDomain = re.compile('^[\w\.\-]{1,250}\.[\w\-]{2,63}$')

#Regex to validate an input for SQL search
ValidInput = re.compile('^[\w\.\-]{1,253}$')

#Regex CSV Line:
#Any number of spaces
#Group 1: domain.com
#Tab or Comma seperator
#Group 2: Comment
CSV = re.compile('^\s*([\w\.\-]{1,253})[\t,]([\w]{1,255})')

#Regex EasyList Line:
#|| Marks active domain entry
#Group 1: domain.com (Need to eliminate IP addresses, so assume TLD begins with [a-z]
#Non-capturing group: Domain ending
#Non-capturing group: Against document type: Acceptable - ^, $3p, $third-party, $all, $doc, $document, $popup, $popup,third-party
EasyLine = re.compile('^\|\|([\w\.\-]{1,250}\.[a-zA-Z][\w\-]{1,62})(?:\^|\.)(?:\^|\$3p|\$third\-party|\^\$?all|\$doc|\$document|\$popup|\$popup,third\-party)?(?:,domain=~localhost.*)?\n$')

#Regex Plain Line
#Group 1: domain.com
#Group 2: optional comment.
#Utilise negative lookahead to make sure that two hashes aren't next to each other,
# as this could be an EasyList element hider
PlainLine = re.compile(r'^([\w\-\.]{1,253})( #(?!#).*)?\n$')

#Regex TLD Line:
TLDLine = re.compile('^(\.\w{1,63})(?:\s#.*)?\n$')

#Regex Unix Line
UnixLine = re.compile('^(?:0|127)\.0\.0\.[01]\s+([\w\.\-]{1,250}\.[\w\-]{2,63})\s*#?(.*)\n$')

#Version from bl_notrack DEPRECATED
NTVersion = re.compile('^# ?LatestVersion (\d{1,2}\.\d{1,2})\n$')

BlockListStatus = re.compile('^\$this\->set_blocklist_status\(\'(bl_\w{2,25})\', (true|false)\);\n$')

BlockListCustom = re.compile('^\$this\->set_blocklist_custom\(\'(.{0,2000})\'\);\n$')

"""
Regex DnsmasqLine
Group log_day will be preceeded by an extra space for single digit
Group sys: IPv4 or IPv6 (no need for specific verification)
Group action: Actions of interest (ignores forward)
Non-Capture: Query Type - A, AAAA (ignores PTR)
Group domain: Domain queried, preceeded by negative-lookahead to ignore IP requests
Group res: what response was given
"""
DnsmasqLine = re.compile(r'^(?P<log_month>\w{3})  ?(?P<log_day>\d{1,2}) (?P<log_time>\d{2}:\d{2}:\d{2}) dnsmasq\[\d{1,7}\]: (?P<serial>\d+) (?P<sys>[\d\.:]+)\/\d+ (?P<action>query|reply|config|cached|\/etc\/localhosts\.list)(?:\[A{1,4}\])? (?![\d\.:])(?P<domain>[\w\.\-]{2,254}) (?:is|to|from) (?P<res>[\w\.\-:<>]+)$')

Filter_Bool = re.compile(r'^[01]$')

Filter_Duration = re.compile(r'^\d{1,2}[dDhH]$')

Filter_IP = re.compile(r'^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))|((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$')

Filter_Str = re.compile(r'^[\w\.\-]+$')


AddHost_Line = re.compile(r"^\('(.+)', '(.+)', '(.+)', '(.+)'\)$")

#Regex for PHP Code from a config file
#Non-capturing group: $config or $this
#->
#Group 1: variable or function name
#Non-capturing group: =
#Group 2: - Non-capturing group: value / Non-capturing group: Function contents
#End with ;\n
PHPLine = re.compile(r"^\$(?:config|this)\->([\w]+)(?:\s*=\s*'?)?((?:[\w\.:\-]+)|(?:\(.+\)))'?;\n$")

StatusLine = re.compile(r"^\$(?:config|this)\->set_status\((\d{1,2}), (\d+)\);\n$")
