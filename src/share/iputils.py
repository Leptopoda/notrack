#!/usr/bin/env python3
#Title       : IP Utils
#Description : Function to get the IP address of NoTrack Server
#Author      : QuidsUp
#Date        : 2022-02-12
#Version     : 22.02
#Usage       : python3 iputils.py

#Standard Imports
import socket

def get_host_ip() -> str:
    """
    Returns the IP address for this host

    Make a network connection to unroutable 169.x IP in order to get system IP
    """
    hostip: str

    try:
        #Connect to an unroutable 169.254.0.0/16 address on port 1
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("169.254.0.255", 1))
    except socket.error as e:
        print('Unable to open network connection')
        print(e)
        exit(1)
    else:
        hostip = s.getsockname()[0]
    finally:
        s.close()

    return hostip


def get_host_name() -> str:
    """
    Returns the name for this host
    """
    return socket.gethostname()


if __name__ == "__main__":
    print(get_host_ip())
    print(get_host_name())
