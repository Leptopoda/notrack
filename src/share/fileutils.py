#!/usr/bin/env python3
#Title       : NoTrack File Utilities
#Description : Misc functions for interacting with files
#Author      : QuidsUp
#Date        : 2022-01-22
#Version     : 22.03
#Usage       : N/A this is loaded by other modules


#Standard imports
import json
import os
import shutil
import sys
import time

#Local imports
import share.errorlogger

#Create logger
logger = share.errorlogger.createlogger('fileutils')


def blank_file(filename: str) -> bool:
        """
        Overwrite a file with nothing to blank it out
        """
        global logger

        logger.info(f'Blanking out {filename}')

        try:
            f = open(filename, 'w')                       #Open log file for ascii writing
        except IOError as e:
            logger.error(f'Unable to blank {filename}')
            logger.error(e)
            return False
        except OSError as e:
            logger.error(f'Unable to blank {filename}')
            logger.error(e)
            return False
        else:
            f.write('')                                   #Write blank line
            f.close()
        return True


def copy(src: str, dest: str) -> bool:
    """
    Copy a File or Folder from source to destination

    Parameters:
        Source file
        Destination file
    Returns:
        True on success, False on failure
    """
    global logger

    if os.path.isdir(src):                                 #Check if source is a folder
        logger.info(f'Copying folder: {src} to {dest}')
        try:                                               #Attempt to copy folder
            shutil.copytree(src, dest, dirs_exist_ok=True)
        except:
            logger.error(f'Unable to copy folder {src} to {dest}')
            return False

    elif os.path.isfile(src):                              #Check if source is a file
        logger.info(f'Copying file: {src} to {dest}')
        try:                                               #Attempt to copy the file
            shutil.copy(src, dest)
        except OSError as e:
            logger.error(f'Unable to copy file {src} to {dest}')
            logger.error(e)
            return False

    else:                                                  #Nothing found
        logger.error(f'Unable to copy {src}, source is missing')
        return False

    return True


def move_file(source: str, destination: str, permissions=0):
    """
    Move a File or Folder from source to destination
    Optional: Set file permissions

    Parameters:
        Source file
        Destination file
        Optional file permissions
    Returns:
        True on success, False on failure
    """
    global logger

    #Check source file/folder exists
    if not os.path.isfile(source) and not os.path.isdir(source):
        logger.error(f'Unable to move {source}, file is missing')
        return False

    try:
        shutil.move(source, destination)                   #Attempt to move the file
    except IOError as e:
        logger.error(f'Unable to move {source} to {destination}')
        logger.error(e)
        return False
    else:
        if permissions != 0:
            os.chmod(destination, permissions)
    return True


def delete(src: str) -> bool:
    """
    Delete a File or Folder

    Parameters:
        src (str): File or Folder to delete
    Returns:
        True on success, False on failure or not needed
    """
    global logger

    if os.path.isdir(src):                                #Check if src is a folder
        logger.info(f'Deleting folder: {src}')
        try:                                              #Attempt to delete folder
            shutil.rmtree(src, ignore_errors=True)
        except:
            logger.error(f'Unable to delete folder {src}')
            logger.error(e)
            return False

    elif os.path.isfile(src):                             #Check if src is a file
        logger.info(f'Deleting file: {src}')
        try:                                              #Attempt to delete file
            os.remove(src)
        except OSError as e:
            logger.error(f'Unable to delete file {src}')
            logger.error(e)
            return False

    else:                                                 #Nothing found
        return False

    return True


def file_exists(filename: str) -> bool:
    """
    Check if a file exists
    """
    return os.path.isfile(filename)


def load_file(filename: str) -> list:
    """
    Load contents of file and return as a list
    Parameters:
        file name with folder
    Returns:
        List of all lines in file
        Empty list if file doesn't exist or error occured
    """
    global logger
    filelines: list

    if not os.path.isfile(filename):
        logger.info(f'Unable to load {filename}, file is missing')
        return []

    try:
        f = open(filename, 'r')                            #Open file for reading
    except IOError as e:
        logger.error(f'Unable to read to {filename}')
        logger.error(e)
        return []
    except OSError as e:
        logger.error(f'Unable to read to {filename}')
        logger.error(e)
        return []
    else:
        filelines = f.readlines()
        f.close()
    return filelines


def load_json(filename: str) -> dict:
    """
    Load contents of file and return as a list
    Parameters:
        file name with folder
    Returns:
        List of all lines in file
        Empty list if file doesn't exist or error occured
    """
    global logger
    jsondata: dict

    logger.info(f'Loading {filename}')
    if not os.path.isfile(filename):
        logger.warning(f'Unable to load {filename}, file is missing')
        return {}

    try:
        f = open(filename, 'r')                            #Open file for reading
    except IOError as e:
        logger.error(f'Unable to read to {filename}')
        logger.error(e)
        return {}
    except OSError as e:
        logger.error(f'Unable to read to {filename}')
        logger.error(e)
        return {}
    else:
        jsondata = json.load(f)
        f.close()
    return jsondata


def save_blob(data, filename: str) -> bool:
    """
    Save Blob
    Save a binary blob to a file

    Parameters:
        Data to save to disk
        Filename
    Returns:
        True on success, False on failure
    """
    try:
        f = open(filename, 'wb')                           #Open file for binary writing
    except IOError as e:
        logger.error(f'Error writing to {filename}')
        logger.error(e)
        return False
    except OSError as e:
        logger.error(f'Error writing to {filename}')
        logger.error(e)
        return False
    else:
        f.write(data)
        f.close()
    return True


def save_file(lines: list, filename: str) -> bool:
    """
    Save a list into a text file

    Parameters:
        Lines of ascii data to save to file
        File to save to
    Returns:
        True on success, False on failure
    """
    try:
        f = open(filename, 'w')                            #Open file for ascii writing
    except IOError as e:
        logger.error(f'Unable to write to {filename}')
        logger.error(e)
        return False
    except OSError as e:
        logger.error(f'Unable to write to {filename}')
        logger.error(e)
        return False
    else:
        f.writelines(lines)
        f.close()
    return True


def extract_file(srczip: str, destination: str, tempdir: str):
    """
    Unzip a file to destination

    Parameters:
        srczip (str): Zip file
        destination (str): Output destination
    """
    from zipfile import ZipFile
    global logger

    with ZipFile(srczip) as zipobj:
        for compressedfile in zipobj.namelist():
            if compressedfile.endswith('.txt'):
                zipobj.extract(compressedfile, f'{tempdir}/')
                logger.info(f'Extracting {compressedfile}')
                move_file(f'{tempdir}/{compressedfile}', destination)


def find_notrack() -> str:
    """
    Based on current working directory
    """
    cwd = ''
    location = ''

    cwd = os.getcwd()                                     #Get current working directory
    location = os.path.dirname(cwd)                       #Get parent folder

    return location


def get_modified_time(filename: str) -> float:
    """
    Get file modified time

    Parameters:
        File Name
    Returns:
        Modified time on success, or 0.0 on failure
    """
    modtime = 0.0

    if not os.path.isfile(filename):
        #File missing, return zero
        return 0.0

    try:                                               #Attempt to change ownership
        modtime = os.path.getmtime(filename)
    except OSError as e:
        logger.error(f'Unable to get modified time of {filename}')
        logger.error(e)
        return 0.0

    return modtime


def get_owner(src: str) -> object:
    """
    Get file / folder ownership details

    Parameters:
        src (str): Source File or Folder
    Returns:
        statinfo class
        NOTE use st_uid and st_gid for user / group ownership
    """
    if not os.path.isfile(src) and not os.path.isdir(src):
        logger.error(f'{src} is missing, unable to find owner')
        return None

    return os.stat(src)


def set_owner(src: str, uid: int, gid: int, mode: oct) -> bool:
    """
    Set file / folder ownership details

    Parameters:
        src (str): Source File or Folder
        uid (int): User ID
        gid (int): Group ID
    Returns:
        True on success, False on failure
    """
    if os.path.isfile(src):                                #Single file
        try:                                               #Attempt to change ownership
            os.chown(src, uid, gid)
        except OSError as e:
            logger.error(f'Unable to change ownership of {src}')
            logger.error(e)
            return False
        else:
            set_permissions(src, mode)

    elif os.path.isdir(src):                               #Multiple files / folders
        for root, dirs, files in os.walk(src):             #Use os.walk to navigate struct
            os.chown(root, uid, gid)
            for item in dirs:
                os.chown(os.path.join(root, item), uid, gid)
            for item in files:
                os.chown(os.path.join(root, item), uid, gid)
                set_permissions(os.path.join(root, item), mode)

    else:
        logger.error(f'{src} is missing, unable to change ownership')
        return False

    return True


def set_permissions(src: str, mode: oct) -> bool:
    """
    Set permissions of a file

    Parameters:
        src: Source File
        mode: Octal Mode
    Returns:
        True on success, False on failure
    """
    if os.path.isfile(src):                                #Single file
        try:                                               #Attempt to change ownership
            os.chmod(src, mode)
        except OSError as e:
            logger.error(f'Unable to change ownership of {src}')
            logger.error(e)
            return False

    else:
        logger.error(f'{src} is missing, unable to change permissions')
        return False

    return True
