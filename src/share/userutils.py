#!/usr/bin/env python3
#Title       : NoTrack User Utilities
#Description : Functions for validating user permissions and currently running user
#Author      : QuidsUp
#Date        : 2022-01-22
#Version     : 22.01
#Usage       : N/A this is loaded by other modules


#Standard imports
import os


def check_root():
    """
    Check script is being run as root
    """
    if os.geteuid() != 0:
        print('ERROR  This script has been run with insufficient permissions')
        print('Please rerun with sudo or as root user')
        exit(2)


def find_username(location: str) -> str:
    """
    Find username depending on OS Type
    TODO Complete for other OS's
    """
    if os.name == 'posix':
        return find_unix_username(location)

    return ''


def find_unix_username(location: str) -> str:
    """
    Match the home folder against username with data from /etc/passwd

    Parameters:
        None
    Returns:
        username or root
    """
    import pwd
    passwd = pwd.getpwall()                            #Get groups from /etc/passwd

    if not location.startswith('/home'):  #Return root for any non-home directory
        return 'root'

    for obj in passwd:
        if obj.pw_dir == '/':                          #Disregard anything for root folder
            continue
        #Check if there is any match with this users home folder location
        if location.startswith(obj.pw_dir):
            return obj.pw_name                         #Yes, return username

    return 'root'                                      #No match found, return root
