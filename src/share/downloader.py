#!/usr/bin/env python3
#Title       : NoTrack URL Downloader
#Description : I wrote this before knowing about requests. Bit too late to change as its not always installed
#Author      : QuidsUp
#Date        : 2022-01-23
#Version     : 22.01
#Usage       : N/A this is loaded by other modules

#Standard imports
from urllib.request import Request, urlopen
from urllib.error import HTTPError, URLError

#Local imports
import share.errorlogger
from share.fileutils import save_blob

#Create logger
logger = share.errorlogger.createlogger('downloader')

#https://www.whatismybrowser.com/guides/the-latest-user-agent/firefox
#https://www.whatismybrowser.com/guides/the-latest-user-agent/chrome

#Selection of user-agents to try
user_agents = {
    'wget/1.20.3',
    'Mozilla/5.0 (Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'
}

def download(url: str, dest: str) -> bool:
    """
    Download File
    Some sites reject the default python/urllib agent, so we try from user_agents set

    Parameters:
        URL
        File Destination
    Returns:
        True - Success
        False - Failed download
    """
    res_code = 0

    for user_agent in user_agents:
        headers = {
            'User-Agent': user_agent
        }

        req = Request(url, headers=headers)

        try:
            response = urlopen(req)
        except HTTPError as e:
            if e.code == 404 or e.code == 429:
                #Page not found / Rate Limited, no point in trying again
                logger.error(f'Unable to download {url}')
                logger.error(f'HTTP Error {e.code}: {e.reason}')
                return False
            else:
                #Any other error - Take another attempt up to max of for loop
                logger.warning(f'Unable to download {url}')
                logger.warning(f'HTTP Error {e.code}: {e.reason}')
        except URLError as e:
            logger.error(f'Unable to download {url}')
            logger.error(e)
        else:
            res_code = response.getcode()
            if res_code in range (200, 203):
                #Successful download
                save_blob(response.read(), dest)
                return True
            elif res_code in range(204, 299):
                #Successful connection, but no data returned
                logger.warning(f'HTTP Response {res_code}: No data found from {url}')
            else:
                #Another error happened
                logger.warning(f'HTTP Response {res_code}: unable to download {url}')

    return False
