#!/usr/bin/env python3
#Title       : NoTrack
#Description : This script will download latest block lists from various sources, then parse them into Dnsmasq
#Author      : QuidsUp
#Date        : 2015-01-14
#Version     : 22.03
#Usage       : sudo python notrack.py

#Standard imports
import os
import shutil
import sys
import time

#Local imports
import folders
import regex
import share
from config import NoTrackConfig
from ntrkmariadb import DBWrapper
from ntrkservices import Services
from statusconsts import *

#Create logger
logger = share.createlogger('blockparser')

MAX_AGE = 169200 #47 Hours in Seconds
#MAX_AGE = 43200

config = NoTrackConfig()

class BlockParser:
    _dedupcount: int = 0                    #Per list deduplication count
    _domaincount: int = 0                   #Per list of added domains
    _totaldedupcount: int = 0
    _dns_config_allowline: str              #Template line for DNS config file Allow
    _dns_config_blockline: str              #Template line for DNS config file Block
    _allowed_domains = set()                #Quick lookup for allowed domains
    _blocked_domains = set()                #Quick lookup for blocked domains
    _blocked_tlds = set()                   #Quick lookup for blocked top level domain
    _blocklisttemplate = dict()             #JSON Data from blocklisttemplate.json
    _domains_to_block = list()              #List of tupples for the blocklist

    def __init__(self):
        config.load_blocklists()

        self.__services = Services()                       #Declare service class
        self.__dbwrapper = DBWrapper()                     #Declare MariaDB Wrapper

        self._blocklisttemplate = share.load_json(f'{folders.webdir}/include/blocklisttemplate.json')

        #Fill in users blacklist and tld list locations
        self._blocklisttemplate['bl_allowlist']['url'] = folders.whitelist
        self._blocklisttemplate['bl_blacklist']['url'] = folders.blacklist
        self._blocklisttemplate['bl_tld']['url'] = folders.tldist

        #Fill in _dns_block_line and _dns_config_allowline based on host IP
        self.__get_hostdetails()

    def __del__(self):
        self._allowed_domains.clear()
        self._blocked_domains.clear()
        self._blocked_tlds.clear()
        self._domains_to_block.clear()


    def _dns_allow_line(self, domain: str) -> str:
        """
        Formatted line for allowing a domain
        """
        return self._dns_config_allowline % domain


    def _dns_block_line(self, domain: str) -> str:
        """
        Formatted line for blocking a domain
        """
        return self._dns_config_blockline % domain


    def __get_hostdetails(self) -> None:
        """
        Get Host Name and IP address for _dns_config_blockline and _dns_config_allowline
        If the users config dns_blockip is localhost or blank, then the actual
         Host IP address will need to be obtained via sockets in share.iputils
        """
        allowline: str
        blockline: str
        dns_blockip: str
        host_ip: str
        host_name: str

        dns_blockip = config.get('server', 'dns_blockip')
        if dns_blockip == '127.0.0.1' or dns_blockip == '::1' or dns_blockip == '':
            host_ip = share.get_host_ip()
        else:
            host_ip = dns_blockip

        host_name = share.get_host_name()

        logger.info(f'Hostname: {host_name}, IP Address: {host_ip}')

        #Setup the template strings for writing out to black/white list files
        [blockline, allowline] = self.__services.get_dnstemplatestr(host_name, host_ip)

        self._dns_config_allowline = allowline
        self._dns_config_blockline = blockline


    def __add_domain(self, subdomain: str, comment: str, source: str) -> None:
        """
        Process supplied domain and add it to self._domains_to_block
        1. Extract domain.co.uk from say subdomain.domain.co.uk
        2. Check if domain.co.uk is in _blocked_domains set
        3. If subdomain is actually a domain then record _blocked_domains set
        4. Reverse subdomain
        5. Append to self._domains_to_block as [reverse, subdomain, comment, source]

        Parameters:
            Subdomain or domain
            A comment
            Source block list name
        """
        reverse: str                                       #Reversed domain

        matches = regex.Domain.search(subdomain)

        if matches == None:                                #Could be a TLD instead?
            self.__add_top_level_domain(subdomain, comment, source)
            return

        if matches.group(0) in self._blocked_domains:
            #Domain has already been processed, either as allowed or blocked
            #logger.debug(f'{subdomain} is already in blockdomainset as {matches.group(0)}')
            self._dedupcount += 1
            self._totaldedupcount += 1
            return

        if matches.group('tld') in self._blocked_tlds:
            #Domain has already been blocked by TLD
            #logger.debug(f'{subdomain} is blocked by TLD {matches.group(2)}')
            return

        if matches.group(0) == subdomain:
            #Only add private domain to blockdomainset and ignore subdomains
            #logger.debug(f'Adding domain {subdomain}')
            self._blocked_domains.add(subdomain)

        #Reverse the domain for later sorting and deduplication
        #An Extra dot is required to match other subdomains and avoid similar spellings
        reverse = subdomain[::-1] + '.'

        #Drop faulty domain matches in dnsmasq - a double hyphen at chars 2-3, not punycode xn--
        if regex.BadDomain.search(reverse) is not None:
            return

        self._domains_to_block.append(tuple([reverse, subdomain, comment, source]))
        self._domaincount += 1


    def __add_top_level_domain(self, tld: str, comment: str, source: str) -> None:
        """
        Process Top Level Domain and add it to _blocked_tlds set

        Parameters:
            Possible Top Level Domain
            A comment
            Source block list name
        """
        reverse: str

        matches = regex.TLD.search(tld)

        if matches == None:                                #Don't know what it is
            return

        self._blocked_tlds.add(tld)
        reverse = tld[::-1] + '.'
        self._domains_to_block.append(tuple([reverse, tld, comment, source]))
        self._domaincount += 1


    def __match_defanged(self, line: str, listname: str) -> bool:
        """
        Checks custom blocklist file line against Defanged List line regex

        Parameters:
            Line from file
            Block list name
        Returns:
            True on successful match
            False when no match is found
        """
        matches = regex.Defanged.search(line)                  #Search for first match

        if matches is not None:                                #Has a match been found?
            #Add group 1 - Domain and replace defanged [.] with .
            self.__add_domain(matches.group(1).replace('[.]', '.'), '', listname)
            return True

        return False                                           #Nothing found, return False


    def __match_easyline(self, line: str, listname: str) -> bool:
        """
        Checks custom blocklist file line against Easy List line regex

        Parameters:
            Line from file
            Block list name
        Returns:
            True on successful match, False when no match is found
        """
        matches = regex.EasyLine.search(line)                  #Search for first match

        if matches is not None:                                #Has a match been found?
            self.__add_domain(matches.group(1), '', listname)  #Add group 1 - Domain
            return True

        return False                                           #Nothing found, return False


    def __match_plainline(self, line: str, listname: str) -> bool:
        """
        Checks custom blocklist file line against Plain List line regex

        Parameters:
            Line from file
            Block list name
        Returns:
            True on successful match, False when no match is found
        """
        matches = regex.PlainLine.search(line)                 #Search for first match

        if matches is not None:                                #Has a match been found?
            self.__add_domain(matches.group(1), matches.group(2), listname)
            return True

        return False                                           #Nothing found, return False


    def __match_unixline(self, line: str, listname: str) -> bool:
        """
        Checks custom blocklist file line against Unix List line regex

        Parameters:
            Line from file
            Block list name
        Returns:
            True on successful match, False when no match is found
        """
        matches = regex.UnixLine.search(line)                  #Search for first match

        if matches is not None:                                #Has a match been found?
            self.__add_domain(matches.group(1), matches.group(2), listname)
            return True

        return False                                           #Nothing found, return False


    def __process_allowlist(self, lines: list, linecount: int, listname: str) -> None:
        """
        Add items from an allowlist into both blocked domains and allowed domains
        NOTE A domain being in the block domain set will prevent it from being added later to the block domain list
        """
        line: str
        num_allowed_domains: int
        sqldata = list()
        splitline = list()

        logger.info(f'Processing allow list {listname}')

        for line in lines:
            splitline = line.split('#', 1)
            if splitline[0] == '\n' or splitline[0] == '': #Ignore Comment line or Blank
                continue

            self._blocked_domains.add(splitline[0][:-1])
            self._allowed_domains.add(splitline[0][:-1])

            if len(splitline) > 1:                         #Line has a comment
                sqldata.append(tuple(['whitelist', splitline[0][:-1], True, splitline[1][:-1]]))
            else:                                          #No comment, leave it blank
                sqldata.append(tuple(['whitelist', splitline[0][:-1], True, '']))

        num_allowed_domains = len(self._allowed_domains)

        if num_allowed_domains > 0:
            logger.info(f'Number of domains in allow list: {num_allowed_domains}')
            self.__dbwrapper.blocklist_insertdata(sqldata)


    def __process_customlist(self, lines: list, listname: str) -> None:
        """
        We don't know what type of list this is, so try regex match against different types
        1. Reset dedup and domain counters
        2. Read list of lines
        3. Try different regex matches

        Parameters:
            lines (list): List of lines
            Block list name
        """
        line: str
        linecount: int
        self._dedupcount = 0                              #Reset per list dedup count
        self._domaincount = 0                             #Reset per list domain count

        linecount = len(lines)

        for line in lines:                                 #Read through list
            if self.__match_plainline(line, 'custom'):     #Try against Plain line
                continue
            if self.__match_easyline(line, 'custom'):      #Try against Easy List
                continue
            if self.__match_unixline(line, 'custom'):      #Try against Unix List
                continue
            self.__match_defanged(line, 'custom')          #Finally try against Defanged

        logger.info(f'Added {self._domaincount}, Deduplicated {self._dedupcount}')
        self.__dbwrapper.blockliststats_insert(listname, linecount, self._domaincount)


    def __process_csv(self, filelines: list, linecount: int, listname: str) -> None:
        """
        List of domains in a CSV file, assuming cell 1 = domain, cell 2 = comment
        1. Reset dedup and domain counters
        2. Read list of filelines
        3. Check regex match against regex.CSV
        4. Add domain and comment

        Parameters:
            filelines (list): List of lines from file to being processed
            Count of lines in file being processed
            Block list name
        """
        line: str

        self._dedupcount = 0                              #Reset per list dedup count
        self._domaincount = 0                             #Reset per list domain count

        for line in filelines:                            #Read through list
            matches = regex.CSV.match(line)

            if matches is not None:                       #Has a match been found?
                #Add Group 1 - Domain, Group 2 - Comment
                self.__add_domain(matches.group(1), matches.group(2), listname)

        logger.info(f'Added {self._domaincount}, Deduplicated {self._dedupcount}')
        self.__dbwrapper.blockliststats_insert(listname, linecount, self._domaincount)


    def __process_easylist(self, lines: list, linecount: int, listname: str) -> None:
        """
        List of domains in Adblock+ filter format [https://adblockplus.org/filter-cheatsheet]
        1. Reset dedup and domain counters
        2. Read list of lines
        3. Check regex match against regex.EasyLine
        4. Add domain

        Parameters:
            lines (list): List of lines
            Count of lines in file being processed
            Block list name
        """
        self._dedupcount = 0                              #Reset per list dedup count
        self._domaincount = 0                             #Reset per list domain count

        for line in lines:                                #Read through list
            matches = regex.EasyLine.search(line)         #Search for first match

            if matches is not None:                       #Has a match been found?
                #Add group 1 - Domain, NOTE no comment is available
                self.__add_domain(matches.group(1), '', listname)

        logger.info(f'Added {self._domaincount}, Deduplicated {self._dedupcount}')
        self.__dbwrapper.blockliststats_insert(listname, linecount, self._domaincount)


    def __process_plainlist(self, lines: list, linecount: int, listname: str) -> None:
        """
        List of domains with optional # separated comments
        1. Reset dedup and domain counters
        2. Read list of lines
        3. Split each line by hash delimiter
        4. Add domain

        Parameters:
            lines (list): List of lines
            Count of lines in file being processed
            Block list name
        """
        splitline = list()

        self._dedupcount = 0                              #Reset per list dedup count
        self._domaincount = 0                             #Reset per list domain count

        for line in lines:                                 #Read through list
            splitline = line.split('#', 1)                 #Split by hash delimiter

            if splitline[0] == '\n' or splitline[0] == '': #Ignore Comment line or Blank
                continue

            if len(splitline) > 1:                         #Line has a comment
                self.__add_domain(splitline[0][:-1], splitline[1][:-1], listname)

            else:                                          #No comment, leave it blank
                self.__add_domain(splitline[0][:-1], '', listname)

        logger.info(f'Added {self._domaincount}, Deduplicated {self._dedupcount}')
        self.__dbwrapper.blockliststats_insert(listname, linecount, self._domaincount)


    def __process_unixlist(self, lines: list, linecount: int, listname: str):
        """
        List of domains starting with either 0.0.0.0 or 127.0.0.1 domain.com
        1. Reset dedup and domain counters
        2. Read list of lines
        3. Check regex match against regex.UnixLine
        4. Add domain

        Parameters:
            lines (list): List of lines
            Count of lines in file being processed
            Block list name
        """
        self._dedupcount = 0                              #Reset per list dedup count
        self._domaincount = 0                             #Reset per list domain count

        for line in lines:                                 #Read through list
            matches = regex.UnixLine.search(line)          #Search for first match
            if matches is not None:                        #Has a match been found?
                self.__add_domain(matches.group(1), '', listname)  #Add group 1 - Domain

        logger.info(f'Added {self._domaincount}, Deduplicated {self._dedupcount}')
        self.__dbwrapper.blockliststats_insert(listname, linecount, self._domaincount)


    def __review_tld_blocks(self) -> None:
        """
        Check for any domains in allow list impacted by the TLD blocks?
        This should be done after TLD and users block lists are processed
        """
        impacted_domains: int
        filelines = list()

        #Check for allowed domains for any affected by TLD blocks
        for domain in self._allowed_domains:
            matches = regex.Domain.search(domain)         #Extract the TLD
            if matches.group('tld') in self._blocked_tlds:
                filelines.append(self._dns_allow_line(domain))

        impacted_domains = len(filelines)
        if impacted_domains > 0:                         #Any domains impacted?
            logger.info(f'{impacted_domains} domains added to a DNS allow list in order avoid being blocked Top Level Domain blocking')
            share.save_file(filelines, f'{folders.dnslists}whitelist.list')

        else:
            logger.info('No allowed domains impacted by Top Level Domain blocking')
            share.delete(f'{folders.dnslists}whitelist.list')


    def __check_file_age(self, filename: str) -> bool:
        """
        Check last modified time is within MAX_AGE (2 days)

        Parameters:
            File Name
        Returns:
            True update file
            False file within MAX_AGE
        """
        if time.time() > (share.get_modified_time(filename) + MAX_AGE):
            logger.info(f'{filename} is older than 2 days or missing')
            return True

        logger.info(f'{filename} in date, skip downloading new copy')
        return False


    def __download_list(self, url: str, listname: str, destination: str) -> bool:
        """
        Download file
        Request file is unzipped (if necessary)

        Parameters:
            url (str): URL
            listname (str): List name
            destination (str): File destination
        Returns:
            True success
            False failed download
        """
        extension: str
        outputfile: str

        logger.info(f'Downloading block list: {url}')

        #Prepare for writing downloaded file to temp folder
        if url.endswith('zip'):
            #Zip files need handling differently to uncompressed files
            extension = 'zip'
            outputfile = f'{folders.tempdir}/{listname}.zip'

        else:
            #Other extension - Assume .txt for output
            extension = 'txt'
            outputfile = destination

        if not share.download(url, outputfile):
            #There was a problem downloading the file
            return False

        if extension == 'zip':
            share.extract_file(outputfile, destination, folders.tempdir)

        return True


    def __action_lists(self) -> None:
        """
        Go through config and process each enabled list
        1. Skip disabled lists
        2. Check if list is downloaded or locally stored
        3. For downloaded lists
        3a. Check file age
        3b. Download new copy if out of date
        4. Read file into filelines list
        5. Process list
        """
        blname: str                                       #Block list name (shortened)
        filename: str                                     #Block list file name
        linecount: int

        for blname, bl in self._blocklisttemplate.items():
            if not config.get('blocklist', blname, default=bl['enabled']):
                continue

            logger.info(f'Processing {blname}')

            #Is this a downloadable file or locally stored?
            if bl['url'].startswith('http') or bl['url'].startswith('ftp'):
                filename = f'{folders.tempdir}/{blname}.txt' #Download to temp folder
                if self.__check_file_age(filename):
                    #Old file - download a new copy
                    self.__download_list(bl['url'], blname, filename)

            else:
                #URL is actually the filename of a local file
                filename = bl['url'];

            filelines = share.load_file(filename)
            linecount = len(filelines)

            if linecount == 0:                            #Anything read from file?
                logger.warning(f'Data missing from {blname}, unable to process')
                continue

            if bl['process'] == 'plain':
                self.__process_plainlist(filelines, linecount, blname)
            elif bl['process'] == 'easylist':
                self.__process_easylist(filelines, linecount, blname)
            elif bl['process'] == 'unixlist':
                self.__process_unixlist(filelines, linecount, blname)
            elif bl['process'] == 'csv':
                self.__process_csv(filelines, linecount, blname)
            elif bl['process'] == 'allowlist':
                self.__process_allowlist(filelines, linecount, blname)
            else:
                logger.error(f"Unknown processing type: {bl['process']}")
                return

            logger.info(f'Finished processing {blname}')


    def __action_customlists(self) -> None:
        """
        Go through config and process each enabled list
        1. Skip disabled lists
        2. Check if list is downloaded or locally stored
        3. For downloaded lists
        3a. Check file age
        3b. Download new copy if out of date
        4. Read file into filelines list
        5. Process list based on type
        """
        blname: str
        blfilename: str                                   #Block list file name
        blurl: str                                        #Block list URL
        custom_str: str                                   #The custom line from bl.php
        i: int = 0                                        #Loop position (for naming)
        customurllist = list()

        custom_str = config.get('blocklist', 'custom')
        if custom_str == '':
            logger.info('No additional Custom Blocklists to process')
            return

        logger.info('Processing Custom Blocklists')

        customurllist = custom_str.split(',')             #Explode comma seperated vals

        for blurl in customurllist:
            i += 1
            blname = f'bl_custom{i}'                      #Make up a name
            logger.info(f'{blname} - {blurl}')

            #Is this a downloadable file or locally stored?
            if blurl.startswith('http') or blurl.startswith('ftp'):
                #Download to temp folder with loop position in file name
                blfilename = f'{folders.tempdir}/{blname}.txt'
                if self.__check_file_age(blfilename):      #Does file need freshening?
                    self.__download_list(blurl, blname, blfilename)

            else:                                          #Local file
                blfilename = blurl;

            filelines = share.load_file(blfilename)
            if not filelines:                              #Anything read from file?
                logger.warning(f'File missing, unable to process {blname}')
                continue

            self.__process_customlist(filelines, blname)
            logger.info(f'Finished processing {blname}')


    def __dedup_lists(self) -> None:
        """
        Final sort and then save list to file
        1. Sort the blocklist by the reversed domain (blocklist[x][0])
        2. Check if each item matches the beginning of the previous item
            (i.e. a subdomain of a blocked domain)
        3. Remove matched items from the list
        4. Add unique items into sqldata and blacklist
        5. Save blacklist to file
        6. Insert SQL data
        """
        previous_domain: str = '\0'  #Previous has to be something (e.g. a null byte)
        dns_blacklist = list()
        sqldata = list()

        self._dedupcount = 0
        logger.info('Sorting and Deduplicating blocklist')

        self._domains_to_block.sort(key=lambda x: x[0])   #Sort list on col0 "reversed"
        for item in self._domains_to_block:
            if item[0].startswith(previous_domain):
                self._dedupcount += 1
            else:
                dns_blacklist.append(self._dns_block_line(item[1]))
                sqldata.append(tuple([item[3], item[1], True, item[2]]))
                previous_domain = item[0]

        logger.info(f'Further deduplicated {self._dedupcount} domains')
        logger.info(f'Final number of domains in blocklist: {len(dns_blacklist)}')

        share.save_file(dns_blacklist, f'{folders.dnslists}notrack.list')
        self.__dbwrapper.blocklist_insertdata(sqldata)


    def create_blocklist(self) -> None:
        """
        Create blocklist and restart DNS Server

        1. Make sure blocklist and blockliststats SQL tables exist
        2. Clear SQL tables
        3. Action default lists, starting with allow lists
        4. Action users custom lists
        5. Deduplicate list
        6. Restart DNS Server
        """
        self.__dbwrapper.blocklist_createtable()                      #Create SQL Tables
        self.__dbwrapper.blockliststats_createtable()
        self.__dbwrapper.blocklist_cleartable()                       #Clear SQL Tables

        self.__action_lists()
        self.__action_customlists()

        if len(self._allowed_domains) == 0:
            logger.info('Nothing in allow list, deleting file')
            share.delete(f'{folders.dnslists}whitelist.list')

        self.__review_tld_blocks()

        logger.info('Finished processing all block lists')
        logger.info(f'Total number of domains added: {len(self._domains_to_block)}')
        logger.info(f'Total number of domains deduplicated: {self._totaldedupcount}')

        self.__dedup_lists()                                          #Dedup then insert domains
        self.__services.restart_dnsserver()


    def disable_blocking(self) -> None:
        """
        Move blocklist to temp folder
        """
        if share.move_file(folders.main_blocklist, folders.temp_blocklist):
            logger.info('Moving blocklist to temp folder')
        else:
            logger.warning('Blocklist missing')

        self.__services.restart_dnsserver()


    def enable_blockling(self) -> None:
        """
        Move temp blocklist back to DNS config folder
        """
        if share.move_file(folders.temp_blocklist, folders.main_blocklist):
            logger.info('Moving temp blocklist back')
            self.__services.restart_dnsserver()

        else:
            logger.warning('Temp blocklist missing, I will recreate it')
            self.create_blocklist()



def main():
    print('NoTrack Block List Parser')
    logger.setLevel(20)                                   #Change log level to INFO
    share.check_root()
    blockparser = BlockParser()
    blockparser.create_blocklist()
    print('Finished creating block list for NoTrack :-)')
    print()

if __name__ == "__main__":
    main()
