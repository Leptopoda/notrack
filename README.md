# NoTrack
NoTrack is a [DNS-Sinkhole](https://en.wikipedia.org/wiki/DNS_sinkhole) which protects all devices on your home network from visiting Tracking, Advertising, and Malicious websites.   

NoTrack provides a DNS Server, Log Collector, and Web Interface to provide you maximum control over what your computers and smart phones can access.  
All data stored and processed on your network is for you to do what you want with. No logs are ever sent outside of your network.  
This means I have no idea how many people use NoTrack, and I'm happy with it being that way.

## Preventing Tracking  
Tracking is absolutely rife, on average [30 cookies](https://www.cookiebot.com/en/cookie-checker/) are set by each website. Although you can block third-party cookies with a web browser, there are also more complex methods of tracking, such as:
- Tracking Pixels
- HTML5 Canvas Fingerprinting
- AudioContext Fingerprinting
- WebRTC Local IP Discovery  

NoTrack works by sinkholing known tracking / advertising domains, so that you end up with a block page being displayed, instead of ending up with some unwanted tracker.

[NoTrack Blocklist](https://gitlab.com/quidsup/notrack-blocklists) is one of the largest DNS blocklists dedicated to blocking access to Tracking sites.  
You can also utilise the NoTrack [Annoyance Blocklist](https://gitlab.com/quidsup/notrack-annoyance-blocklist) to prevent a wide range of infurating cookie popups.  
  
# Features    
### Web Interface Dashboard   
At a glance see how many sites are in your blocklist, number of DNS Queries, number of Systems on your Network, and volume of traffic over the past 24 hours, as well as links to all the other admin features in the custom built interface.
![notrackmain](https://gitlab.com/quidsup/notrack/-/wikis/uploads/a14d33af928c142ef4734bb6eacd8728/Screenshot_20211014_220437.png)
   
### Analytics
NoTrack Analytics will monitor your traffic and provide an Alert when any of your devices accessed domains suspected of being related to Tracking or Advertising, when these have not been blocked.  
You have the option of submitting suspect domains to NoTrack Blocklist for inclusion, or you can add it to your custom Block / Allow list.
  
NoTrack Analytics runs on your system locally. None of your data is ever transmitted.   
![notrackanalytics](https://gitlab.com/quidsup/notrack/-/wikis/uploads/e4501a37568f02a5f80a5bf1e8bbdf49/Screenshot_20211020_221845.png)

### More Features
See the [NoTrack Wiki](https://gitlab.com/quidsup/notrack/wikis/Features) for more features
  
# Installation
NoTrack is best used on a Raspberry Pi with [Ubuntu Server](https://ubuntu.com/download/raspberry-pi). Compatibility with other Linux distros will be made available in future versions.   

### Automated Install with Ubuntu / Linux Mint
```bash
wget https://gitlab.com/quidsup/notrack/raw/master/install-ubuntu.sh
bash install-ubuntu.sh
```
### Automated Install with Debian / Raspberry Pi OS (formerly Raspbian)
```bash
wget https://gitlab.com/quidsup/notrack/raw/master/install-debian.sh
bash install-debian.sh
```   

# License
NoTrack is an open source project licensed under [GNU General Public License v3.0](https://gitlab.com/quidsup/notrack/-/raw/master/LICENSE)

# Project Status
NoTrack is fully functional, although development of new features is slow since this is just a side project.
